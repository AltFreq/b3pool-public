const async = require("async");
var mysql = require('mysql');
const Webhook = require("webhook-discord");
const monthNames = ["January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December"
                   ];
const Hook = new Webhook("https://discordapp.com/api/webhooks/488232184646467594/UiG_fBIfFJFNdYfCakEtENYe7Lbh9fFCDwOAuv-JqQzYii4o1niVKSHuFaPAj2Z5hCGd");
var database = mysql.createConnection({
    host: "67.205.148.48",
    user: "home",
    password: "Ms016356!01",
    database: "d3xPool"
});


function dec2hex(i) {
    var color= (i+0x10000).toString(16).substr(-6).toUpperCase();
    if(color.length<6){
        return "FFFFFF"
    }else{
        return color;
    }
}

function sendMessageFunc(message, title, color, callback){
    Hook.custom("B3FNPool",message,title,color);
    callback();
}

function getTimeTillMidnightUTCDaily(callback){
    var midnight = new Date();
    midnight.setUTCHours( 24 );
    midnight.setUTCMinutes( 0 );
    midnight.setUTCSeconds( 0 );
    midnight.setUTCMilliseconds( 0 );
    console.log(midnight.getTime());
    callback((midnight.getTime() - new Date().getTime()));
}

function getTimeTillMidnightUTCWeekly(callback){
    var midnight = new Date();
    midnight.setUTCHours( 24 );
    midnight.setUTCMinutes( 0 );
    midnight.setUTCSeconds( 0 );
    midnight.setUTCMilliseconds( 0 );
    midnight.setUTCDate(midnight.getUTCDate() + (7 - midnight.getUTCDay()) % 7);
    var now = new Date();
    callback((midnight.getTime() - now.getTime()));
}


function getTimeTillMidnightUTCMonthly(callback){
    var midnight = new Date();
    midnight.setUTCHours( 24 );
    midnight.setUTCMinutes( 0 );
    midnight.setUTCSeconds( 0 );
    midnight.setUTCMilliseconds( 0 );
    midnight.setUTCDate(1);
    midnight.setUTCMonth(midnight.getUTCMonth()+1);
    var now = new Date();
    callback((midnight.getTime() - now.getTime()));
}


function getDailyStats(callback){
    var date = new Date()
    var title = "Daily Statistics - " +date.toUTCString();
    var sql = "SELECT COUNT(*) AS rewards, SUM(Amount) AS reward_sum FROM Node_Rewards WHERE DateTime >= DATE_ADD(CURDATE(), INTERVAL -1 DAY);";
    database.query(sql, function (err, reward_Data, fields) {
        if (err) throw err;
        if(reward_Data.length>0){
            reward_Data=reward_Data[0];
            var sql = "SELECT SUM(Lottery_Amount)+Sum(Direct_Amount) as pool_Total, Count(*) as pool_Count FROM Temporary_Member;";
            database.query(sql, function (err, pool_Data, fields) {
                if (err) throw err;
                pool_Data=pool_Data[0]
                var roi = 15000/(15000/pool_Data.pool_Total*reward_Data.reward_sum);
                var message = "```ml\n                                                   \nNumber Of Rewards Earnt: "+reward_Data.rewards+"\nValue Of Rewards: "+reward_Data.reward_sum+" kB3\nAverage Reward: "+(reward_Data.reward_sum/reward_Data.rewards)+"\nEstimated ROI: "+roi+" days\nFN Pool Size: "+pool_Data.pool_Total+" kB3\nNew Users: 0\nUsers Removed: 0\nTotal FN Pool Members: "+pool_Data.pool_Count+"\n```";
                var color = "#"+dec2hex(reward_Data.reward_sum);
                sendMessageFunc(message, title, color, function(){
                    console.log("Message sent")
                });
            });

        }else{
            var sql = "SELECT SUM(Lottery_Amount)+Sum(Direct_Amount) as pool_Total, Count(*) as pool_Count FROM Temporary_Member;";
            database.query(sql, function (err, pool_Data, fields) {
                if (err) throw err;
                pool_Data=pool_Data[0];
                var message = "```ml\n                                                   \nNumber Of Rewards Earnt: 0\nFN Pool Size: "+pool_Data.pool_Total+" kB3\nNew Users: 0\nUsers Removed: 0\nTotal FN Pool Members: "+pool_Data.pool_Count+"\n```";                var color = "#"+dec2hex(pool_Data.pool_Total);
                sendMessageFunc(message, title, color, function(){
                    console.log("Message sent")
                });
            });
        }
    });
}

function getWeeklyStats(callback){
    var date = new Date();
    var lastweek = new Date();
    lastweek.setDate(lastweek.getDate()-7);
    var title = "Weekly Statistics - " +lastweek.toUTCString()+" - "+ date.toUTCString();
    var sql = "SELECT COUNT(*) AS rewards, SUM(Amount) AS reward_sum FROM Node_Rewards WHERE DateTime >= DATE_ADD(CURDATE(), INTERVAL -7 DAY);";
    database.query(sql, function (err, reward_Data, fields) {
        if (err) throw err;
        if(reward_Data.length>0){
            reward_Data=reward_Data[0];
            var sql = "SELECT SUM(Lottery_Amount)+Sum(Direct_Amount) as pool_Total, Count(*) as pool_Count FROM Temporary_Member;";
            database.query(sql, function (err, pool_Data, fields) {
                if (err) throw err;
                pool_Data=pool_Data[0];
                var roi = 15000/(15000/pool_Data.pool_Total*reward_Data.reward_sum)
                var message = "```ml\n                                                   \nNumber Of Rewards Earnt: "+reward_Data.rewards+"\nValue Of Rewards: "+reward_Data.reward_sum+" kB3\nAverage Reward: "+(reward_Data.reward_sum/reward_Data.rewards)+"\nEstimated ROI: "+roi+" weeks\nFN Pool Size: "+pool_Data.pool_Total+" kB3\nNew Users: 0\nUsers Removed: 0\nTotal FN Pool Members: "+pool_Data.pool_Count+"\n```";
                var color = "#"+dec2hex(reward_Data.reward_sum);
                sendMessageFunc(message, title, color, function(){
                    console.log("Message sent")
                });
            });

        }else{
            var sql = "SELECT SUM(Lottery_Amount)+Sum(Direct_Amount) as pool_Total, Count(*) as pool_Count FROM Temporary_Member;";
            database.query(sql, function (err, pool_Data, fields) {
                if (err) throw err;
                pool_Data=pool_Data[0];
                var message = "```ml\n                                                   \nNumber Of Rewards Earnt: 0\nFN Pool Size: "+pool_Data.pool_Total+" kB3\nNew Users: 0\nUsers Removed: 0\nTotal FN Pool Members: "+pool_Data.pool_Count+"\n```";                var color = "#"+dec2hex(pool_Data.pool_Total);
                sendMessageFunc(message, title, color, function(){
                    console.log("Message sent")
                });
            });
        }
    });
}

function getMonthlyStats(callback){
    var date = new Date();
    var title = "Month Statistics for " + monthNames[date.getMonth()-1];
    var sql = "SELECT COUNT(*) AS rewards, SUM(Amount) AS reward_sum FROM Node_Rewards WHERE DateTime >= DATE_ADD(CURDATE(), INTERVAL -1 MONTH);";
    database.query(sql, function (err, reward_Data, fields) {
        if (err) throw err;
        if(reward_Data.length>0){
            reward_Data=reward_Data[0];
            var sql = "SELECT SUM(Lottery_Amount)+Sum(Direct_Amount) as pool_Total, Count(*) as pool_Count FROM Temporary_Member;";
            database.query(sql, function (err, pool_Data, fields) {
                if (err) throw err;
                pool_Data=pool_Data[0];
                var roi = 15000/(15000/pool_Data.pool_Total*reward_Data.reward_sum)
                var message = "```ml\n                                                   \nNumber Of Rewards Earnt: "+reward_Data.rewards+"\nValue Of Rewards: "+reward_Data.reward_sum+" kB3\nAverage Reward: "+(reward_Data.reward_sum/reward_Data.rewards)+"\nEstimated ROI: "+roi+" months\nFN Pool Size: "+pool_Data.pool_Total+" kB3\nNew Users: 0\nUsers Removed: 0\nTotal FN Pool Members: "+pool_Data.pool_Count+"\n```";
                var color = "#"+dec2hex(reward_Data.reward_sum);
                sendMessageFunc(message, title, color, function(){
                    console.log("Message sent")
                });
            });

        }else{
            var sql = "SELECT SUM(Lottery_Amount)+Sum(Direct_Amount) as pool_Total, Count(*) as pool_Count FROM Temporary_Member;";
            database.query(sql, function (err, pool_Data, fields) {
                if (err) throw err;
                pool_Data=pool_Data[0];
                var message = "```ml\n                                                   \nNumber Of Rewards Earnt: 0\nsFN Pool Size: "+pool_Data.pool_Total+" kB3\nNew Users: 0\nUsers Removed: 0\nTotal FN Pool Members: "+pool_Data.pool_Count+"\n```";
                var color = "#"+dec2hex(pool_Data.pool_Total);
                sendMessageFunc(message, title, color, function(){
                    console.log("Message sent")
                });
            });
        }
    });
}

function resetDaily(){
    getTimeTillMidnightUTCDaily(function(midnight){
        console.log("Midnight is in " + midnight + "miliseconds for daily");
        setTimeout(function(){
            getDailyStats(function(){
                resetDaily();
            });
        },midnight);
    });
}

function resetWeekly(){
    getTimeTillMidnightUTCWeekly(function(weekly){
        console.log("Midnight is in " + weekly + "miliseconds for weekly");

        setTimeout(function(){
            getWeeklyStats(function(){
                resetWeekly();
            });
        },weekly)
    });
}

function resetMonthly(){
    getTimeTillMidnightUTCMonthly(function(monthly){
        console.log("Midnight is in " + monthly + "miliseconds for monthly");
        setTimeout(function(){
            getMonthlyStats(function(){
                resetMonthly();
            })
        },monthly);
    });
}


getTimeTillMidnightUTCDaily(function(midnight){
    console.log("Midnight is in " + midnight + "miliseconds for daily");
    setTimeout(function(){
        getDailyStats(function(){
            resetDaily();
        });
    },midnight);
});
getTimeTillMidnightUTCWeekly(function(weekly){
    console.log("Midnight is in " + weekly + "miliseconds for weekly");
    setTimeout(function(){
        getWeeklyStats(function(){
            resetWeekly();
        });
    },weekly)
});

getTimeTillMidnightUTCMonthly(function(monthly){
    console.log("Midnight is in " + monthly + "miliseconds for monthly");
    setTimeout(function(){
        getMonthlyStats(function(){
            resetMonthly();
        })
    },monthly);
});

