var mysql = require('mysql');
var RpcClient = require('b3coind-rpc');
const async = require("async");
const request = require("request");


var client = null;
var blockheight = 0;
var nodes = [];

var con = mysql.createConnection({
    host: "mysql_server",
    user: "home",
    password: "Ms016356!01",
    database: "d3xPool"
});

function setRPCClient(port, callback){
    console.log("Client rpc " + port);
    var config = {
        protocol:'http',//Optional. Will be http by default
        host:'127.0.0.1',//Will be 127.0.0.1 by default
        user:'AltFreq',//Optional, only if auth needed
        pass:'Ms016356!01',//Optional. Can be named 'pass'. Mandatory if user is passed.
        port:port,//Will be 8443 for https or 8080 for http by default
    };
    var client = new RpcClient(config);
    callback(client);
}

function getStakedAmount(address, callback){
    request.get('https://chainz.cryptoid.info/explorer/address.summary.dws?coin=b3&id='+address, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            var staked = data.stakeOut/100000000
            var number = data.stakenb
            if(isNaN(staked)){
                console.log(body);
                console.log("retrying " + address)
                setTimeout(function(){
                    getStakedAmount(address,callback);
                },10000);
            }else{
                if(staked>0){
                    var end = data.tx[data.tx.length-1][4];
                    if(end > 0){
                        getTransactionTime(data.tx[data.tx.length-1][1],function(time){
                            callback([number,staked,time]);
                        });
                    }else{
                        getTransactionTime(data.tx[data.tx.length-2][1],function(time){
                            callback([number,staked,time]);
                        });
                    }
                }else{
                    callback([number,staked,0]);
                }
            }
        }else{
            console.error("Error getting web data trying again in 5 seconds");
            callback();
        }
    });
}

function getTransactionTime(transaction_id, callback){
    client.getTransaction(transaction_id,function(err,res){
        var transaction = res.result;
        var blockTime = new Date(transaction["blocktime"]*1000);
        callback(blockTime);
    });
}

function getTransactionData(transaction_id, output, callback){
    client.getTransaction(transaction_id,function(err,res){
        var transaction = res.result;
        var vout = transaction["vout"];
        var confimations = transaction["confirmations"];
        var burnBlock = blockheight-confimations
        var disintegrationCost = burnBlock<85000?25000:burnBlock<105000?20000:15000;
        var blockTime = new Date(transaction["blocktime"]*1000);
        for(var j = 0; j < vout.length; j++){
            var nout = vout[j]["n"];
            if(nout == output){
                var address = vout[j].scriptPubKey.addresses[0];

                getStakedAmount(address,function(stakeData){
                    console.log("Node " + address + " added " +  stakeData);
                    if(stakeData[2]!==0){
                        nodes.push({transaction_id: transaction_id,Address:address, output: output, cost: disintegrationCost, burnBlock: burnBlock, date: blockTime.toISOString().replace("T"," ").substring(0,19), rewardAmount: stakeData[1], rewardTimes: stakeData[0], lastRewarded: stakeData[2].toISOString().replace("T"," ").substring(0,19)});
                    }else{
                        nodes.push({transaction_id: transaction_id,Address:address, output: output, cost: disintegrationCost, burnBlock: burnBlock, date: blockTime.toISOString().replace("T"," ").substring(0,19), rewardAmount: stakeData[1], rewardTimes: stakeData[0], lastRewarded: null});
                    }
                    callback();
                });
            }
        }
    });
}

function compare(a,b) {
    if (a.burnBlock < b.burnBlock)
        return -1;
    if (a.burnBlock > b.burnBlock)
        return 1;
    return 0;
}

setRPCClient(8999,function(c){
    client = c;
    client.getBlockCount(function(err,res){
        blockheight = res.result;
        console.log("Current block height = " + blockheight);
        client.fundamentalnode("outputs", function(err,res){
            var outputs = res.result;
            var keys = Object.keys(outputs)
            console.log("Loading " +keys.length + " nodes");
            async.eachSeries(keys, function(transaction_id, callback) {
                var output = outputs[transaction_id];
                setTimeout(function(){
                    getTransactionData(transaction_id, output, function(){
                        callback();
                    });
                },11000);
            }, function(err) {
                if( err ) {
                    console.error('Error');
                } else {
                    nodes.sort(compare)
                    con.connect(function(err) {
                        if (err) throw err;
                        console.log("Connected!");
                        async.eachSeries(nodes, function(node, callback) {

                            var sql = "INSERT INTO Nodes (Transaction_ID, Address, Transaction_Output, Cost, Burn_Block, Created, Earnt, Number_Of_Rewards, Last_Paid) VALUES (?,?,?,?,?,?,?,?,?)";
                            con.query(sql,[node.transaction_id, node.Address, node.output, node.cost, node.burnBlock, node.date, node.rewardAmount, node.rewardTimes, node.lastRewarded], function (err, result) {
                                if (err) throw err;
                                console.log("Number of records inserted: " + result.affectedRows);
                                callback();
                            });

                        }, function(err) {
                            if( err ) {
                                console.error('Error');
                            } else {
                                console.log("DONE INSERTING");
                                con.destroy();
                            }
                        });
                    });

                }
            });
        });
    });
});



