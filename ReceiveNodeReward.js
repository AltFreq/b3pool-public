var RpcClient = require('b3coind-rpc');
const async = require("async");
var mysql = require('mysql');
const Webhook = require("webhook-discord");
const Hook = new Webhook("https://discordapp.com/api/webhooks/488131258442973184/wgkwC1O50f6DHWN0h9QZ4ozoASfk2mQz-T0bfpBkf-TllQq7ARr6RxFwTHtSVwcJnADP");
var txid = process.argv[2]
var client = null;
var database = mysql.createConnection({
    host: "67.205.148.48",
    user: "home",
    password: "Ms016356!01",
    database: "d3xPool"
});



function setManagerClient(callback){
    var config = {
        protocol:'http',//Optional. Will be http by default
        host:'127.0.0.1',//Will be 127.0.0.1 by default
        user:'AltFreq',//Optional, only if auth needed
        pass:'Ms016356!01',//Optional. Can be named 'pass'. Mandatory if user is passed.
        port:8998,//Will be 8443 for https or 8080 for http by default
    };
    client = new RpcClient(config);
    callback();
}
console.log("Processing txid: " + txid);


function getTransaction(callback){
    client.getTransaction(txid,function(err,res){
        if(err){
            console.log("Can't contact wallet waiting 10 seconds");
            console.log(err);
            setTimeout(function(){
                getTransaction(callback);
            },10000);
        }else{
            callback(res.result);
        }
    });
}

function dec2hex(i) {
    var color= (i+0x10000).toString(16).substr(-6).toUpperCase();
    if(color.length<6){
        return "FFFFFF"
    }else{
        return color;
    }
}

function sendMessageFunc(val, nodeIndex, txid, callback){
    Hook.custom("B3FNPool","Reward of **"+val+"kB3** received\n [View Transaction](https://chainz.cryptoid.info/b3/tx.dws?"+txid+")","Node "+nodeIndex,"#"+dec2hex(val));
    callback();
}


console.log("Setting manager Client")
setManagerClient(function(){
    getTransaction(function(data){
        var confirmations = data["confirmations"];
        console.log("Confirmations = " + confirmations)
        if(confirmations>=1){
            var nodeAddress = data["details"][0]["address"];
            var rewardAmount = data["vout"][data["vout"].length-1]["value"];
            var blockTime = new Date(data["blocktime"]*1000);
            blockTime =blockTime.toISOString().replace("T"," ").substring(0,19);
            database.connect(function(err) {
                if (err) throw err;
                console.log("Connected!");
                console.log("Finding id for address " + nodeAddress)
                var sql = "SELECT Node_ID FROM Nodes WHERE Address = ?";
                database.query(sql, nodeAddress, function (err, result, fields) {
                    if (err) throw err;
                    var nodeID = result[0].Node_ID;
                    console.log("Node id = " + nodeID);
                    sql ="INSERT INTO Node_Rewards (Transaction_ID, Node_ID, Amount, DateTime, Paid) VALUES (?,?,?,?,0)";
                    database.query(sql, [txid, nodeID, rewardAmount, blockTime], function (err, result) {
                        if (err) throw err;
                        console.log("Number of records inserted: " + result.affectedRows);
                        console.log("Sending message");
                        sendMessageFunc(rewardAmount, nodeID, txid, function(){
                            console.log("done");
                            database.destroy();
                        });
                    });
                });
            });
        }else{
            console.log("Confirmations too high");
        }
    });
});
