var RpcClient = require('b3coind-rpc');

const async = require("async");
const fs = require('fs');

const stdin = process.openStdin();

var config = {
    protocol: 'http', //Optional. Will be http by default
    host: '127.0.0.1', //Will be 127.0.0.1 by default
    user: 'AltFreq', //Optional, only if auth needed
    pass: 'Ms016356!01', //Optional. Can be named 'pass'. Mandatory if user is passed.
    port: 9004, //Will be 8443 for https or 8080 for http by default
};

var wallet = new RpcClient(config);

wallet.fundamentalnode('outputs', function(err,  res){
    if(err){
        console.log(err);
        process.exit(1);
    }else{
        var outputs = res.result;
        nodeAddresses = {}
        //nodeaddress = {} output: outputtx, outputvout: vout, address {address: pubkey}, inputAddresses[{address: add, pubkey: pk}, {address: add, pubkey: pk}];
        async.eachSeries(Object.keys(outputs), function(data, callback){
            var nout = parseInt(outputs[data]);
            wallet.getTransaction(data, function(err, res){
                if(err){
                    console.log(err)
                    process.exit(1);
                }else{
                    var nodeTransaction = res.result;
                    output = nodeTransaction['vout'][nout];
                    if(output.value!=0.00100000){
                        console.log(output.value)
                        process.exit(1);
                    }else{
                        var nodeAddress = output.scriptPubKey.addresses[0];
                        if(!nodeAddress){
                            console.log(output.scriptPubKey);
                            process.exit(1);
                        }else{
                            wallet.dumpprivkey(nodeAddress, function(err, res){
                                if(err){
                                    console.log(err);
                                    process.exit(1);
                                }else{
                                    var nodeAddressPrivKey = res.result;
                                    var inputs = nodeTransaction['vin'];
                                    var inputAddresses = []
                                    async.eachSeries(inputs, function(inputData, callback){
                                        var inputTxid = inputData['txid'];
                                        var inputvout = inputData['vout'];
                                        if(!inputTxid){
                                            console.log(inputData);
                                            process.exit(1);
                                        }else{
                                            wallet.getTransaction(inputTxid, function(err, inputRes){
                                                var inputTransaction = inputRes.result;
                                                var inputAddress = inputTransaction.vout[inputvout].scriptPubKey.addresses[0];
                                                if(!inputAddress){
                                                    console.log(inputData);
                                                    process.exit(1);
                                                }else{
                                                    wallet.dumpprivkey(inputAddress, function(err, res){
                                                        if(err){
                                                            console.log(err);
                                                            process.exit(1);
                                                        }else{
                                                            inputAddresses.push({address: inputAddress, pubkey: res.result});
                                                            callback();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    },function(){
                                        nodeAddresses[data] = {vout: nout, nodeAddress: {address:nodeAddress, privateKey: nodeAddressPrivKey}, inputAddresses: inputAddresses };
                                        callback();
                                    });
                                }
                            })
                        }
                    }
                }
            })
        }, function(){
            //finished
//            var jsonobj = JSON.parse(nodeAddresses);
//            console.log(jsonobj)
            var jsonContent = JSON.stringify(nodeAddresses);
            fs.writeFile("nodeData.json", jsonContent, 'utf8', function (err) {
                if (err) {
                    console.log("An error occured while writing JSON Object to File.");
                    return console.log(err);
                }
                console.log("JSON file has been saved.");
                process.exit(1);
            });
        });
    }
})

