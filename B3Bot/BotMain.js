const Discord = require('discord.js');
const bot = new Discord.Client();
var client = new Discord.Client();

var awaitingCommand = new Set();
bot.on('ready', () => {
    console.log(`Logged in as ${bot.user.tag}!`);
});

const exampleEmbed = new Discord.RichEmbed()
.setColor('#ff0000')
.setTitle('__**----WARNING----**__')
//    .setURL("https://test.com")
//    .setAuthor('Some name', 'https://i.imgur.com/wSTFkRM.png', 'https://discord.js.org')
.setDescription('To clear all of your accounts messages your login token is required. It is usually suggested that you should never give out your login token as it gives control of your account to someone else.');
//    .setThumbnail('https://i.imgur.com/wSTFkRM.png')
//    .addField('Regular field title', 'Some value here')
//    .addBlankField()
//    .addField('Inline field title', "[Hello World]('javascript:%28function%28%29%7Balert%28%22Hello%20World%22%29%7D%29%28%29%3B')", true)
//    .addField('Inline field title', 'Some value here', true)
//    .addField('Inline field title', 'Some value here', true)
//    .setImage('https://i.imgur.com/wSTFkRM.png')
//    .setTimestamp()
//    .setFooter('Some footer text here', 'https://i.imgur.com/wSTFkRM.png');


function warningEmbed(message){
    return new Discord.RichEmbed()
        .setColor('#ffA500')
        .setTitle('__**----ERROR----**__')
        .setDescription(message)
}

function getHelpMenu(){
    return new Discord.RichEmbed()
        .setColor('#3399FF')
        .setTitle('__**Help Menu**__')
        .setDescription('For more information on any command run **help (command)**')
        .addField('\u200b',
                  '**Balance**         : Display a list of balances associated with the users account\n\
**Buy**             : Create a buy order in the pools node market\n\
**Claim**           : Starts the address claiming wizard al?lowing a user to connect a specific B3 address to their account\n\
**Deposit**         : Displays the deposit address for your account allowing you to send kB3 to the pool\n\
**Help,?**          : Display the help menu or specific information about a command\n\
**Gift**            : Gift another discord user a certain amount of kB3\n\
**Market**          : Displays the current buy and sell offers in the pools node market\n\
**Notifications**   : Setup when you would like the bot to message you after certain events\n')
        .addField('\u200b',
                  '**Rewards**         : List the latest rewards your account has received\n\
**ROI**             : Provides you with an estimated ROI depending on the last amount of days specified\n\
**Sell**            : Create a sell order in the pools node market\n\
**Settings**        : Allows a user to change specific settings associated with their account\n\
**Stakes**          : List the latest stakes your account has received)\n\
**Stats,Statistics**: Displays personal statistics and global statistics to the user\n\
**Ticket,Request**  : Used to access our help desk system allowing a user to track any issues or requests they may have\n\
**Transfer**        : Transfer your unused coins into different wallets within the pool.\n\
**Value**           : Displays the current value of your account in USD and BTC\n\
**Withdraw**        : Withdraw unused coins from your pool account\n'
                 )
}

//function getHelpMenu(){
//    return "-**Help Menu**-\n\
//```md\n\
//<command>: [description](options)\n\
//< For more information on any command run help (command) >\n\
//\n\
//<Balance>         : [Display a list of balances associated with the users account]()\n\
//<Buy>             : [Create a buy order in the pools node market](amount price_per_kB3)\n\
//<Claim>           : [Starts the address claiming wizard al?lowing a user to connect a specific B3 address to their account]()\n\
//<Deposit>         : [Displays the deposit address for your account allowing you to send kB3 to the pool]()\n\
//<Help,?>          : [Display the help menu or specific information about a command](command)\n\
//<Gift>            : [Gift another discord user a certain amount of kB3](discord_ID amount)\n\
//<Market>          : [Displays the current buy and sell offers in the pools node market]()\n\
//<Notifications>   : [Setup when you would like the bot to message you after certain events]()\n\
//<Rewards>         : [List the latest rewards your account has received](number of rewards to display)\n\
//<ROI>             : [Provides you with an estimated ROI depending on the last amount of days specified](days)\n\
//<Sell>            : [Create a sell order in the pools node market](amount price_per_kB3)\n\
//<Settings>        : [Allows a user to change specific settings associated with their account](setting value)\n\
//<Stakes>          : [List the latest stakes your account has received](number of stakes to display)\n\
//<Stats,Statistics>: [Displays personal statistics and global statistics to the user](all | node | staking)\n\
//<Ticket,Request>  : [Used to access our help desk system allowing a user to track any issues or requests they may have](id | open | closed)\n\
//<Transfer>        : [Transfer your unused coins into different wallets within the pool.](amount source destination)\n\
//<Value>           : [Displays the current value of your account in USD and BTC]()\n\
//<Withdraw>        : [Withdraw unused coins from your pool account](amount destination_address)\n\
//```";
//}

function getHelpRich(command, description, options, example){
    if(options!=null){
        return new Discord.RichEmbed()
            .setColor('#ffffff')
            .setTitle("Command: " + command)
            .addField('Description:', description, true)
            .addField('Options:', options, true)
            .addField('Example Usage:', example);
    }else{
        return new Discord.RichEmbed()
            .setColor('#ffffff')
            .setTitle("Command: " + command)
            .addField('Description:', description, true);
    }
}

function getHelp(command){
    switch(command){
        case "help": case "?":
            return getHelpRich(command,'help <*command>\nDisplays a list of all available commands or if used with another command shows a description of that command', '<*command> - Any other available command(Optional)', 'help\n help balance\n help deposit');
            break;
        case "ping":
            return getHelpRich(command,'ping\nSends a ping command to the bot. The bot will reply with \'pong\'', null, null);
            break;
        default:
            console.log("non found")
            return warningEmbed("The command " + command + " is not supported. Please use **help** to view a list of available commands");
    }
}

bot.on('message', msg => {
    var id = msg.author.id;
    if(id === bot.user.id){
        console.log("Bot message  = " + msg.content);
    }else if(msg.content.startsWith('/')){
        console.log("/ command ignoring")
    }else{
        var array = msg.content.toLowerCase().split(' ');
        var command = array[0];
        console.log("message - " + msg.content + " from user " + msg.author.username + msg.author.discriminator);
        console.log(array);
        console.log(command);
        console.log("Author " + awaitingCommand.has(msg.author));
        if(!awaitingCommand.has(msg.author)){
            switch(command){
                case "help": case "?":
                    if(array.length>1){
                        msg.reply(getHelp(array[1]));
                    }else{
                        console.log("getting help menu")
                        msg.reply(getHelpMenu());
                    }
                    break;
                case "ping":
                    msg.reply("pong");
                    break;
                default:
                    msg.reply(warningEmbed("Invalid command run the **help** command for a list of valid commands and information."));
            }
        }
    }
});

bot.login('NDkxNzIzOTQ5Mjc3NzA4Mjg4.DoMBSA.YFYnu_FeWhJTOua1Erl9dWQ6-KE');
