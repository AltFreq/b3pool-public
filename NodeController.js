var RpcClient = require('b3coind-rpc');
const request = require("request");
const async = require("async");
const fs = require('fs');
const path = require('path');
const readline = require("readline");
const stdin = process.openStdin();

var onlineBlockCount = 0;
var onlineFNCount = 0
var b3nodesSite =  null;

var blockheight = 0;

var users = []
var nodes = []
var posNodes = [];
var maxNodesOnUser = 0;

var processes ={};

function checkWalletCommands(rpc, callback){
    console.debug("Checking wallet commands");
    var commandCompleted = false;
    setTimeout(function(){
        if(commandCompleted===false){
            console.log("wallet has timed out, killing");
            var process = processes[""+rpc.port];
            if(process===undefined){
                killprocess(rpc.port,function(res){
                    setTimeout(function(){
                        startRootWallet(function(){
                            setTimeout(function(){
                                checkWalletCommands(rpc, callback);
                            },10000);
                        });
                    },5000);
                });
            }else{
                process.kill('SIGINT');
            }
        }
        //kill -9 $(lsof -sTCP:LISTEN -t -i :8999)
    },60000);
    rpc.getblockcount(function (err, res) {
        if (err !== null) {
            console.debug("error checking wallet command " + err);
            commandCompleted =true
            callback(err);
        }else{
            console.debug("No errors");
            commandCompleted = true
            callback();
        }
    })
}

function setupB3WebData(callback, webCounter=0){
    console.debug("Updating online data");
    request.get('http://fn.b3nodes.net/fncheck.txt', function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var lines = body.split('\n');
            onlineFNCount = lines[3].split(/\t+/)[1];
            console.debug("Online FNS = " + onlineFNCount);
            onlineBlockCount = lines[9].split(/\t+/)[0]-1;
            console.debug("Block count = "+ onlineBlockCount);
            b3nodesSite = body;
            callback();
        }else{
            console.error("Error getting web data trying again in 5 seconds");
            webCounter+=1
            if(webCounter==2){
                onlineBlockCount = 169385
                callback();
            }else{
                setTimeout(function(){
                    setupB3WebData(callback, webCounter);
                },5000);
            }
        }
    });
}

function stopWallet(rpc, callback){
    console.debug("Wallet stopping")
    rpc.stop(function(err, ret){
        if(!err){
            setTimeout(function(){
                callback();
            },10000);
        }else{
            console.debug("Error stopping wallet trying again " + err);
            setTimeout(function(){
                stopWallet(rpc,callback)
            },10000);
        }
    });
}

function rootRPCSetup(callback){
    getRPCClient(8999, function(client){
        rootRPC = client;
        checkWalletCommands(rootRPC, function(err){
            if(!err){
                stopWallet(rootRPC, function(){
                    console.debug("Wallet stop completed")
                    callback();
                })
            }else{
                callback();
            }
        });
    });
}

function getUserDirectories(callback){
    console.debug("Getting user directories")
    fs.readdir('/home/', (err, directories) => {
        if (err) console.error (err);
        callback(directories);
    });
}

function getRPCClient(port, callback){
    console.debug("Setting up client for " + port)
    var config = {
        protocol:'http',//Optional. Will be http by default
        host:'127.0.0.1',//Will be 127.0.0.1 by default
        user:'AltFreq',//Optional, only if auth needed
        pass:'Ms016356!01',//Optional. Can be named 'pass'. Mandatory if user is passed.
        port:port,//Will be 8443 for https or 8080 for http by default
    };
    var client = new RpcClient(config);
    callback(client);
}

function setupUser(port,ip, user, callback){
    console.debug("Setting up " + user + " on port "+ port);
    getRPCClient(port, function(client){
        var data = {username : user, client: client, ip:ip}
        users.push(data);
        callback();
    });
}

function readUserConfFile(directory, file, finished){
    var port =null;
    var ip = null
    fs.readFile( directory, 'utf8', function (err, data) {
        if (err) console.error("error reading b3coin.conf file");
        var lines = data.split('\n');
        async.each(lines, function(fn, callback) {
            if(fn.length >5) {
                var split = fn.split('=');
                var setting = split[0];
                var value = split[1];
                if(setting==='rpcport'){
                    port = value;
                }
                if(setting==='fundamentalnodeaddr'){
                    ip = value;
                }
            }
            callback();
        }, function(err) {
            if( err ) {
                console.error('Error loading b3coin.conf');
            } else {
                console.debug('b3coin.conf correctly loaded');
                setupUser(port,ip,file, function(){
                    console.log(file + " finished");
                    finished();
                });
            }
        });
    });
}

function setupUsersRPC(userDirectories, finished){
    console.debug("Getting b3 directories");
    async.each(userDirectories, function(file, callback) {
        console.debug("Setting up " + file);
        var b3dir = '/home/'+file+'/.B3-CoinV2';
        fs.stat(b3dir, (err, stats) =>{
            if (err) console.log(err);
            if(stats && stats.isDirectory()){
                console.log(b3dir + " is directory")
                readUserConfFile(b3dir+'/b3coin.conf', file, function(){
                    callback();
                });
            }
        });
    }, function(err) {
        if( err ) {
            console.error('setup users rpc err');
        } else {
            console.debug( 'users rpc done');
            finished();
        }
    });
}

function checkUsersSetup(finished){
    console.debug("Checking user wallets setup")
    async.each(users, function(user, callback) {
        console.debug("Checking user " + user.username + " wallet commands");
        checkWalletCommands(user.client, function(err){
            if(!err){
                console.debug("No error stopping wallet")
                stopWallet(user.client, function(){
                    console.debug("Wallet stop completed - " + user.username);
                    callback();
                });
            }else{
                callback();
            }
        });
    }, function(err) {
        if( err ) {
            console.error('check users setup err - ' + err);
        } else {
            console.debug( 'user wallets stopped');
            finished();
        }
    });
}

function usersSetup(callback){
    console.debug("Users setup running")
    getUserDirectories(function(userDirectories){
        setupUsersRPC(userDirectories,function(){
            checkUsersSetup(function(){
                console.log("Users setup completed")
                callback();
            });
        });
    });
}

function nodeExists(transaction){
    var contained = false;
    for(var i = 0; i < nodes.length; i++){
        var node = nodes[i];
        var nodeTx = node["txid"];
        if(nodeTx===transaction){
            contained = true;
            break;
        }
    }
    return contained;
}


function getTransactionData(transaction_id, output, callback){
    rootRPC.getTransaction(transaction_id,function(err,res){
        var transaction = res.result;
        if(!nodeExists(transaction_id)){
            var vout = transaction["vout"];
            var confimations = transaction["confirmations"];
            var burnBlock = blockheight-confimations
            var disintegrationCost = burnBlock<85000?25000:burnBlock<105000?20000:15000;
            var blockTime = new Date(transaction["blocktime"]*1000);
            for(var j = 0; j < vout.length; j++){
                var nout = vout[j]["n"];
                if(nout == output){
                    var address = vout[j].scriptPubKey.addresses[0];
                    nodes.push({"txid": transaction_id, "pubkey":address, "output": output, "cost": disintegrationCost, "burnBlock": burnBlock, "date": blockTime.toISOString().replace("T"," ").substring(0,19), "genkey": '68bGnhqpm2AMtbi2DrRp3vGm6BVpa5k6QGaHZa4i4uF4QHYo5zE'});
                    callback();
                    break;
                }
            }
        }else{
            callback();
        }
    });
}

function compare(a,b) {
    if (a.burnBlock < b.burnBlock)
        return -1;
    if (a.burnBlock > b.burnBlock)
        return 1;
    return 0;
}

function loadNodesFromWallet(finished){
    rootRPC.fundamentalnode("outputs", function(err,res){
        var outputs = res.result;
        var keys = Object.keys(outputs)
        console.log("Loading " +keys.length + " nodes");
        async.eachSeries(keys, function(transaction_id, callback) {
            var output = outputs[transaction_id];
            getTransactionData(transaction_id, output, function(){
                callback();
            });
        }, function(err) {
            if( err ) {
                console.error('Error');
            } else {
                nodes.sort(compare);
                //Add in name, genkey, number
                var i = 0;
                async.eachSeries(nodes, function(node, callback) {
                    node.name = "B3FN"+i;
                    node["number"] = i;
                    i++;
                    callback();
                }, function(err) {
                    if( err ) {
                        console.error('Error');
                    } else {
                        nodes.sort(compare);
                        //Add in name, genkey, number
                        console.log("All nodes loaded");
                        finished();
                    }
                })
            }
        });
    });
}

function loadNodesFromFile(finished){
    fs.readFile(path.resolve(require('os').homedir(),".B3-CoinV2", 'fundamentalnode.conf'), 'UTF-8', (err, data) =>{
        if (err) console.error(err);
        var lines = data.split('\n');
        var counter = 0
        async.each(lines, function(fn, callback) {
            if(fn.length >5) {
                var split = fn.split(' ');
                nodes.push({"name":split[0],"genkey": split[2],"txid": split[3],"output": split[4], "number": split[0].split('N')[1]});
            }
            callback();
        }, function(err) {
            if( err ) {
                console.error('load nodes from err');
            } else {
                console.log(nodes.length + ' nodes loaded');
                console.log(nodes);
                finished();
            }
        });
    });
}

function writeToFNConfig(data, callback){
    console.debug("Writing data to fundamentalnode.conf");
    fs.writeFile (path.resolve(require('os').homedir(),".B3-CoinV2","fundamentalnode.conf"), data, function(err) {
        if (err) throw err;
        console.log("Data written");
        callback();
    });
}

function loadPublicKeysFromFile(finished){
    console.debug("Loading public keys");
    fs.readFile( path.resolve(path.resolve(__dirname),"nodePublicKeys.txt"), 'utf8', function (err, data) {
        if (err) console.error("error reading nodePublic keys file");
        var lines = data.split('\n');
        async.each(lines, function(fn, callback) {
            if(fn.length >5) {
                var split = fn.split(' ');
                var name = split[0].split('e')[1];
                var publicKey = split[1]
                for(var i = 0; i < nodes.length; i++){
                    var node = nodes[i];
                    var nodeNumber = node.number
                    if(parseInt(nodeNumber)==parseInt(name)){
                        nodes[i]['pubkey'] = publicKey;
                        break;
                    }
                }
            }
            callback();
        }, function(err) {
            if( err ) {
                console.error(' load public keys from fileerr');
            } else {
                console.debug(nodes.length + ' node keys loaded');
                finished();
            }
        });
    });
}

function writeNodesToFundamentalnodeConf(finished){
    var nodeconfString = '';
    async.each(nodes, function(node, callback) {
        nodeconfString+=node['name'] + ' ' + '[2604:a880:2:d0::20a4:1]:5647 ' +  node['genkey'] + ' ' + node['txid'] + ' ' + node['output'] + '\n';
        callback()
    }, function(err) {
        if( err ) {
            console.error('load nodes from err');
        } else {
            writeToFNConfig(nodeconfString, function(){
                console.log(nodes.length + ' nodes written to conf');
                finished();
            })
        }
    });
}

function nodesSetup(callback){
    startRootWallet(function(){
        setTimeout(function(){
            rootRPC.getBlockCount(function(err,res){
                if(res===undefined){
                    setTimeout(function(){
                        nodesSetup(callback);
                    },10000);
                }else{
                    blockheight = res.result;
                    console.log("Current block height = " + blockheight);
                    loadNodesFromWallet(function(){
                        stopWallet(rootRPC,function(){
                            setTimeout(function(){
                                writeNodesToFundamentalnodeConf(function(){
                                    startRootWallet(function(){
                                        setTimeout(function(){
                                            callback();
                                        },5000);
                                    });
                                });
                            },5000);
                        })
                    });
                }
            });
        },5000);
    });
}


function configureFN(user,node, callback){
    console.debug("Getting FN data from user and node");
    var configLoc = path.resolve('/home/',user.username,".B3-CoinV2","b3coin.conf");
    var push = false;
    var val = null;
    fs.readFile(configLoc, 'utf8', function (err, data) {
        if (err) throw err;
        var lines = data.split('\n');
        for(var i = 0; i < lines.length; i++){
            if(lines[i].startsWith("fundamentalnodeprivkey")){
                val = "fundamentalnodeprivkey="+node.genkey;
                user.genkey = node.genkey;
                lines[i] = val;
            }
            if(lines[i].startsWith("fundamentalnodeaddr")){
                var addr = lines[i].split('=');
                val = ([node.name,addr[1]]);
            }
        }
        lines = lines.join('\n');
        fs.writeFile (configLoc, lines, function(err) {
            if (err) throw err;
            if(val){
                callback(val);
            }else{
                callback();
            }
        });
    });
}

function configureFNs(fnlist, finished){
    console.debug("Configuring FNS");
    var fnConf = [];
    async.each(fnlist, function(nodeGroup, callback) {
        var user = nodeGroup[1];
        var node = nodeGroup[0];
        console.log("configuring fn for " + node.name + " user " + user.username)
        configureFN(user,node,function(obj){
            if(obj){
                fnConf.push(obj);
            }
            console.log("configured fn for " + node.name + " user " + user.username)
            callback();
        });
    }, function(err) {
        if( err ) {
            console.log('configure fns err');
        } else {
            finished(fnConf);
        }
    });
}

function getFNConfigData(fndata, callback){
    console.log("GET FN CONFIG DATA " + fndata.length)
    fs.readFile(path.resolve(require('os').homedir(),".B3-CoinV2","fundamentalnode.conf"), 'utf8', function (err, data) {
        if (err) throw err;
        var lines = data.split('\n');
        for(var i = 0; i < fndata.length; i++){
            var fnName = fndata[i][0];
            var fnAddr = fndata[i][1];
            for(var j=0; j<lines.length; j++){
                var line = lines[j];
                var objs = line.split(" ");
                if(objs[0]==fnName){
                    console.debug("Setting ip for fn" + fnName + " fnaddr =" + fnAddr)
                    objs[1]=fnAddr;
                    line = objs.join(" ");
                    lines[j] = line;
                    break;
                }
            }
            for(var j = 0; j < nodes.length; j++){
                var node = nodes[j];
                if(fndata[i][0] == node.name){
                    console.debug("Setting ip for fn" + node.name + " fnaddr =" + fnAddr)
                    nodes[j].fnip = fnAddr;
                    break
                }
            }
        }
        callback(lines.join('\n'));
    });
}

function killprocess(port, callback){
    console.log("Getting process with port");
    var spawn = require('child_process').spawn,
        cmd    = spawn("lsof", ['-sTCP:LISTEN', '-t', '-i',':'+port]);
    cmd.stdout.on('data', function (data) {
        console.log('stdout: ' + data.toString());
        console.log("Killing")
        var spawn = require('child_process').spawn,
            cmd    = spawn("kill", ['-9', parseInt(data.toString())]);
        cmd.stderr.on('data', function (data) {
            console.log('stderr: ' + data.toString());
        });
        callback()
    });
    cmd.stderr.on('data', function (data) {
        console.log('stderr: ' + data.toString());
    });
}

function run_cmd(command, args, callback) {
    console.log("Running command " + command + ' ' + args);
    var spawn = require('child_process').spawn,
        cmd    = spawn(command, args),
        result = '';
    cmd.stdout.on('data', function (data) {
        //        console.log('stdout: ' + data.toString());
        result+=data.toString();
    });
    cmd.stderr.on('data', function (data) {
        //        console.log('stderr: ' + data.toString());
        result+=data.toString();
    });
    cmd.on('exit', function (code) {
        //        console.log('child process exited with code ' + code.toString());
        console.log(result);
        callback(cmd, result);
    });
}

function startWallet(user, callback) {
    console.log("Start wallet command")
    if(user==='root'){
        run_cmd("b3coind",[],function(cmd, res){
            processes['8999'] = cmd;
            callback(res);
        });
    }else{
        run_cmd("b3coind",["-datadir=/home/"+user.username +"/.B3-CoinV2"],function(cmd, res){
            var number = user.username.split("n")[1];
            if(number.length==1){
                number='0'+number;
            }
            processes["90"+number] = cmd;
            console.log("Stored process in " + "90"+number);
            callback(res);
        });
    }
}

function restartWallet(user, callback){
    if(user==="root"){
        console.log("Restarting wallet - " +user);
        setTimeout(function(){
            stopWallet(rootRPC,function(res){
                console.log("Stop completed " + res);
                setTimeout(function(){
                    startWallet("root", function(err){
                        if(!err){
                            callback();
                        }
                        if(err.startsWith("Error: Cannot obtain a lock on data directory")){
                            console.log(user.username+ " already running");
                            restartWallet(user, function(){
                                callback();
                            });
                        }
                    });
                },20000);
            });
        },1000);
    }else{
        console.log("Restarting wallet - " +user.username);
        setTimeout(function(){
            stopWallet(user.client,function(res){
                console.log("Stop completed " + res);
                setTimeout(function(){
                    startWallet(user, function(err){
                        if(!err){
                            callback();
                        }
                        if(err.startsWith("Error: Cannot obtain a lock on data directory")){
                            console.log(user.username+ " already running");
                            restartWallet(user, function(){
                                callback();
                            });
                        }
                    });
                },20000);
            });
        },1000);
    }
}

function startRootWallet(callback){
    startWallet("root",function(err){
        if(err.startsWith("Error: Cannot obtain a lock on data directory")){
            restartWallet("root", function(){
                console.log("Root running successfully")
                callback();
            });
        }else{
            console.log("Root wallet started ");
            callback();
        }
    });
}


function startFN(node, callback){
    console.debug("Starting " + node.name);
    var commandCompleted = false;
    setTimeout(function(){
        if(commandCompleted===false){
            console.log("Root wallet has timed out, killing start fn " + node.name);
//            processes['8999'].kill('SIGINT');
            killprocess(8999,function(res){
                setTimeout(function(){
                    startRootWallet(function(){
                        setTimeout(function(){
                            startFN(node, callback);
                        },5000);
                    });
                },5000);
            });
        }
        //kill -9 $(lsof -sTCP:LISTEN -t -i :8999)
    },60000);

    rootRPC.walletPassPhrase("Ms016356!01", [10000], function(err,res){
        if(err){
            console.log(err);
            //            callback(err);
        }else{
            console.log(res);

            rootRPC.fundamentalnode("start-alias", node.name, function(err,res){
                if(err){
                    console.log(err);
                    //                    callback(err);
                }else{
                    console.log(res);
                    commandCompleted = true;
                    console.log(node.name + " set to true");
                    callback();
                }
            });
        }
    });
}

function nodeInPOS(node){
    for(var i = 0; i < posNodes.length; i++){
        var posNode = posNodes[i];
        if(node["pubkey"]===posNode){
            return true;
            break;
        }
    }
    return false;
}

function checkForPosNodes(callback){
    console.log("Adding pos nodes")
    posNodes = []
    if(b3nodesSite!=null){
        var lines = b3nodesSite.split('\n');
        for(var i = 0; i < lines.length; i++){
            if(lines[i].length > 50){
                var nodeData = lines[i].replace(/\s+/g,' ').trim().split(' ');
                var address = nodeData[0];
                var status = nodeData[1];
                var pubkey = nodeData[2];
                if(status==='POS_ERROR'){
                    //                for(var i = 0; i < nodes.length; i++){
                    //                    var node = nodes[i];
                    //                    if(node["pubkey"]===pubkey){
                    posNodes.push(pubkey);
                    console.log("Adding " + pubkey + "  to pos list");
                    //                    }
                    //                }
                }
            }
        }
    }
    console.log(posNodes.length + " added to pos node count")
    callback();
}

function setup(callback){
    console.log("Setting up rotator");

    console.debug("Setting up root RPC")
    rootRPCSetup(function(){
        usersSetup(function(){
            nodesSetup(function(){
                setupB3WebData(function(){
                    checkForPosNodes(function(){
                        console.log("Setup Finished")
                        callback();
                    });
                });
            });
        });
    });
}



function assignNodesToUsers(finished){
    async.each(users, function(user, callback) {
        console.log("user  - " + user.username);
        var nodeList = []
        var nodeStrings = ""
        var userNumber = parseInt(user.username.split("n")[1]);
        for(var i = userNumber; i < nodes.length; i+=17){
            var node = nodes[i];
            nodeList.push(node);
            nodeStrings+=" "+node.number+",";
        }
        console.log("user " + userNumber + " has been assigned " + nodeStrings + " len " + nodeList.length);
        user.nodes = nodeList
        if(maxNodesOnUser<nodeList.length){
            maxNodesOnUser=nodeList.length;
        }
        callback();
    }, function(err) {
        if( err ) {
            console.error('setup users rpc err');
        } else {
            console.debug( 'users rpc done');
            finished();
        }
    });
}

function startRound(round, finished){
    console.log("Starting round " + round);
    var fnlist = []
    for(var i = 0; i < users.length; i++){
        var user = users[i];
        if(round<=user.nodes.length){
            var node = user.nodes[round];
            if(node!=undefined){
                fnlist.push([node,user]);
            }
        }
    }


    console.log(fnlist.length);
    stopWallet(rootRPC,function(){
        setTimeout(function(){
            configureFNs(fnlist, function(fundamentalnodeEdits){
                getFNConfigData(fundamentalnodeEdits, function(data){
                    writeToFNConfig(data, function(){
                        startRootWallet(function(){
                            setTimeout(function(){
                                console.log("configuring fns completed");
                                async.each(users, function(user, callback) {
                                    if(round<user.nodes.length){
                                        var node = user.nodes[round];
                                        console.log("Node is " + node.name);
                                        checkWalletCommands(user.client,function(err){
                                            if(!err){
                                                stopWallet(user.client,function(){
                                                    setTimeout(function(){
                                                        startWallet(user, function(){
                                                            setTimeout(function(){
                                                                callback();
                                                            },5000);
                                                        });
                                                    },5000)
                                                });
                                            }else{
                                                startWallet(user, function(){
                                                    setTimeout(function(){
                                                        callback();
                                                    },5000);
                                                });
                                            }
                                        });
                                    }else{
                                        callback();
                                    }
                                }, function(err) {
                                    if( err ) {
                                        console.error('Error with round');
                                    } else {
                                        console.debug( 'round setup done');
                                        finished();
                                    }
                                });
                            },5000)
                        })

                    });
                });

            });
        },5000)
    });
}

function startRoundFNs(round, finished){
    async.eachSeries(users, function(user, callback){
        var node = user.nodes[round];
        if(node===undefined){
            callback();
        }else if(nodeInPOS(node)){
            console.log("Skipping " + node.name + " " + node.pubkey);
            callback();
        }else{
            startFN(node, function(){
                try{
                    callback();
                }catch(error){
                    console.log(error + " ignoring");
                }
            });
        }
    }, function(err){
        if(err){throw err}
        else{finished()};
    });

}

function startFNCycle(finished){
    var highestRoundCount = []
    for(var i = 0; i < maxNodesOnUser; i++){
        highestRoundCount.push(i);
    }
    async.eachSeries(highestRoundCount, function(round, callback){
        startRound(round, function(){
            startRoundFNs(round,function(){
                console.log("Round finished");
                setTimeout(function(){
                    callback();
                },30000);
            })
        });
    }, function(err){
        if(err){ throw err}
        else{finished()}
    });
}

function autoRun(timeTaken){
    var timeToWait = 2100000-timeTaken*1000
    console.log("Waiting " +(timeToWait/1000) +"seconds before running again")
    setTimeout(function(){
        (function() {
            var begin=Date.now();
            nodesSetup(function(){
                assignNodesToUsers(function(){
                    setupB3WebData(function(){
                        checkForPosNodes(function(){
                            startFNCycle(function(){
                                var end= Date.now();
                                var timeSpent=(end-begin)/1000;
                                console.log(timeSpent + " seconds");
                                autoRun(timeSpent)
                            });

                        });
                    });
                })
            })
        })();
    },timeToWait);
}

setup(function(){
    console.log("Starting node cycle");
    assignNodesToUsers(function(){
        console.log("nodes assigned to users");
        setTimeout(function(){
            checkWalletCommands(rootRPC,function(err){
                if(err){
                    console.log("Error with root wallet, not running")
                }else{
                    console.log("Root wallet running")
                    var begin=Date.now();
                    startFNCycle(function(){
                        console.log("Finished");
                        var end= Date.now();
                        var timeSpent=(end-begin)/1000;
                        console.log(timeSpent + " seconds");
                        autoRun(timeSpent)
                    })
                }

            })
        },5000);
    });
});

stdin.addListener("data", function(d) {
    // note:  d is an object, and when converted to a string it will
    // end with a linefeed.  so we (rather crudely) account for that
    // with toString() and then trim()
    if(d.toString().trim()==='q'){
        process.exit(1);
    }
});
