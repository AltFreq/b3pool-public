var RpcClient = require('b3coind-rpc');
const async = require("async");
var mysql = require('mysql');
const Webhook = require("webhook-discord");
const Hook = new Webhook("https://discordapp.com/api/webhooks/487506950062604290/3dHGAOjGQuyCodyxq5Ylv09m4ph17xL5zf02onz17ZKws8Nnaetf53Zd20s8SzEioz9j");
var client = null;

var mainWallet = 'SVuB5BJymcSSaFgzrAmazqhaf1WLUYnbGg';


var memberList = [];
var storedPayments = {};
var poolTotal =0;

var maxTxSize = 10000
var minTxSize = 78
var inputTxSize = 148


var database = mysql.createConnection({
    host: "67.205.148.48",
    user: "home",
    password: "Ms016356!01",
    database: "d3xPool"
});

function setManagerClient(callback){
    var config = {
        protocol:'http',//Optional. Will be http by default
        host:'127.0.0.1',//Will be 127.0.0.1 by default
        user:'AltFreq',//Optional, only if auth needed
        pass:'Ms016356!01',//Optional. Can be named 'pass'. Mandatory if user is passed.
        port:8998,//Will be 8443 for https or 8080 for http by default
    };
    client = new RpcClient(config);
    callback();
}
console.log("Starting node payment service ");

function sendMessageFunc(val, nodeIndex, txids, callback){
    var string = "Reward of **"+val+"kB3** paid";
    for(var i = 0; i < txids.length; i++){
        string += "\n[View Transaction](https://chainz.cryptoid.info/b3/tx.dws?"+txids[i]+")";
    }
    Hook.custom("B3FNPool",string,"Node "+nodeIndex,"#"+dec2hex(val));
    callback();
}

function dec2hex(i) {
    var color= (i+0x10000).toString(16).substr(-6).toUpperCase();
    if(color.length<6){
        return "FFFFFF"
    }else{
        return color;
    }
}

function getMemberList(finished){
    console.log("Getting rewards list");
    var sql = "SELECT * FROM Temporary_Member";
    poolTotal = 0;
    database.query(sql, function (err, result, fields) {
        if (err) throw err;
        async.each(result, function(member, callback) {
            var wallet_Address = member.Wallet_Address;
            var lottery_Amount = member.Lottery_Amount;
            var direct_Amount = member.Direct_Amount;
            var calculated_Amount = member.Total_Amount;
            poolTotal+=lottery_Amount+direct_Amount;
            memberList.push({Wallet_Address: wallet_Address, Amount: calculated_Amount});
            callback();
        }, function(err) {
            if( err ) {
                console.error('Error');
            } else {
                console.log("Loaded members");
                console.log("Pool total =" +poolTotal)
                finished();
            }
        });
    });
}



// when recording txs update local to db


function getStoredPayments(finished){
    console.log("Getting Stored Payments");
    var sql = "SELECT * FROM Temporary_Owed";
    database.query(sql, function (err, result, fields) {
        if (err) throw err;
        async.each(result, function(member, callback) {
            var wallet_Address = member.Wallet_Address;
            var amount = member.Amount
            storedPayments[wallet_Address] = amount;
            callback();
        }, function(err) {
            if( err ) {
                console.error('Error');
            } else {
                console.log("Loaded stored Payments");
                finished();
            }
        });
    });
}

function calculateFees(payments, callback){
    var size = minTxSize
    for(var i = 0; i < payments; i++){
        size+=inputTxSize;
    }
    var fee = 0.001;
    //    fee+=(Math.floor(size/1000)*0.0001);
    console.log(fee);
    console.log("Total fees = " + fee);
    callback(fee);
}

function getRewardsList(reward_Amount, finished){
    var rewardsList = {};
    console.log("Reward " + reward_Amount)
    async.each(memberList, function(member, callback) {
        var wallet_Address = member.Wallet_Address;
        var percentage_Owned = member.Amount/poolTotal;
        var reward = percentage_Owned*reward_Amount;
        var stored = false;
        if(reward<0.001){
            if(wallet_Address in storedPayments){
                storedPayments[wallet_Address]+=reward
            }else{
                storedPayments[wallet_Address] = reward;
            }
        }else{
            rewardsList[wallet_Address]= reward;
        }
        callback();
    }, function(err) {
        if( err ) {
            console.error('Error');
        } else {
            console.log("rewards list created");
            console.log("RewardsList " + Object.keys(rewardsList).length);
            finished(rewardsList);
        }
    });
}
function unlockForStaking(callback){
    client.walletPassPhrase("Ms016356!01",[10000000, true],function(err,res){
        if (err){
            console.log(err);
            throw err;
        }else{
            callback();
        }
    });
}

function unlockWallet(callback){
    client.walletPassPhrase("Ms016356!01",[1000],function(err,res){
        if (err){
            console.log(err);
            throw err;
        }else{
            callback();
        }
    });
}

function sendRewards(group, transaction_ID, reward_Output, rewards, callback){
    console.log("creating raw tx");
    var inputs = [{"txid":transaction_ID, "vout": reward_Output}]
    client.createRawTransaction(inputs,rewards, function(err,res){
        if (err) throw err;
        var rawTx = res.result;
        console.log("Raw transaction created")
        console.log("unlocking wallet");
        unlockWallet(function(){
            console.log("Wallet unlocked")
            console.log("Signing transaction")
            client.signRawTransaction(rawTx,function(err,res){
                if (err){
                    console.log(err);
                    throw err;
                }
                var signedTransaction = res.result["hex"];
                console.log("Signed");
                console.log("Sending Transaction");

                client.sendRawTransaction(signedTransaction,function(err,res){
                    if (err){
                        console.log(err);
                        throw err;
                    }
                    console.log("Transaction sent");
                    console.log(res.result);
                    var sentTransaction = res.result;
                    unlockForStaking(function(){
                        //update paid if all done with paying out
                        var sql = "UPDATE Node_Rewards SET Paid = 1 WHERE Transaction_ID = ?";
                        database.query(sql, [transaction_ID], function (err, result) {
                            if (err) throw err;
                            var values = []
                            console.log("Creating values "+Object.keys(rewards).length);
                            for (var key in rewards) {
                                var wal = key;
                                var amt = rewards[key];
                                values.push([transaction_ID+'-'+group, sentTransaction, amt, wal]);
                            }
                            //321 of vals to be paid.
                            sql = "INSERT INTO Node_Payment_History (Reward_Transaction, Paid_Transaction, Paid_Amount, Member_Rewarded) VALUES ?";
                            console.log("Writing to db");
                            database.query(sql, [values], function (err, result) {
                                if (err) throw err;
                                callback(sentTransaction);
                            });
                        });
                    });
                });
            });
        });
    });
}

function updateStored(callback){
    var values = []
    console.log("Creating values for stored payments "+Object.keys(storedPayments).length);
    for (var key in storedPayments) {
        var wal = key;
        var amt = storedPayments[key];
        values.push([wal,amt]);
    }
    if(values.length>0){
        var sql = "REPLACE INTO Temporary_Owed (Wallet_Address, Amount) VALUES ?";
        console.log("Writing to db");
        database.query(sql, [values], function (err, result) {
            if (err) throw err;
            callback();
        });
    }else{
        callback();
    }
}

function findSuitableInput(amountNeeded, callback){
    var totalFound = 0;
    var inputs  = []
    console.log("Finding suitable input for amount " + amountNeeded);
    client.listUnspent(function(err,res){
        var listOfUnspent = res.result;
        for(var i = 0; i < listOfUnspent.length; i++){
            var item = listOfUnspent[i];
            var address = item["address"];
            if(address === 'SVuB5BJymcSSaFgzrAmazqhaf1WLUYnbGg'){
                var confimations = item["confirmations"]
                if(confimations>=1){
                    var amount = item["amount"];
                    if(amount<1000){
                        var txid = item["txid"];
                        var vout = item["vout"];
                        totalFound+=amount;
                        inputs.push({"txid":txid, "vout": vout});
                        if(totalFound>=amountNeeded){
                            console.log("inputs found - val="  + totalFound);
                            callback([inputs,totalFound])
                            break;
                        }
                    }
                }
            }
        }
    });
}



function payStoredPayments(callback){
    var paymentList = {};
    var totalVal = 0;
    console.log("Creating values for stored payments to pay "+Object.keys(storedPayments).length);
    for (var key in storedPayments) {
        var wal = key;
        var amt = storedPayments[key];
        if(amt> 0.001){
            totalVal+=amt;
            paymentList[wal] = amt;
            delete storedPayments[wal]
        }
    }
    var sql = "DELETE FROM Temporary_Owed WHERE Amount > 0.001";
    console.log("Deleting from DB");
    database.query(sql, function (err, result) {
        if (err) throw err;
        callback();
    });
}

function checkNodeRewards(){
    console.log("Checking node rewards")
    var sql = "SELECT * FROM Node_Rewards WHERE Paid = 0";
    database.query(sql, function (err, result, fields) {
        if (err) throw err;
        if(result.length>0){
            console.log("Rewards - " +result.length)
            async.eachSeries(result, function(nodeReward, callback) {
                var transaction_ID = nodeReward.Transaction_ID;
                var node_ID = nodeReward.Node_ID;
                var reward_Amount = nodeReward.Amount;
                var mainWalletValue = 0;
                var totalSpent = 0;
                client.getTransaction(transaction_ID,function(err,res){
                    if(res!=undefined){
                        var confirmations = res.result["confirmations"];
                        var reward_Output = res.result.vout.length-1
                        if(confirmations>=31){
                            console.log("Pay - "+ transaction_ID);
                            getRewardsList(reward_Amount, function(rewards){
                                            //split rewards list /100
                                var multiRewards = []
                                var rewardCounter= 0;
                                var rewardPos = 0
                                var rewardList = {}
                                var mainWalletReward = 0
                                for(reward in rewards){
                                    if(reward!==mainWallet){
                                        rewardCounter+=1
                                        rewardList[reward] = rewards[reward]
                                        if(rewardCounter===66 || rewardPos*66+rewardCounter==Object.keys(rewards).length-1){
                                            multiRewards[rewardPos] = rewardList
                                            rewardList = {}
                                            rewardCounter=0
                                            rewardPos+=1
                                        }
                                    }
                                }
                                //add main wallet to last group
                                //***
//                                console.log(multiRewards)
                                for(var i =  0; i < multiRewards.length; i++){
                                    var rewardGroup = multiRewards[i]
                                    for(user in rewardGroup){
                                                totalSpent+=rewardGroup[user];
                                    }
                                    console.log(Object.keys(rewardGroup).length);
                                    calculateFees(Object.keys(rewardGroup).length,function(fee){
                                    totalSpent+=fee

                                    console.log("Total spent including fees = " + totalSpent);
                                    console.log("Left over = " + (reward_Amount-totalSpent));
                                    if(i===multiRewards.length-1){
                                        //shouldnt need anymore
                                        var overspent =totalSpent-reward_Amount;
                                        if(overspent>0){
                                            //TODO need to fix up mainWallet
                                            if(overspent>mainWalletReward){
                                                console.log("Error making payment not enough funds in main to cover")
                                            }else{
                                                console.log("Overspent reducing income from main")
                                                mainWalletReward -=overspent;
                                            }

                                        }else{
                                            console.log("Leftover found adding to main wallet")
                                            console.log("Main balance " + mainWalletReward);
                                            mainWalletReward=mainWalletReward+(reward_Amount-totalSpent);
                                            console.log("Main balance" + mainWalletReward)
                                        }
                                    }else{
                                        //add change address
                                        multiRewards[i][mainWallet] = reward_Amount-totalSpent
                                    }
                                });
                                }
                                //add main wallet //check if 67
                                if(Object.keys(multiRewards[rewardPos-1]).length===67){
                                    var newGroup = []
                                    newGroup[mainWallet]=mainWalletReward-0.0001
                                    multiRewards[rewardPos] = newGroup

                                }else{
                                    multiRewards[rewardPos-1][mainWallet]=mainWalletReward
                                }
                                console.log(multiRewards)
                                //****





                                var previousTxID = transaction_ID
                                var previousRewardOut = reward_Output
                                var transactionIds = []
                                var groupCounter = 0
                                async.eachSeries(multiRewards, function(multiReward, callback) {
                                    sendRewards(groupCounter, previousTxID, previousRewardOut, multiReward, function(sentTransaction){
                                        console.log("Sent Transaction = " + sentTransaction)
                                        previousTxID = sentTransaction//find output
                                        groupCounter+=1;
                                        transactionIds.push(sentTransaction)
                                        client.getTransaction(sentTransaction,function(err,res){
                                            console.log("Searched transaction")
                                            for(var out in res.result.vout){
                                                if(res.result.vout[out].scriptPubKey.addresses[0]===mainWallet){
                                                   previousRewardOut = res.result.vout[out].n;
                                                   console.log("Calling back")
                                                   callback();
                                                }
                                            }
                                        });
                                    });
                                },function(){
                                    console.log("---Sending message---")
                                    sendMessageFunc(reward_Amount, node_ID, transactionIds, function(){
                                        console.log("Message sent");
                                        callback();
                                    });
                                });
                            });
                        }else{
                            console.log("Confirmations too low - " + confirmations);
                            callback();
                        }
                    }else{
                        console.log("Error")
                        console.log(err);
                        callback();
                    }
                });
            }, function(err) {
                if( err ) {
                    console.error('Error');
                } else {
                    console.log("Paid all rewards");
                    console.log("Checing stored");
                    payStoredPayments(function(){
                        updateStored(function(){
                            console.log("Stored owed payments");
                            storedPayments = {};
                            memberList = [];
                            poolTotal = 0;
                            reRun();
                        })
                    })
                }
            });
        }else{
            console.log("No rewards");
            storedPayments = {};
            memberList = [];
            poolTotal = 0;
            reRun();
        }
    });
}

function reRun(){
    console.log("Waiting an hour to check again");
    setTimeout(function(){
        getMemberList(function(){
            getStoredPayments(function(){
                checkNodeRewards();
            })
        });
    },1800000)
}
setManagerClient(function(){
    database.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        getMemberList(function(){
            getStoredPayments(function(){
                checkNodeRewards();
            })
        });
    });

});
