var mysql = require('mysql');
var RpcClient = require('b3coind-rpc');
const async = require("async");


var old = mysql.createConnection({
    host: "mysql_server",
    user: "home",
    password: "Ms016356!01",
    database: "b3_lottery"
});

var newDB = mysql.createConnection({
    host: "mysql_server",
    user: "home",
    password: "Ms016356!01",
    database: "d3xPool"
});


function load_OwedPayments(callback){
    old.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        old.query("SELECT * FROM node_payments_owed", function (err, result, fields) {
            if (err) throw err;
            var values = []
            for(var i = 0; i < result.length; i++){
                values.push(Object.values(result[i]));
            }
            if (err) throw err;
            console.log("Old disconnected")
            newDB.connect(function(err){
                if (err) throw err;
                console.log("New Connected!");
                var sql = "INSERT INTO Temporary_Owed (Wallet_Address, Amount) VALUES ?";
                newDB.query(sql, [values], function (err, result) {
                    if (err) throw err;
                    console.log("Number of records inserted: " + result.affectedRows);
                    callback();
                });
            });
        });
    });
}
//    Wallet_Address: 'Sib59Lyeh2iybN8CCXVucTbNwirD1BaKrd',
//    Amount: 2078.8802167,
//    Direct_Deposit: 0 },
function loadTempMembers(callback){
    old.query("SELECT * FROM node_owners", function (err, result, fields) {
        if (err) throw err;
        var values = [];
        for(var i = 0; i < result.length; i++){
            values.push(Object.values(result[i]));
        }
        if (err) throw err;
        console.log("Old disconnected")
        console.log("New Connected!");
        var sql = "INSERT INTO Temporary_Member (Wallet_Address, Lottery_Amount, Direct_Amount) VALUES ?";
        newDB.query(sql, [values], function (err, result) {
            if (err) throw err;
            console.log("Number of records inserted: " + result.affectedRows);
            old.end(function(err){
                if (err) throw err;
                newDB.end(function(err){
                    if (err) throw err;
                    callback();
                });
            });
        });
    });
}

load_OwedPayments(function(){
    loadTempMembers(function(){
        console.log("done")
    });
})
