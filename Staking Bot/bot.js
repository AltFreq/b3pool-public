//TODO FIX node balances set via block data for stake difference1

var Discord = require('discord.io');
var auth = require('./auth.json');
const async = require("async");
var RpcClient = require('b3coind-rpc');

const webhook = require("webhook-discord")
const stakingHook = new webhook.Webhook("https://discordapp.com/api/webhooks/546037637618597888/Wkkt6OT0X156s29q0Le53zHtddq35upW_9JQrvyYsZh6fcQQAWnDLKPFwILljmC4l7Xi");
const nodeHook = new webhook.Webhook("https://discordapp.com/api/webhooks/488131258442973184/wgkwC1O50f6DHWN0h9QZ4ozoASfk2mQz-T0bfpBkf-TllQq7ARr6RxFwTHtSVwcJnADP");

var ownerPercentage = 0.08
var referralPercentage = 0.02
var defaultChannelID = "491721460516257832"
var stakingChannelID = "539258434160427008"
var ownerID = '220488952325341185';
var botID = '530333073984978954';
var airDropRunning = false;
var airDropOwner;
var airDropMessage;
var airDropWinners = []
var airDropEntrants = [];
var airDropObjective;
var airDropAmount;
var airDropMinEntrants = 1
var airDropTime = 3600000;
var reactDropRunning = false;
var lotteryDropRunning = false;
var rollDropRunning = false;
var rollDropHighestValue = -1;
var phraseDropRunning = false;
var inviteList = {};

var nodeDonationAddress = 'STuH1dTuq7CoJtJtmbNbB6U8vFg56P7taw';
var donationPrivkey = 'PfHA983fxq8s79mCGwEBfvqcDDnscbHVJyiKFq1m8qYgkXnnfxNz';

var stakingAddresses = [];
var pendingStakes = [];
var stakingMaxBalance = 5000000;
var stakingMinConf = 50;
var pendingTransactionOutputs = [];

const stdin = process.openStdin();


var depositConfig = {
  protocol: 'http', //Optional. Will be http by default
  host: '127.0.0.1', //Will be 127.0.0.1 by default
  user: 'AltFreq', //Optional, only if auth needed
  pass: 'Ms016356!01', //Optional. Can be named 'pass'. Mandatory if user is passed.
  port: 8000, //Will be 8443 for https or 8080 for http by default
};
var stakingConfig = {
  protocol: 'http', //Optional. Will be http by default
  host: '127.0.0.1', //Will be 127.0.0.1 by default
  user: 'AltFreq', //Optional, only if auth needed
  pass: 'Ms016356!01', //Optional. Can be named 'pass'. Mandatory if user is passed.
  port: 8001, //Will be 8443 for https or 8080 for http by default
};
var wallet = new RpcClient(depositConfig);
var stakingWallet = new RpcClient(stakingConfig)
var bot = new Discord.Client({
  token: auth.token,
  autorun: true
});

const sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database('staking-bot.db', (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the staking-bot database.');
  db.serialize(() => {
    db.run("CREATE TABLE IF NOT EXISTS member(id TEXT PRIMARY KEY, pending_balance REAL DEFAULT 0, available_balance REAL DEFAULT 0, staking_balance REAL DEFAULT 0, earnt_stakes REAL DEFAULT 0, earnt_referrals REAL DEFAULT 0, count_stakes INT DEFAULT 0, count_referrals INT DEFAULT 0, default_withdrawal TEXT, referred_by TEXT)");
    db.run("CREATE TABLE IF NOT EXISTS referrals(member_id TEXT, referred_by TEXT, PRIMARY KEY(member_id), FOREIGN KEY (member_id) REFERENCES member(id) ON DELETE CASCADE ON UPDATE NO ACTION, FOREIGN KEY (referred_by) REFERENCES member(id) ON DELETE CASCADE ON UPDATE NO ACTION)");
    db.run("CREATE TABLE IF NOT EXISTS address(member_id TEXT, deposit_address TEXT(36), PRIMARY KEY(deposit_address), FOREIGN KEY (member_id) REFERENCES member(id) ON DELETE CASCADE ON UPDATE NO ACTION)");
    db.run("CREATE TABLE IF NOT EXISTS pending_deposits(transaction_id TEXT(65), receiving_id TEXT, amount REAL, PRIMARY KEY(transaction_id), FOREIGN KEY (receiving_id) REFERENCES member(id) ON DELETE CASCADE ON UPDATE NO ACTION)");
    db.run("CREATE TABLE IF NOT EXISTS confirmed_deposits(transaction_id TEXT(65), receiving_id TEXT, amount REAL, PRIMARY KEY(transaction_id), FOREIGN KEY (receiving_id) REFERENCES member(id) ON DELETE CASCADE ON UPDATE NO ACTION)");
    db.run("CREATE TABLE IF NOT EXISTS withdrawals(transaction_id TEXT(65), member_id TEXT, to_address TEXT(36), amount REAL, PRIMARY KEY(transaction_id), FOREIGN KEY (member_id) REFERENCES member(id) ON DELETE CASCADE ON UPDATE NO ACTION)");
    db.run("CREATE TABLE IF NOT EXISTS pending_stakes(transaction_id TEXT(65),PRIMARY KEY(transaction_id))");
    db.run("CREATE TABLE IF NOT EXISTS confirmed_stakes(address TEXT(36), transaction_id TEXT(65), time TEXT, amount REAL, PRIMARY KEY(transaction_id))");
    db.run("CREATE TABLE IF NOT EXISTS pending_unstake(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, member_id TEXT, amount REAL)");
    db.run("CREATE TABLE IF NOT EXISTS staking_addresses(address TEXT(36),  waiting boolean DEFAULT 0, last_transaction TEXT(65), last_stake_transaction TEXT(65))");
    db.run("CREATE TABLE IF NOT EXISTS confirmed_stake_deposits(transaction_id TEXT(65),PRIMARY KEY(transaction_id))");
    db.run("CREATE TABLE IF NOT EXISTS paid_node_rewards(transaction_id TEXT(65), PRIMARY KEY(transaction_id))")
    loadDatabaseData();
  });
});

function loadDatabaseData(){
  stakingAddresses=[];
  pendingStakes=[];
  db.all("SELECT * FROM staking_addresses", (err, rows) => {
    if (err) {
      console.error(err.message);
    }
    if (rows.length === 0) {
      console.log("No staking address");
      getNewStakingAddress(false, function(stakingAddress) {
        stakingAddresses.push({
          address: stakingAddress,
          //                    balance: 0,
          waiting: false,
          lastTx: null,
          lastStakingTx: null
        })
        console.log("Staking wallet address generated " + stakingAddress);
      });
      getNewStakingAddress(true, function(stakingAddress) {
        stakingAddresses.push({
          address: stakingAddress,
          //                    balance: 0,
          waiting: true,
          lastTx: null,
          lastStakingTx: null
        })
        console.log("Staking wallet address generated " + stakingAddress);
      });
    } else {
      rows.forEach((row) => {
        var data = {
          address: row.address,
          //                    balance: row.balance,
          waiting: row.waiting,
          lastTx: row.last_transaction,
          lastStakingTx: row.last_stake_transaction

        };
        stakingAddresses.push(data);
      });
      console.log(stakingAddresses.length + " staking addresses loaded");
      loadPendingStakes();
    }
  });
}

bot.on('ready', function(evt) {
  console.log('Logged in to discord as: ' + bot.username + ' - (' + bot.id + ')');
});

bot.on('ready', function(evt) {
  updateExistingMembers();
});

//check for new members
bot.on('ready', function(evt) {
  console.log("New users = " + Object.keys(bot.users).length);
  let sql = 'SELECT 1 FROM member where id=?';
  db.parallelize(() => {
    async.each(bot.users, function(user) {
      db.get(sql, [user.id], (err, row) => {
        if (err) {
          return console.error(err.message);
        }
        if (!row) {
          console.log(user.id + " does not exist creating profile");
          addUserToDatabase(user.id, ownerID);
        }
      });
    });
  });
});

//TODO
bot.on('disconnect', function(errMsg, code) {
  bot.connect()
});

//load invites
bot.on('ready', function(evt) {
  bot.getServerInvites('388793731907846165', function(err, res) {
    var invites = res
    async.each(invites, function(inviteData, callback) {
      inviteList[inviteData.code.toString()] = [inviteData.inviter.username, inviteData.uses];
      callback();
    }, function() {
      console.log("Server invites loaded - " + Object.keys(inviteList).length);
    });
  });
});

//Invite event
bot.on('guildMemberAdd', function(member) {
  bot.getServerInvites('388793731907846165', function(err, res) {
    var invites = res
    async.each(invites, function(inviteData) {
      var previousValue = inviteList[inviteData.code.toString()];
      if (!previousValue) {
        console.log("new invite");
        inviteList[inviteData.code.toString()] = [inviteData.inviter.username, inviteData.uses];
        previousValue = 0
      } else {
        previousValue = previousValue[1];
      }
      var newValue = inviteData.uses;
      if (newValue > previousValue) {
        inviteList[inviteData.code.toString()][1] = previousValue + 1;
        let sql = 'SELECT 1 FROM member where id=?';
        let memberId = member.id;
        db.get(sql, [memberId], (err, row) => {
          if (err) {
            return console.error(err.message);
          }
          if (row) {
            console.log("Member rejoined " + bot.users[member.id].username + " - " + member.id + " invited by " + inviteData.inviter.username);
          } else {
            console.log("New member joined " + bot.users[member.id].username + " - " + member.id + " invited by " + inviteData.inviter.username);
            addUserToDatabase(member.id, inviteData.inviter);
          }
        });
      }
    });
  });
});

// socket wallet server
require('net').createServer(function(socket) {
  socket.on("error", function(err){
    console.log("Caught socket error: ")
    console.log(err.stack)
  });
  console.log("Wallet socket connected");
  socket.on('data', function(data) {
    getTransaction(data, function(transaction) {
      if (!transaction) {
        console.log("Error getting transaction");
      } else {
        var txid = transaction.txid;
        var confirmations = transaction.confirmations
        var userID = transaction.details[0].account
        var amount = transaction.details[0].amount
        var category = transaction.details[0].category
        console.log(category + " " + amount + " " + txid);
        if (category !== "send") { //TODO getting called twice on stake
          if (confirmations === 0) {
            db.parallelize(() => {
              db.run("INSERT OR IGNORE INTO pending_deposits(transaction_id, receiving_id, amount) VALUES (?,?,?)", [txid, userID, amount], function(err) {
                if (err) {
                  return console.log(err.message);
                }
                console.log(`A row has been inserted with rowid ${this.lastID}`);
              });
              db.run("UPDATE member SET pending_balance=pending_balance+? WHERE id=?", [amount, userID], function(err) {
                if (err) {
                  return console.log(err.message);
                }
                console.log(`A row has been updated with rowid ${this.lastID}`);
              });
            });
            sendEmbededDirectMessage(getTransactionFields(txid, confirmations, amount), userID, userID);
          } else {
            db.parallelize(() => {
              db.run("DELETE FROM pending_deposits WHERE transaction_id=?", [txid], function(err) {
                if (err) {
                  return console.log(err.message);
                }
                console.log(`A row has been deleted with rowid ${this.lastID}`);
              });
              db.run("INSERT OR IGNORE INTO confirmed_deposits(transaction_id, receiving_id, amount) VALUES (?,?,?)", [txid, userID, amount], function(err) {
                if (err) {
                  return console.log(err.message);
                }
                console.log(`A row has been inserted with rowid ${this.lastID}`);
              });
              db.run("UPDATE member SET available_balance=available_balance+?, pending_balance =CASE WHEN pending_balance-?>0 THEN (pending_balance-?) ELSE 0 END WHERE id=?", [amount, amount, amount, userID], function(err) {
                if (err) {
                  return console.log(err.message);
                }
                console.log(`A row has been updated with rowid ${this.lastID}`);
              });
              sendEmbededDirectMessage(getTransactionFields(txid, confirmations, amount), userID, userID);
            });
          }
        }
      }
    });
  });
}).listen(9005);

require('net').createServer(function(socket) {
  socket.on("error", function(err){
    console.log("Caught socket error: ")
    console.log(err.stack)
  });
  console.log("Staking Socket connected");
  socket.on('data', function(data) {
    getStakingWalletTransaction(data, function(transaction) {
      if (!transaction) {
        console.log("Error getting transaction");
      } else {
        var txid = transaction.txid;
        var confirmations = transaction.confirmations
        var amount;
        var vout;
        var address = transaction.details[transaction.details.length - 1].address
        for (var i = 0; i < transaction.vout.length; i++) {
          if (transaction["vout"][i]["scriptPubKey"]["addresses"]!=undefined && transaction["vout"][i]["scriptPubKey"]["addresses"][0] === address) {
            amount = transaction.vout[i].value;
            vout = i;
          }
        }
        var category = transaction.details[transaction.details.length - 1].category
        var recombined = transaction.details.length === 2;
        var pendingOutput = transaction.details.length === 3;
        if (!transactionIsPending(txid) && category!=="send") {
          isCompletedStakingTransaction(txid, function(completed){
            if(!completed){
              console.log("Adding pending transaction to staking wallet")
              pendingStakes.push({
                txid: txid,
                confirmations: confirmations,
                amount: amount,
                vout: vout,
                category: category,
                address: address,
                recombined: recombined,
                pendingOutput: pendingOutput
              });
              db.run('INSERT INTO pending_stakes(transaction_id) VALUES(?)', [txid], function(err) {
                if (err) {
                  return console.log(err.message);
                }
                console.log(`A row has been inserted into pending_stakes with rowid ${this.lastID}`);
              });
            }
          });
        }
      }
    });
  });
}).listen(9001);

function getCombineStakeAddressesOnly(callback){
  var list = []
  async.each(getStakingAddresses(), function(stakingAddress, callback){
    checkForCombine(stakingAddress.address, function(combine){
      if(combine){
        list.push(stakingAddress.address);
      }
      callback();
    });
  },function(){
    callback(list)
  });
}


function handlePendingUnstakes(callback){
  var spent = [];
  console.log("Handling pending")
  db.all("SELECT * FROM pending_unstake", (err, rows) => {
    if(rows.length===0){
      console.log("no pending unstakes")
      callback("no pending unstakes")
    }else{
      getOutputsForAddresses(getWaitingAddressesOnly(), function(waitingUnspent){
        getCombineStakeAddressesOnly(function(stakingAddresses){
          getOutputsForAddresses(stakingAddresses, function(stakedUnspent){
            console.log("Selected pending unstakes")
            console.log("Selected pending " + rows.length )
            async.eachSeries(rows, function(row, callback){
              var inputsToUse = []
              var inputTotal=0
              var balancesToDecrease = {}
              var changeAddress;
              for(var i = 0; i < pendingTransactionOutputs.length && inputTotal<row.amount+getTransactionFee(inputsToUse.length); i++){
                if(!spent.includes(pendingTransactionOutputs[i].txid)){
                  console.log("Spent !include " + pendingTransactionOutputs[i].txid)
                  inputsToUse.push(pendingTransactionOutputs[i]);
                  spent.push(pendingTransactionOutputs[i].txid);
                  inputTotal+=pendingTransactionOutputs[i].amount
                }
              }
              if(inputTotal<row.amount+getTransactionFee(inputsToUse.length)){
                for(var i = 0; i < waitingUnspent.length && inputTotal<row.amount+getTransactionFee(inputsToUse.length); i++){
                  if(!spent.includes(waitingUnspent[i].txid)){
                    console.log("Spent !include " + waitingUnspent[i].txid)
                    inputsToUse.push(waitingUnspent[i]);
                    spent.push(waitingUnspent[i].txid);
                    inputTotal+=waitingUnspent[i].amount
                    if(!Object.keys(balancesToDecrease).includes(waitingUnspent[i].address)){
                      balancesToDecrease[waitingUnspent[i].address] = waitingUnspent[i].amount
                    }else{
                      balancesToDecrease[waitingUnspent[i].address]+=waitingUnspent[i].amount
                    }
                  }
                }
              }
              if(inputTotal<row.amount+getTransactionFee(inputsToUse.length)){
                for(var i = 0; i < stakedUnspent.length && inputTotal<row.amount+getTransactionFee(inputsToUse.length); i++){
                  if(!spent.includes(stakedUnspent[i].txid)){
                    console.log("Spent !include " + stakedUnspent[i].txid)
                    inputsToUse.push(stakedUnspent[i]);
                    spent.push(stakedUnspent[i].txid);
                    inputTotal+=stakedUnspent[i].amount
                    if(!Object.keys(balancesToDecrease).includes(stakedUnspent[i].address)){
                      changeAddress = stakedUnspent[i].address;
                      balancesToDecrease[stakedUnspent[i].address] = stakedUnspent[i].amount
                    }else{
                      balancesToDecrease[stakedUnspent[i].address]+=stakedUnspent[i].amount
                    }
                  }
                }
              }
              console.log("getting deposit address")
              if(!changeAddress){
                changeAddress=getStakingAddresses()[0].address;
              }
              getUserDepositAddress(row.member_id, function(err, userDepositAddress){
                var receiving_addresses = {};
                receiving_addresses[userDepositAddress] = Math.min(row.amount,inputTotal-getTransactionFee(inputsToUse.length));
                if(inputTotal-row.amount>0){
                  if(inputTotal-getTransactionFee(inputsToUse.length)-row.amount>0){
                    receiving_addresses[changeAddress] = Math.round((inputTotal-getTransactionFee(inputsToUse.length)-row.amount)*10000)/10000;
                  }else{
                    console.log("Error with amounts")
                  }
                }
                console.log("Creating tx")
                console.log(inputsToUse);
                console.log("Receiving");
                console.log(receiving_addresses);
                createRawTransaction(inputsToUse, receiving_addresses, function(hexString) {
                  console.log("Hex string generated " + hexString);
                  if (!hexString) {
                    console.log("Error creating raw")
                    callback("Error creating raw transaction")
                  } else {
                    signRawTransaction(hexString, function(hexString) {
                      if (!hexString) {
                        console.log("Error signing")
                        callback("Error signing raw transaction")
                      } else {
                        console.log("Raw signed");
                        sendRawTransaction(hexString, function(txID) {
                          if (!txID) {
                            console.log("Error sending raw")
                            callback("Error sending raw transaction")
                          } else {
                            console.log("Sent tx " + txID)
                            //update balance on sending address
                            console.log("Decreasing balance!~~")
                            //                                                        Object.keys(balancesToDecrease).forEach((data)=>{
                            //                                                            increaseAddressBalance(data, -balancesToDecrease[data]);
                            //                                                        });
                            storePendingOutput(txID, changeAddress)
                            var sql;
                            if(row.amount<inputTotal){
                              sql = "DELETE FROM pending_unstake where id=?";
                              db.run(sql, [row.id], function(err){
                                if(err){
                                  console.log(err);
                                }
                              })
                            }else{
                              sql = "UPDATE pending_unstake SET amount=? WHERE id=?"
                              db.run(sql, [row.amount-(inputTotal-getTransactionFee(inputsToUse.length)), row.id], function(err){
                                if(err){
                                  console.log(err);
                                }
                              })
                            }
                            removeStakeBalance(row.member_id,  Math.min(row.amount,inputTotal-getTransactionFee(inputsToUse.length)), function(){
                              if(err){
                                callback(err)
                              }
                              for(var i = 0; i < spent.length; i++){
                                if(pendingTransactionOutputs.includes(spent[i])){
                                  pendingTransactionOutputs.splice(i,1);
                                }
                              }
                              callback(null);
                            })
                          }
                        })
                      }
                    })
                  }
                });
              })
            }, function(err){
              callback(err);
            });

          });

        });
      })
    }
  });
  //    load pending unstakes
  //    getwaitingaddress unspent
  //    if not enough load staked address unspent
  //    if still not enough do partial unstake
  //
  //    store results in pendingTransactionOutputs //storependingoutput
}

var nodeRewards = [];

function payoutNodeStakes(txid,index,amount, callback){
  db.get("SELECT * FROM paid_node_rewards WHERE transaction_id =?",[txid], (err, rows) => {
    if (err) {
      console.error(err.message);
    }
    console.log("ROWS = ")
    console.log(rows);
    if (!rows) {
      amount = Math.round(amount*(1.00-ownerPercentage+referralPercentage));
      if(amount <=0){
        amount = 1;
      }
      console.log("Amount " + amount);
      var sql = "UPDATE member set available_balance = CASE id \
      WHEN '220488952325341185'\
      THEN available_balance + round((?-(?/((SELECT sum(node_balance) FROM member)+(SELECT sum(Amount)+sum(Direct_Deposit) from node_owners))*((SELECT sum(node_balance) FROM member)-node_balance))),5)\
      ELSE available_balance + round((?/((SELECT sum(node_balance) FROM member)+(SELECT sum(Amount)+sum(Direct_Deposit) from node_owners))*node_balance),5)\
      END";
      db.run(sql, [amount, amount, amount], function(err){
        if(err){
          console.log(err);
        }
        db.run("INSERT INTO paid_node_rewards (transaction_id) VALUES (?)", [txid], function(err){
          if(err){
            console.log(err)
          }
          console.log("Paid reward and added to db " + txid);
          callback(index)
        })
      })
    }else{
      callback(index)
    }
  });
}

var recombineNextBlock = false;
var rewardedAddress = '';

function checkForNodeReward(blocktx, callback){
  var addressesToRecombine = [];
  var completed = [];
  console.log("Checking for node reward " + blocktx);
  if(!recombineNextBlock){
    for(var i = 0; i < nodeRewards.length; i++){
      var data = nodeRewards[i];
      data.confirmations+=1;
      if(data.confirmations>32){
        sendNodeStakeMessage(data.txid, data.amount)
        payoutNodeStakes(data.txid,i,data.amount, function(index){
          console.log("removing node reward")
          nodeRewards.splice(index,1);
          recombineNextBlock = true;
          rewardedAddress = data.address;
        });
      }
    }
    callback([], []);
  }else{
    recombineNextBlock = false;
    async.forEach(getWaitingAddresses(), function(data, callback){
      getAddressBalance(data.balance, function(balance){
        if(balance>0 && !addressesToRecombine.includes(data.address)){
          addressesToRecombine.push(data.address);
        }
        callback();
      })
    }, function(){
      if(addressesToRecombine.length>0 && !addressesToRecombine.includes(rewardedAddress)){
        addressesToRecombine.push(rewardedAddress);
      }
      callback(addressesToRecombine);
    })

  }
  wallet.getblock(blocktx, function(err, res){
    if(err){
      console.log(blocktx);
      console.log(err)
    }else{
      var rawBlock = res.result;
      async.each(rawBlock["tx"], function(txid){
        wallet.getTransaction(txid, function(err, res){
          if(err){
            console.log(err)
          }else{
            async.each(res.result['vout'], function(output){
              var amount = output['value']
              if(amount>0){
                if(output['scriptPubKey']["addresses"]!=undefined){
                  var address = output['scriptPubKey']["addresses"][0];
                  if(address === nodeDonationAddress){
                    console.log("Node reward received " + amount);
                    nodeRewards.push({confirmations: rawBlock["confirmations"], address:address, amount: amount, txid: txid});
                  }
                }
              }
            });
          }
        })
      })
    }
  });
}

function getAddressBalance(address, callback){
  stakingWallet.listunspent(function(err,res){
    if(err){
      console.log(address);
      console.log(err)
    }
    var balance = 0
    for(var i =0; i < res.result.length;  i++){
      var data = res.result[i];
      if(data["address"]===address){
        balance += data["amount"];
      }
    }
    console.log("Calling bakc address amount " + balance + "  for address -" +address )
    callback(balance)
  })
}

//block notify
require('net').createServer(function(socket) {
  socket.on("error", function(err){
    console.log("Caught socket error: ")
    console.log(err.stack)
  });
  //check for node payment
  console.log("Block Socket connected");
  socket.on('data', function(data) {

    pendingTransactionOutputs = [];
    var recombine = false;
    var addressesToRecombine = [];
    var completedTransactions = [];
    var pendingOutputTransactions = [];
    var receivingAddress;
    checkForNodeReward(data, function(addressesToCombine, completed){
      console.log("new addresses to combine ")
      console.log(addressesToCombine);

      addressesToRecombine = addressesToCombine;
      async.eachSeries(pendingStakes, function(info, callback) {
        console.log("Dealing with ");
        console.log(info)
        if(!receivingAddress){
          if(!checkWaitingAddress(info.address)){
            receivingAddress = info.address;
          }
        }
        if (info.recombined) {
          console.log("Recombined transaction detected");
          //                handlePendingUnstakes(info.address, function(){
          console.log("Pending completed")
          //recombine pending outputs from above TODO
          //recombine with waiting and other inputs on address
          //add in pendingTransactionOutputs
          async.forEach(getWaitingAddresses(), function(data, callback){
            getAddressBalance(data.address,function(balance){
              if(balance>0){
                addressesToRecombine.push(data.address);
              }
              callback();
            })
          }, function(){
            console.log("Waiting completed completed")

            if(addressesToRecombine.length>0){ //TODO remove???
              addressesToRecombine.push(info.address);
            }
            console.log(addressesToRecombine)
            completedTransactions.push(info);
            callback()
          });

        }else if(info.pendingOutput){
          console.log("Pending output transaction detected")
          pendingOutputTransactions.push(info)
          completedTransactions.push(info);
          callback()
          //                /15.98350000
        }else if (info.category === "immature" || info.category === "generate") { //todo remove true
          console.log("Stake transaction detected")
          info.confirmations += 1;
          if (info.confirmations > 32) {//todo 31
            async.forEach(getWaitingAddresses(), function(data, callback){
              getAddressBalance(data.balance, function(balance){
                if(balance>0 && !addressesToRecombine.includes(data.address)){
                  addressesToRecombine.push(data.address);
                }
                callback();
              })
            }, function(){
              if(addressesToRecombine.length>0 && !addressesToRecombine.includes(info.address)){ //TODO remove???
                addressesToRecombine.push(info.address);
              }
              completedTransactions.push(info);
              callback()
            })
          }else{
            console.log("immature " + info.confirmations)
            callback();
          }
        } else if (info.category === "receive") {
          console.log("stake deposit transaction detected")
          if (!addressesToRecombine.includes(info.address)) {
            console.log("Not in recombine list yet")
            checkForCombine(info.address, function(combine) {
              console.log("recombine = " + combine)
              if (combine) {
                addressesToRecombine.push(info.address);

                async.forEach(getWaitingAddresses(), function(data, callback){
                  getAddressBalance(data.address, function(balance){
                    if(balance>0 && !addressesToRecombine.includes(data.address)){
                      addressesToRecombine.push(data.address);
                    }
                    callback();
                  })
                }, function(){
                  completedTransactions.push(info);
                  console.log("Completed tx push")
                  callback()
                })
              }else{
                completedTransactions.push(info);
                console.log("Completed tx push")
                callback()
              }
            });
          }else{
            completedTransactions.push(info);
            callback()
          }
        }
      }, function() {
        //            console.log("Dealing with pending output txs")
        //            var spentOutputs = []
        //            async.eachSeries(pendingOutputTransactions, function(data, callback){
        //                getStakingWalletTransaction(data.txid, function(txData){
        //                    txData.vin.forEach((input) =>{
        //                        spentOutputs.push(input.txid)
        //                    });
        //                    callback();
        //                });
        //            }, function(){
        //                console.log("pending output txs dealt with")
        //                for(var i = 0; i<pendingOutputTransactions.length; i++){
        //                    if(!spentOutputs.includes(pendingOutputTransactions[i].txid)){
        //                        increaseAddressBalance(pendingOutputTransactions[i].address, pendingOutputTransactions[i].amount);
        //                    }
        //                }
        //                pendingOutputTransactions = [];
        //            });
        console.log("Completed Transactions!")
        if (completedTransactions.length>0) {
          async.each(completedTransactions, function(info, callback){
            if(info.category==="immature" || info.category==="generate"){
              console.log("Stake transaction")
              //lookuptransaction - get input balance
              getTransaction(info.txid, function(transactionData){
                if(transactionData){
                  var input = transactionData["vin"][0]["txid"];
                  var inputVout = transactionData["vin"][0]["vout"]
                  getTransaction(input, function(inputData){
                    var oldBalance = inputData["vout"][inputVout]["value"];

                    var balanceDifference = info.amount - oldBalance;
                    //differece added to members
                    console.log("Staked " + balanceDifference)
                    var memberBalanceIncreases = [];
                    getMemberStakingData(function(rows){
                      memberBalanceIncreases[ownerID] = {earnt: 0, earnt_stake: 0, earnt_referral: 0, referral_count: 0}
                      rows.forEach((row) =>{
                        var earnt = Math.floor(row.total_owned*balanceDifference*100000)/100000;
                        var earntOwner = Math.floor(earnt*ownerPercentage*100000)/100000;
                        var earntReferrer =  Math.floor(earnt*referralPercentage*100000)/100000;
                        earnt-=(earntOwner+earntReferrer)
                        console.log(row.id + " - " + earnt);
                        //add balance to user
                        if(earnt>0){
                          if(Object.keys(memberBalanceIncreases).includes(row.id)){
                            memberBalanceIncreases[row.id].earnt+=earnt;
                            memberBalanceIncreases[row.id].earnt_stake+=earnt;
                          }else{
                            memberBalanceIncreases[row.id] = {earnt: earnt,earnt_stake: earnt, earnt_referral: 0, referral_count: 0}
                          }
                        }
                        //add to referrer
                        if(earntReferrer>0){
                          if(Object.keys(memberBalanceIncreases).includes(row.referred_by)){
                            memberBalanceIncreases[row.referred_by].earnt+=earntReferrer;
                            memberBalanceIncreases[row.referred_by].earnt_referral+=earntReferrer;
                            memberBalanceIncreases[row.referred_by].referral_count+=1;
                          }else{
                            memberBalanceIncreases[row.referred_by] = {earnt: earntReferrer,earnt_stake: 0, earnt_referral: earntReferrer, referral_count: 1}
                          }
                        }
                        //add to owner
                        if(earntOwner>0){
                          memberBalanceIncreases[ownerID].earnt+=earntOwner;                          }
                        });
                        //update database
                        console.log("MEMBER BALANCE INC")
                        console.log(memberBalanceIncreases);
                        updateStakingBalances(memberBalanceIncreases, function(){
                          sendStakeMessage(info.txid, balanceDifference)
                          callback();
                        })
                      });
                      //                        storeAddressBalance(info.address, info.amount);
                      storeLastStakeTransaction(info.address, info.txid);
                    });
                  }else{
                    callback();
                  }
                });
              }else{
                storeLastTransaction(info.address, info.txid);
                if(hasLastStake(info.address)){
                  storeLastStakeTransaction(info.address, info.txid);
                }

                //todo run sql to rebalance owner
                callback()
              }
            }, function(){
              //completed updating balances
              async.each(pendingStakes, function(info, callback){
                console.log("Checking if data in completed transactions");
                if(completedTransactions.includes(info)){
                  console.log("true")

                  //                            checkPendingUnstakes(info, function(pendingUsedBalance){
                  //not for staked

                  //                            if(info.amount>0 &&!(info.category==="immature" || info.category==="generate")){
                  //                                increaseAddressBalance(info.address, info.amount);
                  //                            }
                  db.run("DELETE FROM pending_stakes WHERE transaction_id = ?", [info.txid], function(err){
                    if(err){
                      console.log(err);
                    }
                    var index = pendingStakes.indexOf(info)
                    pendingStakes.splice(index,1);
                    db.run("INSERT INTO confirmed_stake_deposits (transaction_id) VALUES (?)", [info.txid], function(err){
                      if(err){
                        console.log(err)
                      }
                      callback()
                    })
                  })
                  //                            })
                }else{
                  console.log(false)
                  callback();
                }
              }, function(){
                console.log("Remaining pending stakes")
                console.log(pendingStakes);
                //todo recombine in same transaction
                handlePendingUnstakes(function(err, unstakeTxid){
                  console.log("Handling")
                  var newAddressesToCombine = [];
                  if(addressesToRecombine.length>0){
                    for(var i = 0; i < addressesToCombine; i++){
                      if(!newAddressesToCombine.contains(addressesToCombine[i])){
                        newAddressesToCombine.push(addressesToCombine[i]);
                      }
                    }
                    addressesToCombine = newAddressesToCombine;
                    // console.log("Combining")

                    // combineInputsOnAddresses(receivingAddress, addressesToRecombine, function(res){
                    //get owner staking balance
                    //get total staking addresses balance
                    //remove member balances from total
                    //add difference to owner
                    //TODO sql
                    checkBalancesMatchOwner(function(){
                      console.log("Not combining")
                    });
                    // });
                  }else{
                    checkBalancesMatchOwner(function(){
                      console.log("Matched")
                    });
                  }
                })
              });
            });
          }else{
            handlePendingUnstakes(function(err, unstakeTxid){
              if(!err){
                if(addressesToRecombine.length>0){
                  // combineInputsOnAddresses(receivingAddress, addressesToRecombine, function(res){
                  //get owner staking balance
                  //get total staking addresses balance
                  //remove member balances from total
                  //add difference to owner
                  //TODO sql
                  checkBalancesMatchOwner(function(){
                    console.log("not combining")
                  });
                // });
              }
            }
          });
        }
      });
    });
  });
}).listen(9002);

function getStakingWalletBalance(callback){
  stakingWallet.getBalance(function(err, res){
    if(err){
      console.log(err);
      callback();
    }else{
      callback(res.result);
    }
  })
}

function getBaseWalletBalance(callback){
  wallet.getBalance(function(err, res){
    if(err){
      console.log(err);
      callback();
    }else{
      callback(res.result);
    }
  })
}


function checkBalancesMatchOwner(callback){
  console.log("~~Checking balances match~~");
  getStakingWalletBalance(function(stakingBalance){
    var sql = "UPDATE member set staking_balance=(SELECT ?-SUM(staking_balance)FROM member WHERE id!=?) WHERE id = ?"
    db.run(sql, [stakingBalance, ownerID, ownerID], function(err){
      if(err){
        console.log(err);
      }
      getBaseWalletBalance(function(baseBalance){
        var sql = "UPDATE member set available_balance=(SELECT ?-SUM(available_balance)FROM member WHERE id!=?) WHERE id = ?"
        db.run(sql, [baseBalance,ownerID, ownerID], function(err){
          if(err){
            console.log(err);
          }
          callback();
        })
      })
    })
  })

}

function checkPendingUnstakes(deposit, callback){
  console.log("Checking pending UNSTAKE for deposit " + deposit);
  var idsToPay = {};
  var leftOver = deposit.amount;
  getPendingUnstakes(function(pendingUnstakes){
    console.log("PendingUnstakes " + pendingUnstakes.length)
    if(pendingUnstakes.length>0){
      pendingUnstakes.forEach((pendingUnstake)=>{
        console.log(pendingUnstake)
        if(leftOver>getTransactionFee(1)){
          if(leftOver-getTransactionFee(1)>=pendingUnstake.amount){
            leftOver -=pendingUnstake.amount;
            idsToPay[pendingUnstake.member_id] = pendingUnstake.amount;
            console.log('added to ids to pay')
            console.log(idsToPay);
            //                    removeFromPending
            removePendingUnstake(pendingUnstake.id);
          }else{
            if(leftOver-getTransactionFee(1)>=1){
              idsToPay[pendingUnstake.member_id] = leftOver-getTransactionFee(1)
              //update pending to new balance - leftOver
              console.log("Total amount not avail only partial")
              updatePendingUnstakeAmount(pendingUnstake.member_id, pendingUnstake.amount - leftOver)
              leftOver=0;
            }
          }
        }
      })
      //replace id with withdraw address
      var balancesToPay = {}
      console.log("Ids to pay = " + Object.keys(idsToPay).length)
      if(Object.keys(idsToPay).length>0){
        if(leftOver>getTransactionFee(1)){
          balancesToPay[deposit.address] = leftOver-getTransactionFee(1)
        }
        async.each(Object.keys(idsToPay), function(id, callback){
          getUserDepositAddress(id, function(err, userDepositAddress){
            balancesToPay[userDepositAddress] = idsToPay[id];
            removeStakeBalance(id, idsToPay[id], function(){
              callback();
            })
          })
        }, function(){
          //pay balancesToPay
          console.log("Balances to pay")
          console.log(balancesToPay);
          console.log(deposit);
          if(Object.keys(balancesToPay).length>0){
            payBalances(balancesToPay, [deposit], function(err, resultTxID){
              console.log(err);
              console.log(resultTxID)
              callback(leftOver!==deposit.amount)
            })
          }else{
            callback(leftOver!==deposit.amount)
          }
        })
      }else{
        callback(leftOver!==deposit.amount)
      }
    }else{
      callback(leftOver!==deposit.amount)
    }
  })
}

function payBalances(balancesToPay, inputs, callback){
  var receiving_addresses = balancesToPay;
  createRawTransaction(inputs, receiving_addresses, function(hexString) {
    console.log("Hex string generated " + hexString);
    if (!hexString) {
      callback("Error creating raw transaction")
    } else {
      signRawTransaction(hexString, function(hexString) {
        if (!hexString) {
          callback("Error signing raw transaction")
        } else {
          sendRawTransaction(hexString, function(txID) {
            if (!txID) {
              callback("Error sending raw transaction")
            } else {
              callback(null, txID)
            }
          })
        }
      })
    }
  });
}

function removePendingUnstake(id){
  console.log("Removing pending unstake " + id)
  db.run("DELETE FROM pending_unstake WHERE id = ?", [id], function(err){
    if(err){
      console.log(err);
    }
  })
}

function updatePendingUnstakeAmount(id, amount){
  console.log("Updating pending unstake " + id + " amount " + amount);
  db.run("UPDATE pending_unstake SET amount = ? WHERE id = ?", [amount, id], function(err){
    if(err){
      console.log(err);
    }
  })
}

function getPendingUnstakes(callback){
  let sql = 'SELECT * FROM pending_unstake';
  db.all(sql, (err, rows) => {
    if (err) {
      console.log("Error loading pending unstakes!")
    }
    callback(rows);
  });
}

function hasPendingUnstake(userID,callback){
  let sql = 'SELECT SUM(amount) as sum FROM pending_unstake where member_id=?';
  db.get(sql, [userID], (err, row) => {
    if (err) {
      return console.error(err.message);
    }
    if (row) {
      if(row.sum){
        callback(row.sum)
      }else{
        callback(0)
      }
    } else {
      callback(0)
    }
  });
}

function isCompletedStakingTransaction(txid, callback){
  let sql = 'SELECT 1 FROM confirmed_stake_deposits where transaction_id=?';
  db.get(sql, [txid], (err, row) => {
    if (err) {
      return console.error(err.message);
    }
    if (row) {
      callback(true)
    } else {
      callback(false)
    }
  });
}

function updateStakingBalances(values, callback){
  db.parallelize(() => {
    sql = "UPDATE member SET staking_balance = staking_balance+?, earnt_stakes = earnt_stakes+?, earnt_referrals = earnt_referrals+?, count_stakes = count_stakes+?, count_referrals = count_referrals+?  WHERE id=?";
    async.each(Object.keys(values), function(id, callback){
      db.run(sql, [values[id].earnt, values[id].earnt_stake, values[id].earnt_referral, 1, values[id].referral_count, id], function(err) {
        if (err) {
          console.log("There was an error updating the database");
          console.log(err);
        } else {
          callback();
        }
      })
    }, function(){
      callback();
    })
  });
}

function checkWaitingAddress(address) {
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].address === address) {
      if (stakingAddresses[i].waiting) {
        return true;
      } else {
        return false;
      }
    }
  }
  return false;
}

function getStakingAddresses() {
  var list = [];
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (!stakingAddresses[i].waiting) {
      list.push(stakingAddresses[i]);
    }
  }
  return list;
}

function getWaitingAddresses() {
  var list = [];
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].waiting) {
      list.push(stakingAddresses[i]);
    }
  }
  return list;
}

function getWaitingAddressesOnly() {
  var list = [];
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].waiting) {
      list.push(stakingAddresses[i].address);
    }
  }
  return list;
}


function addUserToDatabase(id, inviter) {
  db.parallelize(() => {
    db.run('INSERT INTO member(id, referred_by) VALUES(?,?)', [id, inviter.id === undefined ? ownerID : inviter.id], function(err) {
      if (err) {
        return console.log(err.message);
      }
      console.log(`A row has been inserted with rowid ${this.lastID}`);
    });
    db.run('INSERT INTO referrals(member_id, referred_by) VALUES(?,?)', [id, inviter.id === undefined ? ownerID : inviter.id], function(err) {
      if (err) {
        return console.log(err.message);
      }
      console.log("Referer ID " + inviter.id)
      console.log(`A row has been inserted with rowid ${this.lastID}`);
    });
    getNewAddress(id, function(newAddress) {
      if (newAddress) {
        db.run('INSERT INTO address(member_id, deposit_address) VALUES(?,?)', [id, newAddress], function(err) {
          if (err) {
            return console.log(err.message);
          }
          console.log(`A row has been inserted with rowid ${this.lastID}`);
        });
      } else {
        console.log("Address not generated skipping for now")
      }
    });
  });
}

//Message event
bot.on('message', function(user, userID, channelID, message, evt) {
  if (channelID === defaultChannelID && airDropRunning && !reactDropRunning && message.substring(0, 1) !== '!' && userID !== botID) {
    //airdrop commands
    var userHash = get_user(userID).avatar
    var err;
    if (lotteryDropRunning) {
      var numberGuess = message;
      if (airDropEntrants.includes(userID) || isNaN(numberGuess) || numberGuess < 0 || numberGuess > 100) {
        bot.addReaction({
          channelID: channelID,
          messageID: evt.d.id,
          reaction: '❌'
        }, function(err, res) {
          if (err) {
            console.log(err);
          }
        });
      } else {
        var difference = Math.abs(numberGuess - airDropObjective);
        bot.addReaction({
          channelID: channelID,
          messageID: evt.d.id,
          reaction: '✅'
        }, function(err, res) {
          if (err) {
            console.log(err);
          }
        });
        airDropEntrants.push(userID);
        if (numberGuess === airDropObjective) {
          airDropWinners.push(userID)
        }
      }
    } else if (rollDropRunning && message === 'roll') {
      if (!airDropEntrants.includes(userID)) {
        var userRoll = Math.floor(Math.random() * 101);
        sendEmbededMessage(getRollMessageFields(userRoll), user, userID, userHash, channelID);
        airDropEntrants.push(userID);
        if (userRoll > rollDropHighestValue) {
          airDropWinners = [];
          airDropWinners.push(userID);
          rollDropHighestValue = userRoll;
        } else if (userRoll === rollDropHighestValue) {
          airDropWinners.push(userID);
        }
      } else {
        bot.addReaction({
          channelID: channelID,
          messageID: evt.d.id,
          reaction: '❌'
        }, function(err, res) {
          if (err) {
            console.log(err);
          }
        });
      }
    } else if (phraseDropRunning) {
      if (!airDropEntrants.includes(userID) && message === airDropObjective) {
        airDropEntrants.push(userID);
        bot.addReaction({
          channelID: channelID,
          messageID: evt.d.id,
          reaction: '✅'
        }, function(err, res) {
          if (err) {
            console.log(err);
          }
        });
      } else {
        bot.addReaction({
          channelID: channelID,
          messageID: evt.d.id,
          reaction: '❌'
        }, function(err, res) {
          if (err) {
            console.log(err);
          }
        });
      }
    }
  } else if (message.substring(0, 1) == '!') {
    var args = message.substring(1).split(' ').filter(Boolean);
    var cmd = args[0];
    args = args.splice(1);
    console.log("Command " + cmd + (args.length > 0 ? " with arguments " + args.toString() : "") + " received by " + user + " from channel " + channelID);
    var userHash = get_user(userID).avatar
    //        if (channelID === defaultChannelID) {
    switch (cmd) {
      case 'help':
      sendEmbededMessageWithTitle("Command Help", "Below are the common commands available for the B3 Staking Bot", getHelpMessageFields(), user, userID, userHash, channelID);
      break;
      case 'deposit':
      checkDefaultSet(userID, function(err, userHasDefault) {
        if (err) {
          sendEmbededMessage(getDepositMessageFields(err, address), user, userID, userHash, channelID);
        } else if (userHasDefault) {
          getUserDepositAddress(userID, function(err, address) {
            sendEmbededMessage(getDepositMessageFields(err, address), user, userID, userHash, channelID);
          });
        } else {
          sendEmbededMessage(getSetDefaultWithdrawalDepositMessageFields(), user, userID, userHash, channelID);
        }
      });
      break;
      case 'withdraw':
      var requestedAmount = args[0];
      var withdrawalAddress = args[1];
      processWithdrawCommand(userID, requestedAmount, withdrawalAddress, function(err, newBalance, transaction, withdrawnAddress) {
        sendEmbededMessage(getWithdrawMessageFields(err, requestedAmount, withdrawnAddress, newBalance, transaction), user, userID, userHash, channelID);
      });
      break;
      case 'balance':
      getUserBalances(userID, function(err, balance) {
        getStakingWalletBalance(function(stakingTotalBalance){
          sendEmbededMessage(getBalanceMessageFields(err, balance, stakingTotalBalance), user, userID, userHash, channelID)
        })
      });
      break;
      case 'claim':
      sendEmbededMessage(getClaimFields(), user, userID, userHash, channelID);
      break;
      case 'verify':
      var address = args[0];
      var messageHash = args[1];
      processVerifyCommand(userID, address, messageHash, function(err){
        sendEmbededMessage(getVerifyMessageFields(err,address), user, userID, userHash, channelID);
      })
      break;
      case 'stake':
      var requestedStakeAmount = args[0]
      processStakeCommand(userID, requestedStakeAmount, function(err, bal) {
        if(requestedStakeAmount && requestedStakeAmount.toLowerCase() === 'all'){
          requestedStakeAmount = bal;
        }
        sendEmbededMessage(getStakeMessageFields(err, requestedStakeAmount), user, userID, userHash, channelID);
      });
      break;
      case 'unstake':
      var requestedUnStakeAmount = args[0]
      processUnStakeCommand(userID, requestedUnStakeAmount, function(err) {
        sendEmbededMessage(getUnstakeMessageFields(err, requestedUnStakeAmount), user, userID, userHash, channelID);
      });
      break;
      case 'cancelunstake':
      processCancelUnstakeCommand(userID, function(err, amount) {
        sendEmbededMessage(getCancelUnstakeMessageFields(err, amount), user, userID, userHash, channelID);
      });
      break;
      case 'info':
      getInfoMessageFields(function(messageFields) {
        console.log("sending " )
        console.log(messageFields);
        sendEmbededMessageWithTitle("Info", "Displays information about staking and the wallet hosting the pool", messageFields, user, userID, userHash, channelID);
      });
      break;
      case 'tip':
      var requestedReceiver = args[0];
      var requestedTipAmount = args[1];
      if (requestedReceiver) {
        if(requestedReceiver!='@everyone'){
          if(requestedReceiver[0]!='<'){
            requestedReceiver=null
          }else{
            requestedReceiver = requestedReceiver.slice(2, requestedReceiver.length);
            requestedReceiver = requestedReceiver.slice(0, requestedReceiver.length - 1);
            if(requestedReceiver[0]==="!"){
              requestedReceiver = requestedReceiver.substr(1);
            }
          }
        }
      }
      processTipCommand(userID, requestedReceiver, requestedTipAmount, function(err, endTipAmount) {
        sendEmbededMessage(getTipMessageFields(err, endTipAmount?endTipAmount:requestedTipAmount, requestedReceiver), user, userID, userHash, channelID);
      });
      break;
      case 'coffee':
      sendEmbededMessage([{
        "name": "Coffee",
        "value": ":coffee:"
      }], user, userID, userHash, channelID);
      break;
      case 'reactdrop':
      var reactDropAmount = args[0];
      var reactDropEmoji = args[1];
      processReactDropCommand(userID, reactDropAmount, reactDropEmoji, function(err) {
        sendEmbededMessage(getReactDropFields(err, reactDropAmount, reactDropEmoji), user, userID, userHash, channelID, function(messageID) {
          if (!err) {
            startReactionDrop(userID, reactDropAmount, messageID, reactDropEmoji);
          }
        });
      });
      break;
      case 'lotterydrop':
      var lotteryDropAmount = args[0];
      processLotteryDropCommand(userID, lotteryDropAmount, function(err) {
        sendEmbededMessage(getLotteryDropFields(err, lotteryDropAmount), user, userID, userHash, channelID, function(messageID) {
          if (!err) {
            startLotteryDrop(userID, lotteryDropAmount, messageID);
          }
        });
      });
      break;
      case 'rolldrop':
      var rollDropAmount = args[0];
      processRollDropCommand(userID, rollDropAmount, function(err) {
        sendEmbededMessage(getRollDropFields(err, rollDropAmount), user, userID, userHash, channelID, function(messageID) {
          if (!err) {
            startRollDrop(userID, rollDropAmount, messageID);
          }
        });
      });
      break;
      case 'phrasedrop':
      var phraseDropAmount = args[0];
      var phraseDropPhrase = args.splice(1).join(' ');
      console.log(phraseDropPhrase)
      processPhraseDropCommand(userID, phraseDropAmount, phraseDropPhrase, function(err) {
        sendEmbededMessage(getPhraseDropFields(err, phraseDropAmount, phraseDropPhrase), user, userID, userHash, channelID, function(messageID) {
          if (!err) {
            startPhraseDrop(userID, phraseDropAmount, messageID, phraseDropPhrase);
          }
        });
      });
      break;
      case 'dice':
      var diceHighLow = args[0];
      var diceAmount = args[1];
      var multiplier = 2
      if(args[2]){
        multiplier = args[2];
      }
      processDiceCommand(userID, diceHighLow, diceAmount, multiplier, function(err) {
        sendEmbededMessage(getDiceFields(diceHighLow, diceAmount, balance, newBalance), user, userID, userHash, channelID)
      });
      break;
      case 'ping':
      sendEmbededMessage([{
        "name": "Ping",
        "value": "Pong"
      }], user, userID, userHash, channelID);
      break;
      case 'referrals':
      getRefferalFields(userID, function(field) {
        sendEmbededMessage(field, user, userID, userHash, channelID);
      })
      break;
      case 'setwithdrawal':
      var withdrawal = args[0];
      processSetWithdrawal(userID, withdrawal, function(err) {
        sendEmbededMessage(getSetWithdrawalMessageFields(err, withdrawal), user, userID, userHash, channelID);
      })
      break;
      case 'fees':
      sendEmbededMessage(getFeesFields(), user, userID, userHash, channelID);
      break;
    }
    //        } else {
    //            sendEmbededMessage(getDirectMessageErrorFields(), user, userID, userHash, channelID)
    //        }
  }
});

//reaction detect
bot.on('messageReactionAdd', function(event) {
  if (!reactDropRunning) {
    return;
  }
  var data = event.d;
  var messageID = data.message_id;
  if (messageID !== airDropMessage) {
    return;
  }
  var userID = data.user_id
  if (airDropWinners.includes(userID)) {
    return;
  }
  var emoji = data.emoji.name;
  if (emoji !== airDropObjective) {
    return;
  }
  airDropWinners.push(userID)
})
bot.on('messageReactionRemove', function(event) {
  if (!reactDropRunning) {
    return;
  }
  var data = event.d;
  var messageID = data.message_id;
  if (messageID !== airDropMessage) {
    return;
  }
  var userID = data.user_id
  if (!airDropWinners.includes(userID)) {
    return;
  }
  var emoji = data.emoji.name;
  if (emoji !== airDropObjective) {
    return;
  }
  var index = airDropWinners.indexOf(userID);
  if (index !== -1) airDropWinners.splice(index, 1);
});


function getDefaultField(command, message) {
  return {
    "name": command,
    "value": message
  }
}

function getHelpMessageFields() {
  return [
    getDefaultField("!deposit", "Generates your deposit address for the staking pool"),
    getDefaultField("!withdraw <address> <amount>", "Withdraws an available <amount> from your balance and sends it to your <address>"),
    getDefaultField("!balance", "Displays your available balances"),
    getDefaultField("!stake <amount>/all", "Sends the specified <amount>/all to the staking pool"),
    getDefaultField("!unstake <amount>", "Removes the specified <amount> from the staking pool"),
    getDefaultField("!info", "Displays information related to the staking bot"),
    getDefaultField("!tip <@user> <amount>", "Tips the tagged @user with the <amount> from your available balance"),
    getDefaultField("!reactdrop <emoji> <amount>", "Reaction airdrop with <amount> for 5 minutes with winners reacting to the air drop post with the <emoji>. <amount> is split between all winners."),
    getDefaultField("!lotterydrop <amount>", "Lottery airdrop with <amount> for 5 minutes with winners guessing the correct number from 1-100(inclusive). <amount> is split between all winners."),
    getDefaultField("!rolldrop <amount>", "Roll airdrop with <amount> for 5 minues with winners rolling the highest number by typing in `roll` to chat. <amount> is split between all winners."),
    getDefaultField("!phrasedrop <amount> <phrase>", "Phrase airdrop with <amount> for 5 minues with winners typing the <phrase> into chat. <amount> is split between all winners."),
    //        getDefaultField("!dice <high/low>", "Costs the user 5kb3 to play with a random number from 1-10000 being generated. If the user correctly guesses higher than 5600 or lower than 4900 they will win 10kb3"),
    getDefaultField("!referrals", "Displays the number of referrals you have received. You will receive 2% of all your referrals stake amounts. Anyone who uses your invite to this server will count as a referral for life."),
    getDefaultField("!setwithdrawal <address>", "Sets your default withdrawal address, this helps save time when withdrawing and also gives the pool an address to return all your funds to if ever needed."),
    getDefaultField("!fees", "Displays the current fee structure"),
    getDefaultField("!claim", "Instructions to claim your address from the B3LotteryNode pool"),
    getDefaultField("!verify <b3 Address> <message hash>", "Verifies the owner of the address and adds the node pool balance to the discord account")
  ];
}

function getDirectMessageErrorFields() {
  return [
    getDefaultField("Error", "Staking Bot only accepts messages sent in the <#491721460516257832> channel in the B3FNPool server")
  ];
}

function getSetDefaultWithdrawalDepositMessageFields() {
  return [
    getDefaultField("Deposit", "Before generating your deposit address, you will need to set your default withdrawal address with `!setwithdrawal <address>`")
  ];
}

function getSetWithdrawalMessageFields(err, address) {
  return [
    err ? getDefaultField("Set Withdrawal", err) : getDefaultField("Set Withdrawal", "Your default withdrawal address is now `" + address + "`")
  ];
}

function getDepositMessageFields(err, address) {
  return [
    err ? getDefaultField("Deposit", err) : getDefaultField("Deposit", "Your deposit address is `" + address + "`")
  ];
}

function getWithdrawMessageFields(err, requestedAmount, withdrawalAddress, newBalance, transaction) {
  return [
    err ? getDefaultField("Withdraw", err) : getDefaultField("Withdraw", "***" + requestedAmount + "*** kB3 withdrawn to address: " + withdrawalAddress + "\nYour updated available balance is: ***" + newBalance + "*** kB3\nTransaction ID: [" + transaction + "](https://chainz.cryptoid.info/b3/tx.dws?" + transaction + ")")
  ];
}

function getBalanceMessageFields(err, balance, stakingTotalBalance) {
  var fields;

  console.log(balance);
  if(err){
    fields = getDefaultField("Balance", err);
  }else{
    var string = "";
    if(balance[0] > 0){
      string= string + "Pending balance: ***" + balance[0] + " kB3***\n";
    }
    if(balance[1] > 0){
      string= string + "Available balance: ***" + balance[1]+ " kB3***\n";
    }else{
      string= string + "Available balance: ***" + 0 + " kB3***\n";
    }
    if(balance[2] > 0){
      string= string +  "Staking balance: ***" + balance[2] + " kB3*** ("+(balance[2]/stakingTotalBalance*100).toFixed(3)+" %)\n";
    }
    if(balance[3] > 0){
      string= string +  "Node Pool balance: ***" + balance[3] + " kB3*** (" +( balance[3]/3612562.92021548*100 ).toFixed(3)+" %)";
    }
    fields = getDefaultField("Balance",string);
  }
  return [
    fields
  ];
}

function getStakedMessageFields(err, amount, txid) {
  return [
    getDefaultField("Stake", "***" + amount + "*** kB3\nTransaction ID: [" + txid + "](https://chainz.cryptoid.info/b3/tx.dws?" + txid + ")")
  ];
}

function getStakeMessageFields(err, stakeAdded) {
  return [
    err ? getDefaultField("Stake", err) : getDefaultField("Stake", "***" + stakeAdded + "*** kB3 has been added to your staking account.")
  ];
}

function getVerifyMessageFields(err, address){
  return [
    err ? getDefaultField("Verify", err) : getDefaultField("Verify", "The lottery address ***" + address + "*** has been assigned to your account. Run the `!balance` command to view your new balance")
  ];
}

function getUnstakeMessageFields(err, amount) {
  return [
    err ? getDefaultField("Unstake", err) : getDefaultField("Unstake", "Your request to unstake ***" + amount + "*** kB3 has been successful")
  ];
}

function getCancelUnstakeMessageFields(err, amount) {
  return [
    err ? getDefaultField("Cancel Unstake", err) : getDefaultField("Cancel Unstake", "Your requested unstake for ***" + amount + "*** kB3 has been canceled")
  ];
}

function getInfoMessageFields(callback) {
  var stakingInfoMessage = "Error retrieving staking info"
  var walletInfoMessage = "Error retrieving  wallet info"
  getStakingInfo(function(stakingInfo) {
    console.log(stakingInfo)
    getWalletInfo(function(walletInfo) {
      if (walletInfo) {
        walletInfoMessage = "***Wallet Version:*** " + walletInfo.version + "\n***Total Wallet Balance:*** " + walletInfo.balance + "\n***Total Supply:*** " + walletInfo.moneysupply;
      } else {
        console.log("Error retrieving wallet  info");
      }
      var fields = [];
      if (walletInfo) {
        fields.push(getDefaultField("Staking Enabled:", stakingInfo.enabled));
        fields.push(getDefaultField("Staking:", stakingInfo.staking));
        fields.push(getDefaultField("Difficulty:", stakingInfo.difficulty.toString()));
        fields.push(getDefaultField("Wallet Weight:", stakingInfo.weight.toString()));
        fields.push(getDefaultField("Network Weight:", stakingInfo.netstakeweight.toString()));
      } else {
        fields.push(getDefaultField("Staking Info", stakingInfoMessage));
      }
      if (walletInfo) {
        fields.push(getDefaultField("Wallet Version", walletInfo.version));
        fields.push(getDefaultField("Wallet Balance", walletInfo.balance.toString()));
        fields.push(getDefaultField("Total Supply", walletInfo.moneysupply.toString()));
      } else {
        fields.push(getDefaultField("Wallet Info", walletInfoMessage));
      }
      console.log("callback")
      callback(fields);
    });
  });
}

function getTipMessageFields(err, requestedTipAmount, requestedReceiver) {
  return [
    err ? getDefaultField("Tip", err) : getDefaultField("Tip", "<@" + requestedReceiver + "> has been tipped ***" + requestedTipAmount + "*** kB3")
  ];
}

function getReactDropFields(err, reactDropAmount, reactDropEmoji) {
  return [
    err ? getDefaultField("React Drop", err) : getDefaultField("React Drop", "@here\nReaction Airdrop!\nPrize Pool: ***" + reactDropAmount + "*** kB3\nEmoji: " + reactDropEmoji + "\nDuration: ***" + (airDropTime / 1000 / 60) + "*** mins\nPrize will be split between all entrants.\nTo enter react to this post with the same emoji listed in the emoji field")
  ];
}

function getLotteryDropFields(err, lotteryDropAmount) {
  return [
    err ? getDefaultField("Lottery Drop", err) : getDefaultField("Lottery Drop", "\n@here\nLottery Airdrop!\nPrize Pool: ***" + lotteryDropAmount + "*** kB3\nDuration: ***" + (airDropTime / 1000 / 60) + "*** mins\n\nTo enter, type a number from 1-100(inclusive) in this channel, only your first guess will count. Correct guesses will share the prize pool. If there are no correct guesses then all entries will share the prize pool.")
  ];
}

function getRollDropFields(err, rollDropAmount) {
  return [
    err ? getDefaultField("Roll Drop", err) : getDefaultField("Roll Drop", "\n@here\nRoll Airdrop!\nPrize Pool: ***" + rollDropAmount + "*** kB3\nDuration: ***" + (airDropTime / 1000 / 60) + "*** mins\n\nTo enter, type roll in this channel, only your first roll will count. The highest rolls will share the prize pool")
  ];
}

function getPhraseDropFields(err, phraseDropAmount, phraseDropPhrase) {
  return [
    err ? getDefaultField("Phrase Drop", err) : getDefaultField("Phrase Drop", "@here\nPhrase Airdrop!\nPrize Pool: ***" + phraseDropAmount + "*** kB3\nPhrase: ***" + phraseDropPhrase + "***\nDuration: ***" + (airDropTime / 1000 / 60) + "*** mins\nPrize will be split between all entrants.\nTo enter type the \"Phrase\" above into this channel.")
  ];
}

function getDiceFields(diceHighLow, diceAmount, balance, newBalance) {
  return [
    getDefaultField("Dice", "Dice Message")
  ];
}

function getTransactionFields(txid, confirmations, amount) {
  return [
    getDefaultField("Deposit " + (confirmations > 0 ? "Confirmed" : "Detected"), "Transaction ID: [" + txid + "](https://chainz.cryptoid.info/b3/tx.dws?" + txid + ")\nStatus: " + (confirmations > 0 ? "Confirmed" : "Pending") + " (" + confirmations + " confirmations)\nAmount: " + amount + " kB3" + (confirmations > 0 ? "\nDon't forget to stake your coins with the !stake command.\nTo view other commands type !help in the <#491721460516257832> channel" : ""))
  ];
}

function getFeesFields() {
  return [
    getDefaultField("Fees", "The only all earnings via this pool will incur a ***5%*** fee. ***2%*** to your referral and ***3%*** to the pool. There are no fees for withdrawing.")
  ];
}

function getClaimFields(){
  return [
    getDefaultField("Claim", "To claim your B3LotteryAccount please follow the steps below carefully depending on your address type.\n***Exchange Address***\n1. Send your address and a screenshot of your transaction history from the exchange to <@"+ownerID+">. This may take 1-2 days to process\n***Wallet Address***\n1. In your wallets debug console type the command `signmessage <Your B3 Address> 'B3Lottery'` for example `signmessage SVuB5BJymcSSaFgzrAmazqhaf1WLUYnbGg 'B3Lottery'`\n2. To verify you own the wallet run the command `!verify <Your B3 Address> <Signmessage hash>`")
  ];
}

function getRefferalFields(userID, callback) {
  getReferralCount(userID, function(referralCount) {
    if (referralCount < 0) {
      callback([getDefaultField("Referrals", "Error retrieving referrals from database")]);
    } else {
      getReferralEarnings(userID, function(refStaked, refEarnt) {
        if (refStaked < 0) {
          callback([getDefaultField("Referrals", "Error retrieving referrals from database")]);
        } else {
          callback([getDefaultField("Referrals", "Referrals: ***" + referralCount + "***\nReferral Stakes: ***" + refStaked + "***\nEarnt From Referrals: ***" + refEarnt + "*** kB3\n\nAnyone who uses your invite to this server will count as a referral to you. You will earn ***2%*** on all stakes made by your referrals")]);
        }
      });
    }
  });
}

function getRollMessageFields(userRoll) {
  return [
    getDefaultField("Roll", "You have rolled a " + userRoll)
  ]
}

function roundToThreeDecimals(value) {
  return Math.floor(value * 1000) / 1000
}

function getEndAirdropFields(err, title) {
  var fields = [];
  if (err) {
    fields.push(getDefaultField(title, err));
  } else {
    fields.push(getDefaultField(title, "Airdrop has ended! Winners below will each get ***" + roundToThreeDecimals(airDropAmount / airDropWinners.length) + "*** kB3"))
    var winnersStrings = []
    var currentString = '';
    for (var i = 0; i < airDropWinners.length; i++) {
      if (currentString.length + (", " + airDropWinners[i]).length > 950) {
        winnersStrings.push(currentString);
        currentString = ''
      }
      if (currentString.length > 0 || winnersStrings.length > 0) {
        currentString += ", "
      }
      currentString += "<@" + airDropWinners[i] + ">";
    }
    winnersStrings.push(currentString);
    for (i = 0; i < winnersStrings.length; i++) {
      fields.push(getDefaultField("Winners " + (winnersStrings.length > 1 ? (i + 1) : "" + ":"), winnersStrings[i]));
    }
  }
  return fields;
}


function get_user(memberID) {
  var user = bot.users[memberID];
  return user;
}

function sendEmbededDirectMessage(fields, userID, channelID) {
  var data = {
    "to": channelID,
    "embed": {
      "color": dec2hex(userID),
      "fields": fields
    }
  };
  bot.sendMessage(data);
}

function sendNodeStakeMessage(txid,amount){
  const msg = new webhook.MessageBuilder()
  .setName("Node Generation")
  .setColor("#" + dec2hex(amount))
  .addField("", "Node pool generated ***"+(Math.round(amount*10000)/10000)+"***kB3")
  .addField("", "Transaction ID: [" + txid + "](https://chainz.cryptoid.info/b3/tx.dws?" + txid + ")");
  nodeHook.send(msg);
}

function sendStakeMessage(txid,amount){
  const msg = new webhook.MessageBuilder()
  .setName("Stake")
  .setColor("#" + dec2hex(amount))
  .addField("", "Staked ***"+(Math.round(amount*10000)/10000)+"***kB3")
  .addField("", "Transaction ID: [" + txid + "](https://chainz.cryptoid.info/b3/tx.dws?" + txid + ")");
  stakingHook.send(msg);
}

function sendEmbededMessage(fields, user, userID, userHash, channelID, callback) {
  var data = {
    "to": channelID,
    "embed": {
      "color": dec2hex(userID),
      "author": {
        "name": user,
        "url": "https://discordapp.com/users/" + userID,
        "icon_url": "https://cdn.discordapp.com/avatars/" + userID + "/" + userHash + ".png"
      },
      "fields": fields
    }
  };
  bot.sendMessage(data, function(err, res) {
    if (err) {
      console.log(err);
    }
    if (callback)
    callback(res.id);
  });
}

function sendEmbededMessageWithTitle(title, description, fields, user, userID, userHash, channelID) {
  var data = {
    "to": channelID,
    "embed": {
      "title": title,
      "description": description,
      "color": dec2hex(userID),
      "author": {
        "name": user,
        "url": "https://discordapp.com/users/" + userID,
        "icon_url": "https://cdn.discordapp.com/avatars/" + userID + "/" + userHash + ".png"
      },
      "fields": fields
    }
  };
  bot.sendMessage(data);
}

function dec2hex(i) {
  var color = (i + 0x10000).toString(16).substr(-6).toUpperCase();
  if (color.length < 6) {
    return "FFFFFF"
  } else {
    return color;
  }
}

function sendHookMessage(message, userID) {
  const msg = new webhook.MessageBuilder()
  .setName("B3 Staking Bot")
  .setColor("#" + dec2hex(userID))
  .addField("", "***User:***<@" + userID + ">")
  .addField("", message);
  stakingHook.send(msg);
}

function getUserDepositAddress(userID, callback) {
  var address = null;
  let sql = "SELECT deposit_address FROM address WHERE member_id = ?";
  db.get(sql, [userID], (err, row) => {
    if (err) {
      console.error(err.message);
      callback("Error retrieving deposit address")
    } else {
      if (row) {
        callback(false, row.deposit_address);
      } else {
        //create deposit address
        getNewAddress(userID, function(newAddress) {
          if (newAddress) {
            db.run('INSERT INTO address(member_id, deposit_address) VALUES(?,?)', [userID, newAddress], function(err) {
              if (err) {
                return console.log(err.message);
              }
              console.log(`A row has been inserted with rowid ${this.lastID}`);
              callback(false, newAddress);
            });
          } else {
            callback("Error generating a new wallet address");
          }
        })
      }
    }
  });
}

function getUserBalances(userID, callback) {
  var balance = 0;
  let sql = "SELECT * FROM member where id = ?"
  db.get(sql, [userID], (err, row) => {
    if (err) {
      console.error(err.message);
      callback("Error retrieving balances")
    } else {
      if (row) {
        callback(false, [Math.floor(row.pending_balance*100000)/100000, Math.floor(row.available_balance*100000)/100000, Math.floor(row.staking_balance*100000)/100000, Math.floor(row.node_balance*100000)/100000]);
      } else {
        callback("Unable to find balance");
      }
    }
  });
}



function getUserBalance(userID, callback) {
  var balance = 0;
  let sql = "SELECT available_balance FROM member where id = ?"
  db.get(sql, [userID], (err, row) => {
    if (err) {
      console.error(err.message);
      callback("Error retrieving balance")
    } else {
      if (row) {
        callback(false, row.available_balance);
      } else {
        callback("Unable to find balance");
      }
    }
  });
}

function getUserAvailableBalance(userID, callback) {
  var balance = 0;
  let sql = "SELECT available_balance FROM member where id = ?"
  db.get(sql, [userID], (err, row) => {
    if (err) {
      console.error(err.message);
      callback("Error retrieving balance")
    } else {
      if (row) {
        callback(false, row.available_balance);
      } else {
        callback("Unable to find balance");
      }
    }
  });
}

function getUserStakeBalance(userID, callback) {
  var balance = 0;
  let sql = "SELECT staking_balance FROM member where id = ?"
  db.get(sql, [userID], (err, row) => {
    if (err) {
      console.error(err.message);
      callback("Error retrieving balance")
    } else {
      if (row) {
        callback(false, row.staking_balance);
      } else {
        callback("Unable to find balance");
      }
    }
  });
}

function processWithdrawCommand(userID, requestedAmount, withdrawalAddress, callback) {
  var newBalance = -1
  if (!requestedAmount) {
    callback("Invalid withdrawal command. Please use !withdraw <amount> <address>");
    return;
  } else if (requestedAmount) {
    if (isNaN(requestedAmount)) {
      callback("Invalid withdrawal amount(" + requestedAmount + ") Please use a decimal value equal or greater than 1 kB3");
      return;
    } else if (requestedAmount < 1) {
      callback("Invalid withdrawal amount(" + requestedAmount + ") Please use a decimal value equal or greater than 1 kB3");
      return;
    }
    //        else if(requestedAmount > 1){
    //            callback("Withdrawals are currently disabled while I monitor the staking system for bugs, pm me for a withdrawal if you really need it done.");
    //            return;
    //        }
    if (!withdrawalAddress) {
      checkDefaultSet(userID, function(err, userHasDefault) {
        if (err) {
          callback(deferr);
        } else if (userHasDefault) {
          getDefaultWithdrawalAddress(userID, function(err, defaultWithdrawalAddress) {
            if (err) {
              callback(err);
            } else {
              getUserBalance(userID, function(err, balance) {
                if (err) {
                  callback('Error retrieving balance');
                } else {
                  console.log(balance);
                  console.log(requestedAmount);
                  console.log(+balance < +requestedAmount);
                  if (balance < requestedAmount) {
                    callback("You do not have enough balance to withdraw " + requestedAmount + "\n***Current Balance:*** " + balance);
                  } else {
                    sendTransaction(requestedAmount, defaultWithdrawalAddress, function(sentTransaction) {
                      if (!sentTransaction) {
                        callback("Error sending transaction");
                      } else {
                        newBalance = balance - requestedAmount;
                        let sql = "UPDATE member SET available_balance = available_balance - ? WHERE id = ?";
                        let data = [requestedAmount, userID];
                        db.run(sql, data, function(err) {
                          if (err) {
                            console.error(err.message);
                            callback("Error updating database")
                          } else {
                            console.log(`Row(s) updated: ${this.changes}`);
                            let sql = "INSERT  INTO withdrawals(transaction_id, member_id, to_address, amount) VALUES (?, ? ,? ,?)";
                            let data = [sentTransaction, userID, defaultWithdrawalAddress, requestedAmount];
                            db.run(sql, data, function(err) {
                              if (err) {
                                console.error(err.message);
                                callback("Error updating database")
                              } else {
                                callback(err, newBalance, sentTransaction, defaultWithdrawalAddress);
                              }
                            });
                          }
                        })
                      }
                    });
                  }
                }
              });
            }
          });
        } else {
          callback("Invalid withdrawal command. Please use !withdraw <amount> <address>, or set a default withdrawal address with !setwithdrawal <amount>");
          return;
        }
      });
    } else {
      checkValidAddress(withdrawalAddress, function(valid) {
        if (!valid) {
          callback("Error validating address");
        } else if (!valid.isvalid) {
          callback("Invalid address specified(" + withdrawalAddress + ")");
        } else {
          getUserBalance(userID, function(err, balance) {
            if (err) {
              callback('Error retrieving balance');
            } else {
              if (balance < requestedAmount) {
                callback("You do not have enough balance to withdraw " + requestedAmount + "\n***Current Balance:*** " + balance);
              } else {
                sendTransaction(requestedAmount, withdrawalAddress, function(sentTransaction) {
                  if (!sentTransaction) {
                    callback("Error sending transaction");
                  } else {
                    newBalance = balance - requestedAmount;
                    let sql = "UPDATE member SET available_balance = available_balance - ? WHERE id = ?";
                    let data = [requestedAmount, userID];
                    db.run(sql, data, function(err) {
                      if (err) {
                        console.error(err.message);
                        callback("Error updating database")
                      } else {
                        console.log(`Row(s) updated: ${this.changes}`);
                        let sql = "INSERT  INTO withdrawals(transaction_id, member_id, to_address, amount) VALUES (?, ? ,? ,?)";
                        let data = [sentTransaction, userID, withdrawalAddress, requestedAmount];
                        db.run(sql, data, function(err) {
                          if (err) {
                            console.error(err.message);
                            callback("Error updating database")
                          } else {
                            callback(err, newBalance, sentTransaction, withdrawalAddress);
                          }
                        });
                      }
                    })
                  }
                });
              }
            }
          });
        }
      });
    }
  }
}

function processStakeCommand(userID, requestedStakeAmount, callback) {
  if(!requestedStakeAmount){
    callback("Invalid stake amount (" + requestedStakeAmount + "). Please use a decimal value equal greater than ***1*** kB3 or the word 'all'")
  }else if (isNaN(requestedStakeAmount) || requestedStakeAmount < 1) {
    if(requestedStakeAmount.toLowerCase()==='all'){
      getUserBalance(userID, function(err, balance) {
        if (err) {
          callback('Error retrieving balance');
        }else {
          if(balance>0){
            removeAvailableBalance(userID, balance, function(err) {
              if (err) {
                callback("Unable to adjust your available balance");
              } else {
                addStakeBalance(userID, balance, function(err) {
                  if (err) {
                    callback("Unable to adjust your staking balance");
                  } else {
                    console.log("Processing new stake request")
                    processNewStake(balance, function(err) {
                      if (err) {
                        removeStakeBalance(userID, balance, function(err) {
                          addAvailableBalance(userID, balance, function(err) {
                            callback("Error sending to staking wallet, balance refunded");
                          });
                        });
                      } else {
                        callback(null, balance);
                      }
                    })
                  }
                })
              }
            });
          }else{
            callback('No available balance to stake');
          }
        }
      });
    }else{
      callback("Invalid stake amount (" + requestedStakeAmount + "). Please use a decimal value equal greater than ***1*** kB3 or the word 'all'")
    }
  } else {
    getUserBalance(userID, function(err, balance) {
      if (err) {
        callback('Error retrieving balance');
      } else if (balance < requestedStakeAmount) {
        callback("You do not have enough balance to stake ***" + requestedStakeAmount + "*** kB3\nCurrent Balance: ***" + balance + "***");
      } else {
        removeAvailableBalance(userID, requestedStakeAmount, function(err) {
          if (err) {
            callback("Unable to adjust your available balance");
          } else {
            addStakeBalance(userID, requestedStakeAmount, function(err) {
              if (err) {
                callback("Unable to adjust your staking balance");
              } else {
                console.log("Processing new stake request")
                processNewStake(requestedStakeAmount, function(err) {
                  if (err) {
                    removeStakeBalance(userID, requestedStakeAmount, function(err) {
                      addAvailableBalance(userID, requestedStakeAmount, function(err) {
                        callback("Error sending to staking wallet, balance refunded");
                      });
                    });
                  } else {
                    callback();
                  }
                })
              }
            })
          }
        })
      }
    });
  }
}

function processCancelUnstakeCommand(userID, callback){
  hasPendingUnstake(userID, function(pendingUnstakeBalance){
    if(pendingUnstakeBalance<=0){
      callback("You do not currently have a pending unstake");
    }else{
      //remove all from pending
      db.run("DELETE FROM pending_unstake WHERE member_id=?", [userID], function(err) {
        if (err) {
          return console.log(err.message);
        }
        console.log(`A row has been deleted with rowid ${this.lastID}`);
      });
      callback(null, pendingUnstakeBalance)
    }
  });
}

function processUnStakeCommand(userID, requestedUnstakeAmount, callback) {
  requestedUnstakeAmount=requestedUnstakeAmount-0
  if (!requestedUnstakeAmount || isNaN(requestedUnstakeAmount) || requestedUnstakeAmount < 1) {
    callback("Invalid amount (" + requestedUnstakeAmount + ") to unstake. Please use a decimal value equal greater than ***1*** kB3")
  } else {
    getUserStakeBalance(userID, function(err, stakeBalance) {
      if (err) {
        callback('Error retrieving balance');
      } else if (stakeBalance < requestedUnstakeAmount) {
        callback("You do not have enough staked balance to unstake ***" + requestedUnstakeAmount + "*** kB3\nCurrent Stake Balance: ***" + stakeBalance + "***");
      } else {
        //check amount avail
        console.log("Checking for available amount")
        var avail = false;
        var waitingAddressBalance = 0
        var withdrawAddresses = [];
        var recombineWithWaiting = false;
        //check staking addresses
        async.eachSeries(getStakingAddresses().concat(getWaitingAddresses()), function(data, callback){
          console.log(data);
          checkForCombine(data.address, function(combine){
            if(combine || data.waiting){
              withdrawAddresses.push(data.address);
              //                            waitingAddressBalance+=data.balance;
              recombineWithWaiting = true;
              getAddressBalance(data.address, function(balance){
                waitingAddressBalance+=balance;
                callback();
              })
            }else{
              callback()
            }
          });
        }, function(){
          console.log("Waiting address bal " + waitingAddressBalance)
          //check recombine if true, withdraw
          if(waitingAddressBalance>=requestedUnstakeAmount){
            avail = true;
          }
          if(!avail){

            async.forEach(getWaitingAddresses(), function(data, callback){
              getAddressBalance(data.address, function(balance){
                if(balance>0){
                  withdrawAddresses.push(data.address);
                  waitingAddressBalance+=balance;
                }
                callback();
              })
            }, function(){
              console.log("Waiting address bal " + waitingAddressBalance)
              if(waitingAddressBalance>=requestedUnstakeAmount){
                avail = true;
              }
              if(!avail){
                //check pendings
                var pendingValue = 0
                console.log("Checking pending")
                pendingTransactionOutputs.forEach((data)=>{
                  console.log(data)
                  if(data.amount>0){
                    if(!withdrawAddresses.includes(data.address)){
                      withdrawAddresses.push(data.address)
                    }
                    waitingAddressBalance+=data.amount
                  }
                })
              }
              if(waitingAddressBalance>=requestedUnstakeAmount){
                avail = true;
              }
              if(avail){
                console.log("Available in waiting")
                var inputs = [];
                var value = 0;
                var balancesToDecrease = {}
                console.log("Checking pending unstake outputs")
                async.eachSeries(pendingTransactionOutputs,function(withdrawAddress){
                  console.log("pending unstake ");
                  console.log(withdrawAddress);
                  if(value<requestedUnstakeAmount+getTransactionFee(inputs.length)){
                    inputs.push(withdrawAddress);
                    value+=withdrawAddress.amount
                    var index = pendingTransactionOutputs.indexOf(withdrawAddress)
                    console.log("REmoving " + index + " from pendingTransactionOutputs")
                    pendingTransactionOutputs.splice(index,1);
                  }

                });
                console.log("Checking waiting addresses")
                async.eachSeries(withdrawAddresses,function(withdrawAddress, callback){
                  console.log("waiting address")
                  getOutputsForAddress(withdrawAddress, function(outputs){
                    console.log(outputs);
                    for(var i = 0; i < outputs.length; i++){
                      if(value<requestedUnstakeAmount+getTransactionFee(inputs.length)){
                        inputs.push(outputs[i]);
                        value+=outputs[i].amount
                        if(!Object.keys(balancesToDecrease).includes(withdrawAddress)){
                          balancesToDecrease[withdrawAddress] = outputs[i].amount;
                        }else{
                          balancesToDecrease[withdrawAddress]+=outputs[i].amount;
                        }
                      }
                    }
                    callback();
                  });
                }, function(){
                  if(value < requestedUnstakeAmount){
                    storePendingUnstake(userID, requestedUnstakeAmount, callback)
                  }else{
                    console.log("Requested amount " + requestedUnstakeAmount);
                    console.log("Total val "+ value)
                    console.log("Fees = " + getTransactionFee(inputs.length));
                    console.log("Change =" + (value-getTransactionFee(inputs.length)-requestedUnstakeAmount));
                    console.log("Change address = " + withdrawAddresses[0]);
                    getUserDepositAddress(userID, function(err, depositAddress){
                      if(err){
                        callback(err);
                      }else{
                        console.log("Deposit address = " + depositAddress);
                        var receiving_addresses = {};
                        receiving_addresses[depositAddress] = requestedUnstakeAmount;
                        receiving_addresses[withdrawAddresses[0]] = value-getTransactionFee(inputs.length)-requestedUnstakeAmount
                        createRawTransaction(inputs, receiving_addresses, function(hexString) {
                          console.log("Hex string generated " + hexString);
                          if (!hexString) {
                            callback("Error creating raw transaction")
                          } else {
                            signRawTransaction(hexString, function(hexString) {
                              if (!hexString) {
                                callback("Error signing raw transaction")
                              } else {
                                sendRawTransaction(hexString, function(txID) {
                                  if (!txID) {
                                    callback("Error sending raw transaction")
                                  } else {
                                    console.log("Sent tx " + txID)
                                    //update balance on sending address
                                    //                                                                Object.keys(balancesToDecrease).forEach((data)=>{
                                    //                                                                    increaseAddressBalance(data, -balancesToDecrease[data]);
                                    //                                                                });
                                    storePendingOutput(txID, withdrawAddresses[0])
                                    //2-0.0002-1
                                    //storePendingOutputs - txID : value-getTransactionFee(inputs.length)-requestedUnstakeAmount
                                    //update users staked balance
                                    removeStakeBalance(userID, requestedUnstakeAmount, function(){
                                      if(err){
                                        callback(err)
                                      }else{
                                        callback();
                                      }
                                    })
                                  }
                                })
                              }
                            })
                          }
                        });
                      }
                    });
                  }
                });
              }else{
                storePendingUnstake(userID, requestedUnstakeAmount, callback)
              }
            })
          }else{
            console.log("Waiting address bal " + waitingAddressBalance)
            if(waitingAddressBalance>=requestedUnstakeAmount){
              avail = true;
            }
            if(!avail){
              //check pendings
              var pendingValue = 0
              console.log("Checking pending")
              pendingTransactionOutputs.forEach((data)=>{
                console.log(data)
                if(data.amount>0){
                  if(!withdrawAddresses.includes(data.address)){
                    withdrawAddresses.push(data.address)
                  }
                  waitingAddressBalance+=data.amount
                }
              })
            }
            if(waitingAddressBalance>=requestedUnstakeAmount){
              avail = true;
            }
            if(avail){
              console.log("Available in waiting")
              var inputs = [];
              var value = 0;
              var balancesToDecrease = {}
              console.log("Checking pending unstake outputs")
              async.eachSeries(pendingTransactionOutputs,function(withdrawAddress){
                console.log("pending unstake ");
                console.log(withdrawAddress);
                if(value<requestedUnstakeAmount+getTransactionFee(inputs.length)){
                  inputs.push(withdrawAddress);
                  value+=withdrawAddress.amount
                  var index = pendingTransactionOutputs.indexOf(withdrawAddress)
                  console.log("REmoving " + index + " from pendingTransactionOutputs")
                  pendingTransactionOutputs.splice(index,1);
                }

              });
              console.log("Checking waiting addresses")
              async.eachSeries(withdrawAddresses,function(withdrawAddress, callback){
                console.log("waiting address")
                getOutputsForAddress(withdrawAddress, function(outputs){
                  console.log(outputs);
                  for(var i = 0; i < outputs.length; i++){
                    if(value<requestedUnstakeAmount+getTransactionFee(inputs.length)){
                      inputs.push(outputs[i]);
                      value+=outputs[i].amount
                      if(!Object.keys(balancesToDecrease).includes(withdrawAddress)){
                        balancesToDecrease[withdrawAddress] = outputs[i].amount;
                      }else{
                        balancesToDecrease[withdrawAddress]+=outputs[i].amount;
                      }
                    }
                  }
                  callback();
                });
              }, function(){
                if(value < requestedUnstakeAmount){
                  storePendingUnstake(userID, requestedUnstakeAmount, callback)
                }else{
                  console.log("Requested amount " + requestedUnstakeAmount);
                  console.log("Total val "+ value)
                  console.log("Fees = " + getTransactionFee(inputs.length));
                  console.log("Change =" + (value-getTransactionFee(inputs.length)-requestedUnstakeAmount));
                  console.log("Change address = " + withdrawAddresses[0]);
                  getUserDepositAddress(userID, function(err, depositAddress){
                    if(err){
                      callback(err);
                    }else{
                      console.log("Deposit address = " + depositAddress);
                      var receiving_addresses = {};
                      receiving_addresses[depositAddress] = requestedUnstakeAmount;
                      receiving_addresses[withdrawAddresses[0]] = value-getTransactionFee(inputs.length)-requestedUnstakeAmount
                      createRawTransaction(inputs, receiving_addresses, function(hexString) {
                        console.log("Hex string generated " + hexString);
                        if (!hexString) {
                          callback("Error creating raw transaction")
                        } else {
                          signRawTransaction(hexString, function(hexString) {
                            if (!hexString) {
                              callback("Error signing raw transaction")
                            } else {
                              sendRawTransaction(hexString, function(txID) {
                                if (!txID) {
                                  callback("Error sending raw transaction")
                                } else {
                                  console.log("Sent tx " + txID)
                                  //update balance on sending address
                                  //                                                                Object.keys(balancesToDecrease).forEach((data)=>{
                                  //                                                                    increaseAddressBalance(data, -balancesToDecrease[data]);
                                  //                                                                });
                                  storePendingOutput(txID, withdrawAddresses[0])
                                  //2-0.0002-1
                                  //storePendingOutputs - txID : value-getTransactionFee(inputs.length)-requestedUnstakeAmount
                                  //update users staked balance
                                  removeStakeBalance(userID, requestedUnstakeAmount, function(){
                                    if(err){
                                      callback(err)
                                    }else{
                                      callback();
                                    }
                                  })
                                }
                              })
                            }
                          })
                        }
                      });
                    }
                  });
                }
              });
            }else{
              storePendingUnstake(userID, requestedUnstakeAmount, callback)
            }
          }
        });
      }
    });
  }
}

function storePendingUnstake(userID, requestedAmount, callback){
  console.log("none avail in waiting storing pending unstake");
  hasPendingUnstake(userID, function(hasPending){
    if(hasPending>0){
      callback("You already have a pending unstake transaction for ***" + hasPending + "*** kB3.\nTo cancel please type `!cancelunstake`")
    }else{
      //split 500,000
      var splitAmount = requestedAmount;
      db.parallelize(() => {
        console.log("Split amount " + splitAmount)
        while(splitAmount>=stakingMaxBalance){
          db.run("INSERT INTO pending_unstake(member_id, amount) VALUES (?, ?)", [userID, stakingMaxBalance], (err, result) => {
            if (err) {
              return console.log(err.message);
            }
            console.debug(`A row has been inserted with rowid ${this.lastID}`);
          })
          splitAmount-=stakingMaxBalance;
          console.log("Split amount " + splitAmount)
        }
        db.run("INSERT INTO pending_unstake(member_id, amount) VALUES (?, ?)", [userID, splitAmount], (err, result) => {
          if (err) {
            return console.log(err.message);
          }
          console.log(`A row has been inserted with rowid ${this.lastID}`);
        })
        callback("There are currently no funds available for instant unstaking.\nYour request for ***"+requestedAmount+"*** kB3 has been queued\nPlease type `!cancelunstake` if you wish to cancel this unstake")
      });
    }
  })
}

function storePendingOutput(transaction, changeAddress){
  getStakingWalletTransaction(transaction, function(tx){
    async.each(tx.vout, function(output){
      if(output.scriptPubKey.addresses[0]===changeAddress){
        pendingTransactionOutputs.push({"vout": output.n, "txid": tx.txid, amount: output.value, address: changeAddress})
        console.log("Storing pending output");
        console.log(pendingTransactionOutputs)
      }
    });
  })
}

function processTipCommand(userID, requestedReceiver, requestedTipAmount, callback) {
  if (!requestedReceiver) {
    callback("Invalid tip command. Please use !tip <@user> <amount>");
  } else if (!checkUserExists(requestedReceiver)) {
    callback("Tagged user (" + requestedReceiver + ") does not exist on this server.");
  } else if (!requestedTipAmount || isNaN(requestedTipAmount) || requestedTipAmount < 1) {
    callback("Invalid tip amount (" + requestedTipAmount + "). Please use a decimal value equal greater than 1 kB3")
  } else {
    getUserBalance(userID, function(err, balance) {
      if (err) {
        callback('Error retrieving balance');
      } else if (balance < requestedTipAmount) {
        callback("You do not have enough balance to tip " + requestedTipAmount + " kB3\n***Current Balance:*** " + balance);
      } else {
        removeAvailableBalance(userID, requestedTipAmount, function(err) {
          if (err) {
            callback(err);
          } else {
            if(requestedReceiver!='@everyone'){
              addAvailableBalance(requestedReceiver, requestedTipAmount, function(err) {
                if (err) {
                  callback(err);
                } else {
                  callback(err);
                }
              })
            }else{
              addAllAvailableBalance(Math.round(requestedTipAmount/getUserCount()*10000)/10000, function(err) {
                if (err) {
                  callback(err);
                } else {
                  callback(err, Math.round(requestedTipAmount/getUserCount()*10000)/10000);
                }
              })
            }
          }
        })
      }
    });
  }
}

function processReactDropCommand(userID, reactDropAmount, reactDropEmoji, callback) {
  var emojiRegex = '(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c\ude32-\ude3a]|[\ud83c\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])';
  if (!reactDropAmount) {
    callback("Invalid reactdrop command. Please use !reactdrop <amount> <emoji>");
  } else if (isNaN(reactDropAmount) || reactDropAmount < 1) {
    callback("Invalid reactdrop amount (" + reactDropAmount + "). Please use a decimal value equal or greater than 1 kB3");
  } else if (!reactDropEmoji) {
    callback("Missing reactdrop emoji. Please use !reactdrop <amount> <emoji>");
  } else if (!reactDropEmoji.match(emojiRegex)) {
    callback("Invalid reactdrop emoji (" + reactDropEmoji + "). Please use an emoji from discord only");
  } else if (airDropRunning) {
    callback("There is currently an airdrop already running please wait.");
  } else {
    getUserBalance(userID, function(err, balance) {
      if (err) {
        callback('Error retrieving balance');
      } else if (balance < reactDropAmount) {
        callback("You do not have enough balance to reactdrop ***" + reactDropAmount + "*** kB3\nCurrent Balance: ***" + balance + "***");
      } else {
        removeAvailableBalance(userID, reactDropAmount, function(err) {
          if (err) {
            callback(err);
          } else {
            callback();
          }
        })
      }
    });
  }
}

function processLotteryDropCommand(userID, lotteryDropAmount, callback) {
  if (!lotteryDropAmount) {
    callback("Invalid lottery command. Please use !rolldrop <amount>");
  } else if (isNaN(lotteryDropAmount) || lotteryDropAmount < 1) {
    callback("Invalid lotterydrop amount (" + lotteryDropAmount + "). Please use a decimal value equal or greater than 1 kB3");
  } else if (airDropRunning) {
    callback("There is currently an airdrop already running please wait.");
  } else {
    getUserBalance(userID, function(err, balance) {
      if (err) {
        callback('Error retrieving balance');
      } else if (balance < lotteryDropAmount) {
        callback("You do not have enough balance to lotterydrop ***" + lotteryDropAmount + "*** kB3\nCurrent Balance: ***" + balance + "***");
      } else {
        removeAvailableBalance(userID, lotteryDropAmount, function(err) {
          if (err) {
            callback(err);
          } else {
            callback();
          }
        })
      }
    });
  }
}

function processRollDropCommand(userID, rollDropAmount, callback) {
  if (!rollDropAmount) {
    callback("Invalid rolldrop command. Please use !rolldrop <amount>");
  } else if (isNaN(rollDropAmount) || rollDropAmount < 1) {
    callback("Invalid rolldrop amount (" + rollDropAmount + "). Please use a decimal value equal or greater than 1 kB3");
  } else if (airDropRunning) {
    callback("There is currently an airdrop already running please wait.");
  } else {
    getUserBalance(userID, function(err, balance) {
      if (err) {
        callback('Error retrieving balance');
      } else if (balance < rollDropAmount) {
        callback("You do not have enough balance to lotterydrop ***" + rollDropAmount + "*** kB3\nCurrent Balance: ***" + balance + "***");
      } else {
        removeAvailableBalance(userID, rollDropAmount, function(err) {
          if (err) {
            callback(err);
          } else {
            callback();
          }
        })
      }
    });
  }
}

function processPhraseDropCommand(userID, phraseDropAmount, phraseDropPhrase, callback) {
  if (!phraseDropAmount) {
    callback("Invalid phrasedrop command. Please use !phrasedrop <amount> <phrase>");
  } else if (isNaN(phraseDropAmount) || phraseDropAmount < 1) {
    callback("Invalid phrasedrop amount (" + phraseDropAmount + "). Please use a decimal value equal or greater than 1 kB3");
  } else if (!phraseDropPhrase) {
    callback("Missing phrasedrop phrase. Please use !phrasedrop <amount> <phrase>");
  } else if (airDropRunning) {
    callback("There is currently an airdrop already running please wait.");
  } else {
    getUserBalance(userID, function(err, balance) {
      if (err) {
        callback('Error retrieving balance');
      } else if (balance < phraseDropAmount) {
        callback("You do not have enough balance to phrasedrop ***" + phraseDropAmount + "*** kB3\nCurrent Balance: ***" + balance + "***");
      } else {
        removeAvailableBalance(userID, phraseDropAmount, function(err) {
          if (err) {
            callback(err);
          } else {
            callback();
          }
        })
      }
    });
  }
}

function processDiceCommand(userID, diceHighLow, diceAmount, multiplier, callback) {
  //check high/low
  //check amount is NaN and < 1000
  //check multiplier > 1
  callback(newBalance, diceResult);
}

function processSetWithdrawal(userID, withdrawAddress, callback) {
  if (!withdrawAddress) {
    callback("Invalid setwithdrawal command. Please use !setwithdrawal <address>");
  } else {
    checkValidAddress(withdrawAddress, function(valid) {
      if (!valid) {
        callback("Error validating address");
      } else if (!valid.isvalid) {
        callback("Invalid address specified(" + withdrawAddress + ")");
      } else {
        sql = "UPDATE member SET default_withdrawal = ? WHERE id=?";
        db.run(sql, [withdrawAddress, userID], function(err) {
          if (err) {
            callback("There was an error updating the database");
          } else {
            callback();
          }
        })
      }
    });
  }
}

function processVerifyCommand(userID, address, messageHash, callback){
  if (!address) {
    callback("Invalid verify command. Please use !verify <address> <signmessage hash>");
  } else {
    checkValidAddress(address, function(valid) {
      if (!valid) {
        callback("Error validating address");
      } else if (!valid.isvalid) {
        callback("Invalid address specified(" + address + ")");
      } else {

        if(!messageHash){
          callback("Invalid verify command. Please use !verify <address> <signmessage hash>")
        }else{
          var sql = "SELECT 1 FROM node_owners WHERE Wallet_Address = ?"
          db.get(sql, [address], (err, row) => {
            if (err) {
              callback("Error checking for wallet address")
            }else if (!row) {
              callback("Address does not exist in the B3Lottery database or has already been claimed.")
            }else{
              wallet.verifyMessage(address, messageHash, "B3Lottery", function(err, res){
                if(err){
                  callback("Error with hashed message")
                }else if(res.result){
                  sql = "UPDATE member SET node_balance = node_balance+(SELECT amount+direct_deposit from node_owners WHERE Wallet_Address = ?) WHERE id=?";
                  db.run(sql, [address, userID], function(err) {
                    if (err) {
                      console.error(err.message);
                      callback("Error updating database on member")
                    } else {
                      sql = "DELETE FROM node_owners WHERE Wallet_Address = ?";
                      db.run(sql, [address], function(err) {
                        if(err){
                          console.error(err.message);
                          callback("Error updating database on node address")
                        }else{
                          callback();
                        }
                      });
                    }
                  });
                }else{
                  callback("Message was unable to be verified, please ensure you followed the instructions correctly.");
                }
              })
            }
          });
        }

      }
    });
  }
  //check address !null
  //check is b3 address
  //check messagehash !null
  //check for address in node_owners;
  //check address is verified
  //add balance to member
  //delete address from node_owners;
}

function startReactionDrop(ownerID, amount, messageID, emoji) {
  airDropAmount = amount;
  airDropOwner = ownerID;
  airDropMessage = messageID;
  airDropObjective = emoji;
  airDropRunning = true;
  reactDropRunning = true
  console.log("Reaction drop started amount:" + amount + ", emoji: " + emoji);
  setTimeout(function() {
    endReactionDrop();
  }, airDropTime);
}

function endReactionDrop() {
  console.log("Ending drop")
  payAirdropWinners(function(err) {
    sendEmbededMessage(getEndAirdropFields(err, "React Drop"), bot.users[airDropOwner].username, airDropOwner, bot.users[airDropOwner].avatar, defaultChannelID, function() {
      airDropAmount = undefined;
      airDropOwner = undefined;
      airDropMessage = undefined;
      airDropObjective = undefined;
      airDropRunning = false;
      airDropEntrants = [];
      airDropWinners = [];
      reactDropRunning = false;
    });
  });
}

function startLotteryDrop(ownerID, amount, messageID) {
  airDropAmount = amount;
  airDropOwner = ownerID;
  airDropMessage = messageID;
  airDropObjective = Math.floor(Math.random() * 101);
  airDropRunning = true;
  lotteryDropRunning = true;
  lotteryDropWinningValue = 101;
  console.log("Lottery drop started amount:" + amount + ", value: " + airDropObjective);
  setTimeout(function() {
    endLotteryDrop();
  }, airDropTime);
}

function endLotteryDrop() {
  console.log("Ending drop")
  if (airDropWinners.length === 0) {
    airDropWinners = airDropEntrants;
  }
  payAirdropWinners(function(err) {
    sendEmbededMessage(getEndAirdropFields(err, "Lottery Drop"), bot.users[airDropOwner].username, airDropOwner, bot.users[airDropOwner].avatar, defaultChannelID, function() {
      airDropAmount = undefined;
      airDropOwner = undefined;
      airDropMessage = undefined;
      airDropObjective = undefined;
      airDropRunning = false;
      lotteryDropRunning = false;
      airDropEntrants = [];
      airDropWinners = [];
    });
  });
}

function startRollDrop(ownerID, amount, messageID) {
  airDropAmount = amount;
  airDropOwner = ownerID;
  airDropMessage = messageID;
  airDropRunning = true;
  rollDropRunning = true;
  rollDropHighestValu = -1;
  console.log("Roll drop started amount:" + amount);
  setTimeout(function() {
    endRollDrop();
  }, airDropTime);
}

function endRollDrop() {
  console.log("Ending drop")
  payAirdropWinners(function(err) {
    sendEmbededMessage(getEndAirdropFields(err, "Roll Drop"), bot.users[airDropOwner].username, airDropOwner, bot.users[airDropOwner].avatar, defaultChannelID, function() {
      airDropAmount = undefined;
      airDropOwner = undefined;
      airDropMessage = undefined;
      airDropObjective = undefined;
      airDropRunning = false;
      rollDropRunning = false;
      rollDropHighestValue = -1;
      airDropEntrants = [];
      airDropWinners = [];
    });
  });
}

function startPhraseDrop(ownerID, amount, messageID, phrase) {
  airDropAmount = amount;
  airDropOwner = ownerID;
  airDropMessage = messageID;
  airDropObjective = phrase;
  airDropRunning = true;
  phraseDropRunning = true
  console.log("Phrase drop started amount:" + amount + ", phrase: " + phrase);
  setTimeout(function() {
    endPhraseDrop();
  }, airDropTime);
}

function endPhraseDrop() {
  if (airDropWinners.length === 0) {
    airDropWinners = airDropEntrants;
  }
  console.log("Ending drop")
  payAirdropWinners(function(err) {
    sendEmbededMessage(getEndAirdropFields(err, "Phrase Drop"), bot.users[airDropOwner].username, airDropOwner, bot.users[airDropOwner].avatar, defaultChannelID, function() {
      airDropAmount = undefined;
      airDropOwner = undefined;
      airDropMessage = undefined;
      airDropObjective = undefined;
      airDropRunning = false;
      airDropEntrants = [];
      airDropWinners = [];
      phraseDropRunning = false;
    });
  });
}


function payAirdropWinners(callback) {
  if (airDropWinners.length < airDropMinEntrants) {
    addAvailableBalance(airDropOwner, airDropAmount, function(err) {
      if (err) {
        console.log(err);
      }
      callback("<@" + airDropOwner + "> Unfortunatly not enough members entered the airdrop within the 5 minutes. Your balance has been refunded" + (err ? "\n" + err : ""));
    });
  } else {
    var amountEach = roundToThreeDecimals(airDropAmount / airDropWinners.length);
    var amountLeft = airDropAmount - amountEach * airDropWinners.length
    var error;
    db.parallelize(() => {
      addAvailableBalance(ownerID, amountLeft, function(err) {
        if (err) {
          error = err;
        }
      })
      async.each(airDropWinners, function(userID, callback) {
        addAvailableBalance(userID, amountEach, function(err) {
          if (err) {
            error = err;
          }
          callback();
        })
      }, function() {
        callback(error);
      });
    });
  }
}

function getNewAddress(id, callback) {
  wallet.getNewAddress(id, function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  });
}

function getNewStakingAddress(waiting, callback) {
  stakingWallet.getNewAddress(function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      db.run("INSERT INTO staking_addresses(address, waiting) VALUES (?, ?)", [res.result, waiting], (err, result) => {
        if (err) {
          return console.log(err.message);
        }
        console.log(`A row has been inserted with rowid ${this.lastID}`);
        callback(res.result);
      })
    }
  });
}

function getWalletInfo(callback) {
  stakingWallet.getInfo(function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  });
}

function getStakingInfo(callback) {
  stakingWallet.getStakingInfo(function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  });
}

function checkValidAddress(address, callback) {
  wallet.validateaddress(address, function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  });
}

function sendTransaction(amount, address, callback) {
  console.log("Sending tx")
  wallet.sendtoaddress(address, amount, function(err, res) {
    if (err) {
      console.log("Error sending tx")
      console.log(err)
      callback();
    } else {
      callback(res.result);
    }
  });
}

function getTransaction(transaction, callback) {
  wallet.getTransaction(transaction, function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  })
}

function getStakingWalletTransaction(transaction, callback) {
  stakingWallet.getTransaction(transaction, function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  })
}

function checkUserExists(userID) {
  console.log(userID);
  if(userID==='@everyone'){
    return true;
  }else{
    return bot.users[userID]
  }
}

function getUserCount(){
  return Object.keys(bot.users).length;
}

function addAvailableBalance(userID, amount, callback) {
  let sql = "UPDATE member SET available_balance = available_balance+? WHERE id = ?";
  db.run(sql, [amount, userID], function(err) {
    if (err) {
      console.error(err.message);
      callback("Error updating database")
    } else {
      callback();
    }
  });
}

function addAllAvailableBalance(amount, callback) {
  let sql = "UPDATE member SET available_balance = available_balance+?";
  db.run(sql, [amount], function(err) {
    if (err) {
      console.error(err.message);
      callback("Error updating database")
    } else {
      callback();
    }
  });
}

function removeAvailableBalance(userID, amount, callback) {
  let sql = "UPDATE member SET available_balance = available_balance-? WHERE id = ?";
  db.run(sql, [amount, userID], function(err) {
    if (err) {
      console.error(err.message);
      callback("Error updating database")
    } else {
      callback();
    }
  });
}

function addStakeBalance(userID, amount, callback) {
  let sql = "UPDATE member SET staking_balance = staking_balance+? WHERE id = ?";
  db.run(sql, [amount, userID], function(err) {
    if (err) {
      console.error(err.message);
      callback("Error updating database")
    } else {
      callback();
    }
  });
}

function removeStakeBalance(userID, amount, callback) {
  let sql = "UPDATE member SET staking_balance = staking_balance-? WHERE id = ?";
  db.run(sql, [amount, userID], function(err) {
    if (err) {
      console.error(err.message);
      callback("Error updating database")
    } else {
      callback();
    }
  });
}

function getReferralCount(userID, callback) {
  let sql = "SELECT COUNT(*) as count FROM referrals WHERE referred_by = ?";
  db.get(sql, [userID], (err, row) => {
    if (err) {
      console.error(err.message);
      callback(-1);
    } else {
      callback(row ? row.count : 0);
    }
  });
}

function getReferralEarnings(userID, callback) {
  let sql = "SELECT round(earnt_referrals,5) as earnt_referrals, count_referrals FROM member WHERE id = ?";
  db.get(sql, [userID], (err, row) => {
    if (err) {
      callback(-1);
    } else {
      if (row) {
        callback(row.count_referrals, row.earnt_referrals)
      } else {
        console.error("Could not find referral info")
        callback(0, 0);
      }
    }
  });
}

function checkDefaultSet(userID, callback) {
  let sql = "SELECT 1 FROM member WHERE id=? AND default_withdrawal IS NOT NULL";
  db.get(sql, [userID], (err, row) => {
    if (err) {
      callback("Error retrieving default withdrawal address");
    } else {
      if (row) {
        callback(err, true)
      } else {
        callback(err, false)
      }
    }
  })
}

function getDefaultWithdrawalAddress(userID, callback) {
  let sql = "SELECT default_withdrawal FROM member WHERE id = ?";
  db.get(sql, [userID], (err, row) => {
    if (err) {
      callback("Error retrieving your default withdrawal address");
    } else {
      if (row) {
        callback(err, row.default_withdrawal)
      } else {
        callback("Could not find default withdrawal address");
      }
    }
  });
}

function getStakeLocation(amount, callback) {
  getStakingAddress(function(err, stakingAddress) {
    if (err) {
      console.log(err)
    }
    if (!stakingAddress) {
      //get waiting wallet
      //if no waiting wallet
      //create new waiting wallet
      console.log("No staking addresses, getting waiting address")
      getWaitingWallet(amount, function(err, waitingWallet) {
        //add balance to this waiting wallet
        console.log("got waiting address combine to this address!")
        console.log(waitingWallet);
        callback(err, waitingWallet)
      });
    } else {
      //add balance to this address recombine
      console.log("got staking address add to this address")
      callback(err, stakingAddress)
    }
  });
}

function storeLastTransaction(address, transaction) {
  let sql = "UPDATE staking_addresses SET last_transaction = ? WHERE address = ?";
  db.run(sql, [transaction, address], function(err) {
    if (err) {
      console.log("errer");
      return console.log(err.message);
    }
    console.log("SQL updated")
    console.log(`A row has been updated with rowid ${this.lastID}`);
  });
  var found = false;
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].address === address) {
      found = true;
      stakingAddresses[i].lastTx = transaction;
      console.log("Updated lastTX on staking address")
      break;
    }
  }
  if (!found) {
    console.log("error finding address to update lastTX")
  }
}

function storeLastStakeTransaction(address, transaction) {
  let sql = "UPDATE staking_addresses SET last_stake_transaction = ? WHERE address = ?";
  db.run(sql, [transaction, address], function(err) {
    if (err) {
      console.log("errer");
      return console.log(err.message);
    }
    console.log("SQL updated")
    console.log(`A row has been updated last stake setwith rowid ${this.lastID}`);
  });
  var found = false;
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].address === address) {
      found = true;
      stakingAddresses[i].lastStakingTx = transaction;
      console.log("Updated lastTX on staking address")
      break;
    }
  }
  if (!found) {
    console.log("error finding address to update lastStakingTx")
  }
}

//function increaseAddressBalance(address, balance) {
//    console.log("Increasing address balance " + address + " by " + balance)
//    let sql = "UPDATE staking_addresses SET balance = round(balance+?,8) WHERE address = ?";
//    db.run(sql, [balance, address], function(err) {
//        if (err) {
//            console.log("error updating address amount");
//            return console.log(err.message);
//        }
//        console.log("SQL updated")
//        console.log(`A row has been updated with increased balance rowid ${this.lastID}`);
//    });
//    var found = false;
//    for (var i = 0; i < stakingAddresses.length; i++) {
//        if (stakingAddresses[i].address === address) {
//            found = true;
//            stakingAddresses[i].balance += balance;
//            console.log("Updated balance on staking address")
//            break;
//        }
//    }
//    if (!found) {
//        console.log("error finding address to update balance")
//    }
//}

//function storeAddressBalance(address, balance) {
//    console.log("Storing address balance " + address + " " + balance)
//    let sql = "UPDATE staking_addresses SET balance = ? WHERE address = ?";
//    db.run(sql, [balance, address], function(err) {
//        if (err) {
//            console.log("error updating address amount");
//            return console.log(err.message);
//        }
//        console.log("SQL updated")
//        console.log(`A row has been updated with rowid ${this.lastID}`);
//    });
//    var found = false;
//    for (var i = 0; i < stakingAddresses.length; i++) {
//        if (stakingAddresses[i].address === address) {
//            found = true;
//            stakingAddresses[i].balance = balance;
//            console.log("Updated balance on staking address")
//            break;
//        }
//    }
//    if (!found) {
//        console.log("error finding address to update balance")
//    }
//}

function processNewStake(amount, callback) {
  //send staked amount to staking wallet address
  //once confirmed combine to that address - listunspent

  //find staking wallet
  console.log("Getting stake location")
  getStakeLocation(amount, function(err, targetWallet) {
    if (err) {
      callback(err);

    } else {
      console.log("TRAGET WALLET")
      sendTransaction(amount, targetWallet.address, function(transaction) {
        //store last transaction under target wallet
        console.log("SENT TX")
        if (!transaction) {
          callback("Error sending transaction")
        } else {
          callback(err, transaction)
        }
      });
    }
  });
}

function getWaitingWallet(addedBalance, callback) {
  var selected;
  async.each(stakingAddresses, function(data, callback) {
    getAddressBalance(data.address, function(balance){
      if (balance + (addedBalance - 0) < stakingMaxBalance && data.waiting) {
        selected = data;
      } else if (data.waiting) {
        setStakingAddress(data);
      }
      callback();
    })
  }, function(err) {
    console.log("callback in get waiting address")
    if (!selected || err) {
      if (!selected) {
        console.log("Creating new staking address")
        getNewStakingAddress(true, function(stakingAddress) {
          var data = {
            address: stakingAddress,
            //                        balance: 0,
            waiting: true,
            lastTx: null,
            lastStakingTx: null
          }
          stakingAddresses.push(data)
          console.log("Waiting wallet address generated " + stakingAddress);
          selected = data;
          callback();
        });
      } else {
        console.error(err.message);
        callback(err)
      }
    } else {
      callback(err, selected);
    }
  });
}

function setStakingAddress(data) {
  db.run("UPDATE staking_addresses SET waiting=0 WHERE address=?", [data.address], function(err) {
    if (err) {
      return console.log(err.message);
    }
    console.log(`A row has been updated with rowid ${this.lastID}`);
  });
  data.waitig = false;
  for (var i = 0; i < stakingAddresses; i++) {
    if (stakingAddresses[i].address === data.address) {
      stakingAddresses[i] = data;
    }
  }
  stakingAddresses.push(data);
}

function getStakingAddress(callback) {
  var selected;
  async.each(stakingAddresses, function(data, callback) {
    console.log(data)
    if(!data.waiting){
      checkForCombine(data.address, function(combine, nooutputs){
        if(combine || nooutputs){
          selected = data;
        }
        callback();
      });
    }else{
      callback();
    }
  }, function(err) {
    console.log("callback in getstaking address")
    if (!selected) {
      if (!selected) {
        var err = "Error finding staking address to send to"
      }
      console.error(err.message);
      callback(err)
    } else {
      callback(err, selected);
    }
  });
}

function getTransactionConfirmations(transaction, callback) {
  console.log("Getting transaction confirmations " + transaction);
  stakingWallet.getTransaction(transaction, function(err, res) {
    if (err) {
      callback(err);
    } else {
      callback(err, res.result.confirmations);
    }
  });
}

function getStakingAddressBalance(address, callback) {
  var found = false;
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].address === address) {
      getAddressBalance(stakingAddresses[i].address, function(balance){
        found = true;
        callback(balance)
        break;
      })
    }
  }
  if(!found){
    callack();
  }
}

function getOutputsForAddresses(addresses, callback) {
  console.log("addresses");
  console.log(addresses);
  stakingWallet.listunspent(1, 10000, addresses, function(err, res) {
    if (err) {
      console.log(err);
    } else {
      callback(res.result);
    }
  });
}

function getOutputsForAddress(address, callback) {
  console.log("Getting outputs for " + address)
  var list = [];
  list.push(address)
  stakingWallet.listunspent(1, 10000, list, function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  });
}

function hasLastStake(address) {
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].address === address) {
      return stakingAddresses[i].lastStakingTx !== null;
    }
  }
  return
}

function getLastStakeTX(address) {
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].address === address) {
      return stakingAddresses[i].lastStakingTx;
    }
  }
  return
}

function getLastTX(address) {
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].address === address) {
      return stakingAddresses[i].lastTx;
    }
  }
  return
}

function  checkForCombine(address, callback) {
  console.log("checking for combine on " + address)
  getOutputsForAddress(address, function(outputs) {
    if (outputs.length >= 1) {
      if(checkWaitingAddress(address)){
        console.log("Is waiting dont combine")
        callback(false);
      }else if (hasLastStake(address)) {
        var tx = getLastStakeTX(address);
        if (!tx) {
          console.log("no last Staketx")
          callback(false);
        } else {
          getTransactionConfirmations(tx, function(err, confirmations) {
            if (err) {
              console.log(err)
            }
            if (confirmations < stakingMinConf) {
              callback(true);
            } else {
              console.log("last stake <31 > 50")
              callback(false);
            }
          });
        }
      } else {
        var tx = getLastTX(address);
        if (!tx) {
          console.log("no last tx")
          callback(true);
        } else {
          getTransactionConfirmations(tx, function(err, confirmations) {
            if (err) {
              console.log(err)
            }
            getStakingAddressBalance(address, function(stakingAddressBalance){
              if ((!stakingAddressBalance || stakingAddressBalance < stakingMaxBalance) && confirmations < stakingMinConf) {//TODO 300
                callback(true);
              } else {
                console.log("bal high  or confirmations to high to combine")
                callback(false);
              }
            })
          });
        }
      }
    } else {
      console.log("outputs < 1");
      callback(false, true)
    }
  });
}

function combineInputs(receivingAddress, outputs, callback){
  var value = 0;
  var inputs = [];
  var receiving_address = {};
  var inputCount = 0;
  var storedOutputs = []
  outputs.forEach((data) => {
    if(inputCount<20){
      inputs.push({
        "txid": data.txid,
        "vout": data.vout
      });
      value += data.amount;
      inputCount +=1;
    }else{
      storedOutputs.push({
        "txid": data.txid,
        "vout": data.vout,
        "amount": data.amount
      });
    }
    //                increaseAddressBalance(data.address, -data.amount)
  });
  pendingTransactionOutputs.forEach((data)=>{
    inputs.push(data);
    console.log("adding pending amount " + data.amount)
    value += data.amount;
  });
  for(var i = 0; i < inputs.length; i++){
    if(pendingTransactionOutputs.includes(inputs[i])){
      pendingTransactionOutputs.splice(i,1);
    }
  }

  var txFee = getTransactionFee(inputs.length)
  console.log("transaction fee =" + txFee);
  receiving_address[receivingAddress] = value - txFee;
  console.log(inputs)
  console.log();
  console.log(receiving_address)
  createRawTransaction(inputs, receiving_address, function(hexString) {
    console.log("Hex string generated " + hexString);
    if (!hexString) {
      callback("Error creating raw transaction")
    } else {
      signRawTransaction(hexString, function(hexString) {
        if (!hexString) {
          callback("Error signing raw transaction")
        } else {
          sendRawTransaction(hexString, function(txID) {
            if (!txID) {
              callback("Error sending raw transaction")
            } else {
              if(storedOutputs.length>0){
                getStakingWalletTransaction(txID, function(result){
                  var output = {};
                  output["txid"] = result["txid"];
                  result["vout"].forEach((data)=>{
                    var address = data["scriptPubKey"]["addresses"][0];
                    if(address===receivingAddress){
                      output["vout"] = data["n"];
                      output["amount"] = data["value"];
                    }
                  })
                  storedOutputs.push(output);
                  combineInputs(receivingAddress, storedOutputs, callback);
                });
              }else{
                storeLastTransaction(receivingAddress, txID);
                if (hasLastStake(receivingAddress)) {
                  storeLastStakeTransaction(receivingAddress, txID);
                  callback("Inputs recombined!");
                }
                callback("Inputs recombined!");
              }
            }
          })
        }
      })
    }
  });
}

function combineInputsOnAddresses(receivingAddress, addresses, callback) {
  if(!addresses.includes(receivingAddress)){
    addresses.push(receivingAddress);
  }
  getOutputsForAddresses(addresses, function(outputs) {
    if (!outputs) {
      callback('Error getting inputs for addresses')
    } else {
      var value = 0;
      var inputs = [];
      var receiving_address = {};
      var inputCount = 0;
      var storedOutputs = []
      outputs.forEach((data) => {
        if(inputCount<20){
          inputs.push({
            "txid": data.txid,
            "vout": data.vout
          });
          value += data.amount;
          inputCount +=1;
        }else{
          storedOutputs.push({
            "txid": data.txid,
            "vout": data.vout,
            "amount": data.amount
          });
        }
        //                increaseAddressBalance(data.address, -data.amount)
      });
      pendingTransactionOutputs.forEach((data)=>{
        inputs.push(data);
        console.log("adding pending amount " + data.amount)
        value += data.amount;
      });
      for(var i = 0; i < inputs.length; i++){
        if(pendingTransactionOutputs.includes(inputs[i])){
          pendingTransactionOutputs.splice(i,1);
        }
      }

      var txFee = getTransactionFee(inputs.length)
      console.log("transaction fee =" + txFee);
      receiving_address[receivingAddress] = value - txFee;
      console.log(inputs)
      console.log();
      console.log(receiving_address)
      createRawTransaction(inputs, receiving_address, function(hexString) {
        console.log("Hex string generated " + hexString);
        if (!hexString) {
          callback("Error creating raw transaction")
        } else {
          signRawTransaction(hexString, function(hexString) {
            if (!hexString) {
              callback("Error signing raw transaction")
            } else {
              sendRawTransaction(hexString, function(txID) {
                if (!txID) {
                  callback("Error sending raw transaction")
                } else {
                  if(storedOutputs.length>0){
                    getStakingWalletTransaction(txID, function(result){
                      var output = {};
                      output["txid"] = result["txid"];
                      result["vout"].forEach((data)=>{
                        var address = data["scriptPubKey"]["addresses"][0];
                        if(address===receivingAddress){
                          output["vout"] = data["n"];
                          output["amount"] = data["value"];
                        }
                      })
                      storedOutputs.push(output);
                      combineInputs(receivingAddress, storedOutputs, callback);
                    });
                  }else{
                    storeLastTransaction(receivingAddress, txID);
                    if (hasLastStake(receivingAddress)) {
                      storeLastStakeTransaction(receivingAddress, txID);
                      callback("Inputs recombined!");
                    }
                    callback("Inputs recombined!");
                  }
                }
              })
            }
          })
        }
      });
    }
  })
}

function combineInputsOnAddress(address, callback) {
  getOutputsForAddress(address, function(outputs) {
    if (!outputs) {
      callback('Error getting inputs for address')
    } else {
      var value = 0;
      var inputs = [];
      var receiving_address = {};
      outputs.forEach((data) => {
        inputs.push({
          "txid": data.txid,
          "vout": data.vout
        });
        value += data.amount;
      });
      //                pendingTransactionOutputs.push({"vout": output.n, "txid": tx.txid, amount: output.value, address: changeAddress})
      pendingTransactionOutputs.forEach((data)=>{
        inputs.push(data);
      });
      var txFee = getTransactionFee(outputs.length)
      console.log(txFee);
      receiving_address[address] = value - txFee;
      createRawTransaction(inputs, receiving_address, function(hexString) {
        console.log("Hex string generated " + hexString);
        if (!hexString) {
          callback("Error creating raw transaction")
        } else {
          signRawTransaction(hexString, function(hexString) {
            if (!hexString) {
              callback("Error signing raw transaction")
            } else {
              sendRawTransaction(hexString, function(txID) {
                if (!txID) {
                  callback("Error sending raw transaction")
                } else {
                  //                                    storeAddressBalance(address, value - txFee)
                  //store last tx? anad stake and balance
                  storeLastTransaction(address, txID);
                  if (hasLastStake(address)) {
                    storeLastStakeTransaction(address, txID);
                    callback("Inputs recombined!");
                    storePendingOutput(txID, address);
                  };
                }
              })
            }
          })
        }
      });
    }
  })
}

function createRawTransaction(inputs, receiving_addresses, callback) {
  stakingWallet.createRawTransaction(inputs, receiving_addresses, function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  });
}

function signRawTransaction(hexString, callback) {
  stakingWallet.signRawTransaction(hexString, function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result.hex);
    }
  });
}

function sendRawTransaction(hexString, callback) {
  stakingWallet.sendRawTransaction(hexString, function(err, res) {
    if (err) {
      console.log(err);
      callback();
    } else {
      callback(res.result);
    }
  });
}

function getTransactionFee(numberOfInputs) {
  var fee;
  var byteCounter = 226;
  for (var i = 0; i < numberOfInputs; i++) {
    byteCounter += 148;
  }
  fee = Math.floor(byteCounter / 1000);
  if (fee == 0) {
    fee = 1;
  }
  fee += 1;
  return fee / 10000;
}

function loadPendingStakes() {
  db.all("SELECT * FROM pending_stakes", (err, rows) => {
    async.each(rows, function(row, callback) {
      getStakingWalletTransaction(row.transaction_id, function(transaction) {
        var txid = transaction.txid;
        var confirmations = transaction.confirmations
        var amount;
        if(!transaction.details[transaction.details.length - 1]){
          callback()
        }else{
          var address = transaction.details[transaction.details.length - 1].address
          var vout = 0
          for (var i = 0; i < transaction.vout.length; i++) {
            if(transaction.vout[i].scriptPubKey.addresses!=undefined){
              if (transaction.vout[i].scriptPubKey.addresses[0] === address) {
                amount = transaction.vout[i].value;
                vout = i;
              }
            }
          }
          var category = transaction.details[transaction.details.length - 1].category
          var recombined = transaction.details.length === 2
          var pendingOutput = transaction.details.length === 3
          pendingStakes.push({
            txid: txid,
            confirmations: confirmations,
            amount: amount,
            vout: vout,
            category: category,
            address: address,
            recombined: recombined,
            pendingOutput: pendingOutput
          });
          callback();
        }

      });
    }, function() {
      console.log(pendingStakes.length + " pending stakes loaded");
    });
  });
}

function getMemberStakingData(callback) {
  db.all("SELECT id, round(staking_balance/(SELECT SUM(staking_balance) FROM member WHERE staking_balance >0),5) as total_owned, referred_by from member where staking_balance > 0;", (err, rows) => {
    if (err) {
      console.log("Error loading member stakes!")
    }
    callback(rows);
  });
}

function addressInPool(address) {
  for (var i = 0; i < stakingAddresses.length; i++) {
    if (stakingAddresses[i].address === address) {
      return true;
    }
  }
  return false;
}

function transactionIsPending(txid) {
  for (var i = 0; i < pendingStakes.length; i++) {
    if (pendingStakes[i].txid === txid) {
      return true
    }
  }
  return false;
}



stdin.addListener("data", function(d) {
  // note:  d is an object, and when converted to a string it will
  // end with a linefeed.  so we (rather crudely) account for that
  // with toString() and then trim()
  var args = d.toString().trim().split(' ').filter(Boolean);
  if(!args[0]){
    return
  }
  var command = args[0].toLowerCase();

  args = args.splice(1);
  if(command==='q'){
    process.exit(1);
  }else if(command==='getwallets'){
    stakingAddresses.forEach((data)=>{
      console.log(data);
    })
  }else if(command=='stakeeveryone'){
    processStakeCommand(userID, requestedStakeAmount, function(err, bal) {

    });
  }else if(command=='getpendingstakes'){
    pendingStakes.forEach((data)=>{
      console.log(data);
    })
  }else if(command === 'getmember'){
    db.get("SELECT * FROM member where id=?",[args[0]], (err, row)=>{
      //            rows.forEach((data)=>{
      console.log(row);
      //            })5299.99980000
    })
  }else if(command === 'setstakebalance'){
    console.log("set stake balance")
    db.run("update member set staking_balance=? where id=?",[args[1],args[0]], (err)=>{
      if(err){
        console.log(err);
      }else{
        console.log(`A row has been updated with rowid ${this.lastID}`);
      }
    })
  }else if(command === 'removeconfirmedstaking'){
    console.log("remove confirmed staking")
    db.run("delete from confirmed_stake_deposits where transaction_id=?",[args[0]], (err)=>{
      if(err){
        console.log(err);
      }else{
        console.log(`A row has been updated with rowid ${this.lastID}`);
      }
    })
  }else if(command === 'getnoderewards'){
    console.log("getnoderewards")
    console.log(nodeRewards);
  }else if(command === 'setwalletbalance'){
    console.log("set wallet balance")
    //address TEXT(36), balance REAL DEFAULT 0
    db.run("update staking_addresses set balance=? where address=?",[args[1],args[0]], (err)=>{
      if(err){
        console.log(err);
      }else{
        console.log(`A row has been updated with rowid ${this.lastID}`);
        loadDatabaseData();
      }
    })
  }else if(command === 'removependingstake'){
    console.log("removing pending stake")
    db.run("delete from pending_stakes where transaction_id=?",[args[0]], (err)=>{
      if(err){
        console.log(err);
      }else{
        console.log(`A row has been updated with rowid ${this.lastID}`);
        for(var i =0; i < pendingStakes; i++){
          if(pendingStakes.txid===args[0]){
            pendingStakes.splice(i,1);
          }
        }
      }
    })

  }else if(command === 'matchbalances'){
    checkBalancesMatchOwner(function(){

    });
  }else if(command==='getaddressbalance'){
    getAddressBalance(args[0], function(balance){
      console.log(balance);
    })

  }else if(command === 'stakemessage'){

    sendStakeMessage(args[0], args[1])

  }else if(command === 'recombine'){
    var addressesToRecombine = [];

    async.forEach(getWaitingAddresses(), function(data, callback){
      console.log("Checking " + data.address);
      getAddressBalance(data.address,function(balance){
        if(balance>0){
          console.log("Balance > 0  " + balance)
          addressesToRecombine.push(data.address);
        }
        callback();
      })
    },function(){

      if(addressesToRecombine.length>0){ //TODO remove???
        addressesToRecombine.push('SWgz1dP1YeQjextMPHjVxpC5BCYamnQV6k');
        if(addressesToRecombine.length>0){
          // console.log("Combining")
          // combineInputsOnAddresses('SWgz1dP1YeQjextMPHjVxpC5BCYamnQV6k', addressesToRecombine, function(res){
          checkBalancesMatchOwner(function(){
            console.log("not combining")
          });
          // });
        }else{
          checkBalancesMatchOwner(function(){
            console.log("Matched")
          });
        }
      }else{
        getOutputsForAddress("SWgz1dP1YeQjextMPHjVxpC5BCYamnQV6k", function(outputs){
          if(outputs.length > 1){
            addressesToRecombine.push('SWgz1dP1YeQjextMPHjVxpC5BCYamnQV6k');
          }
          if(addressesToRecombine.length>0){
            console.log("Combining")
            // combineInputsOnAddresses('SWgz1dP1YeQjextMPHjVxpC5BCYamnQV6k', addressesToRecombine, function(res){
            checkBalancesMatchOwner(function(){
              console.log("Not combining")
            });
            // });
          }else{
            checkBalancesMatchOwner(function(){
              console.log("Matched")
            });
          }
        })
      }
    })
  }else if(command!=''){
    console.log("no command")
  }else{
    console.log('\n');
  }
});

function updateExistingMembers(){
  console.log("Updating members")
  var list = Object.keys(bot.users)
  db.all("SELECT id,available_balance, staking_balance, pending_balance, default_withdrawal FROM member WHERE id NOT IN ( " + list.map(function(){ return '?' }).join(',') + ")",list, function(err,rows) {

    if (err) {
      return console.log(err.message);
    }
    db.parallelize(() => {
      rows.forEach((data)=>{
        if(data.pending_balance===0){
          //                    if(!data.default_withdrawal){
          //                        if(data.staking_balance>0 || data.available_balance >0){
          //                            //send balance to available_balance
          //                            db.run("UPDATE member SET available_balance=available_balance+?, staking_balance=staking_balance+? WHERE id=?",[data.available_balance, data.staking_balance, ownerID], function(err){
          //                                if(err){
          //                                    console.log(err);
          //                                }
          //                                console.log(`A row has been inserted with rowid ${this.lastID}`);
          //                                db.run("DELETE FROM member WHERE id=?",[data.id], function(err){
          //                                    if (err) {
          //                                        return console.log(err.message);
          //                                    }
          //                                })
          //                            })
          //                        }
          //                    }else{
          //                        if(data.staking_balance>0){
          //                            processUnStakeCommand(data.id,data.staking_balance,function(err){
          //                                console.log(err);
          //                            })
          //                        }else if(data.available_balance>0){
          //                            processWithdrawCommand(data.id, data.available_balance, data.default_withdrawal, function(err, newBalance, transaction, withdrawnAddress) {
          //                                //                                sendEmbededMessage(getWithdrawMessageFields(err, data.available_balance, data.default_withdrawal, newBalance, transaction), user, data.id, userHash, channelID);
          //                            });
          //                        }else{
          //                            db.run("DELETE FROM member WHERE id=?",[data.id], function(err){
          //                                if (err) {
          //                                    return console.log(err.message);
          //                                }
          //                            })
          //                        }
          //                        //send to user
          //                    }
        }
      });
    });
  })
}

setInterval(function(){
  checkBalancesMatchOwner(function(){
    console.log("Checked balances")
    updateExistingMembers();
  })
}, 360000)

//TODO hourly timer for leavers and payout balance
