var fs = require('fs');
const async = require("async");
var RpcClient = require('b3coind-rpc');
var exec = require('child_process').exec;
const request = require('request');

const stdin = process.openStdin();


var baseDir = '/root';
var blockNotify = '/root/d3xpool/Node\ bot/blocknotify.js'
var configDir = baseDir+'/nodeConfigurations/';
var walletDatDir = baseDir+'/nodeWalletDats/';
var walletDir = baseDir+'/nodeWallets/';
var walletDirName = 'node';
var donationAddress = 'STuH1dTuq7CoJtJtmbNbB6U8vFg56P7taw';
var donationPrivkey = 'PfHA983fxq8s79mCGwEBfvqcDDnscbHVJyiKFq1m8qYgkXnnfxNz';

var walletCount = 8;
var nodeCountPercentage = 1;
var nodeCountUsagePosition = 0;

var walletLocations = [];
var availableIps = []

var nodes;

var nodeWallets = {}// 0: [outputs to host];

//load IPs;
var interfaces = require('os').networkInterfaces();

interfaces.eth0.forEach((data)=>{
    if(data.family==='IPv6'&& data.scopeid===0){
        availableIps.push(data.address);
    } 
});

//load users
//use folders for each directory

var deleteFolderRecursive = function(path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function(file, index){
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};
deleteFolderRecursive(configDir);


if (!fs.existsSync(configDir)){
    fs.mkdirSync(configDir);
}
if (!fs.existsSync(walletDir)){
    fs.mkdirSync(walletDir);
}
if (!fs.existsSync(walletDatDir)){
    fs.mkdirSync(walletDatDir);
}


for(var i = 0; i < walletCount; i++){
    if (!fs.existsSync(walletDir+walletDirName+i+'/')){
        fs.mkdirSync(walletDir+walletDirName+i+'/');
    }
    walletLocations[i] = walletDir+walletDirName+i+'/';
}

//load nodes
fs.readFile('../nodeData.json', function read(err, data) {
    if (err) {
        throw err;
    }
    nodes = JSON.parse(data)
    start();
});


function start(){
    console.log(Object.keys(nodes).length + ' Nodes loaded');
    console.log("Using " +Math.floor(Object.keys(nodes).length*nodeCountPercentage) + "nodes position " + nodeCountUsagePosition*Math.floor(Object.keys(nodes).length*nodeCountPercentage) );
    splitNodes();
    //create configs per node
    createConfigs(function(){
        //create clients
        createClients(function(){
            createWalletDats(function(){
                console.log("Completed startup");
                async.each(nodeWallets, function(nodeWallet){
                    startNodeCycle(nodeWallet)
                })
                //                stopAllWallets(function(){
                //                    console.log("completed shutdown")
                //                })  
            })
        });
    });
}

function startNodeCycle(nodeWallet){
    async.eachSeries(nodeWallet.nodes, function(nodeOutput, callback){
        var node = nodes[nodeOutput];
        startWallet(node, nodeWallet, function(){
            nodeWalletHealthChecks(node, nodeWallet, function(){
                //start fn
                //check FN ok status
                //wait 5 min check fn on site
                runFundamentalnode(node, nodeWallet, function(pos){
                    console.log("Node in pos = " + pos)
                    setTimeout(function(){
                        stopWallet(nodeWallet, 0, function(){
                            callback();
                        })
                    },pos?3600000:1800000)
                })
            })
        })
    }, function(){
        console.log("cycle complete for "+nodeWallet.nodeWallet);
        setTimeout(function(){
            startNodeCycle(nodeWallet);
        },3000)
    })
}

function runFundamentalnode(node, nodeWallet, callback){
    //TODO fix if returns bad
    startFundamentalNode(nodeWallet, function(){
        checkFundamentalnodeDebug(nodeWallet, function(running){
            if(running){
                setTimeout(function(){
                    checkFundamentalnodeOnline(node, function(online, pos){
                        if(online){
                            callback(pos);
                        }else{
                            setTimeout(function(){
                                console.log("node node online trying again " + node.nodeWallet)
                                runFundamentalnode(node, nodeWallet, callback);
                            },10000)};
                    })
                },360000);
            }else{
                setTimeout(function(){
                    runFundamentalnode(node, nodeWallet, callback);
                },10000)
            }
        })
    })
}

function checkFundamentalnodeOnline(node, callback){
    var address = node.nodeAddress.address;
    request('http://fn.b3nodes.net/fncheck.txt', function (error, response, body) {
        //        console.log('error:', error); // Print the error if one occurred
        //        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        //        console.log('body:', body); // Print the HTML for the Google homepage.
        if(body!=undefined){
            var lines = body.split("\n");
            var found = false;
            var pos = false;
            console.log("Checking address " + address + " is online - node " + node.nodeWallet)
            for(var i=0; i < lines.length; i++){
                if(lines[i].length>50){
                    var vals = lines[i].split('\t').filter(Boolean)
                    if(vals[2]===address){
                        found = true
                        pos = vals[1]==='POS_ERROR';
                        break;
                    }
                }
            }
            console.log("node " +node.nodeWallet+ " was found online = " + found);
            callback(found, pos);
        }else{
            console.log("Website not loading")
            callback();
        }
    });
}

function checkFundamentalnodeDebug(nodeWallet, callback){
    console.log("checking fundamental node debug " + nodeWallet.nodeWallet)
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWalet.nodeWallet + " appears to be stuck");
            restartWallet(nodeWallet, function(){
                checkFundamentalnodeDebug(nodeWallet, callback); 
            });
        }
    },60000);

    nodeWallet.client.fundamentalnode("debug", function(err, res){
        if(err){
            console.log(err);
        }
        console.log("Debug " +res.result)
        if(res.result==='fundamentalnode started remotely'){
            stopFundamentalNode(nodeWallet,function(){
                setTimeout(function(){
                    startFundamentalNode(nodeWallet, function(){
                        checkFundamentalnodeDebug(nodeWallet, callback);
                    })
                },5000)
            });
        }else{
            callback(res.result==='successfully started fundamentalnode');
        }
        requestCompleted = true; 
    });
}

function stopFundamentalNode(nodeWallet,callback){
    nodeWallet.client.fundamentalnode('stop', function(err, res){
        if(err){
            console.log(err);
            callback();
        }else{
            console.log(res);
            callback();
        }
    })
}

function startFundamentalNode(nodeWallet, callback){
    console.log("starting fundamental node " + nodeWallet.nodeWallet)
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWalet.nodeWallet + " appears to be stuck");
            restartWallet(nodeWallet, function(){
                startFundamentalNode(nodeWallet, callback); 
            });
        }
    },60000);

    nodeWallet.client.fundamentalnode("start", function(err, res){
        if(err){
            console.log(err);
        }
        console.log("Started = " + (res.result==='successfully started fundamentalnode'));
        console.log(res.result);
        callback();
        requestCompleted = true;
    })
}

function nodeWalletHealthChecks(node, nodeWallet, callback){
    console.log("Running node health checks " + nodeWallet.nodeWallet)
    waitForWalletSync(nodeWallet, function(){
        checkFNOutputs(node.nodeOutput, nodeWallet, function(outputCorrect){
            console.log("Wallet contains output " + outputCorrect)
            if(outputCorrect){
                //check FN count
                checkFNCount(nodeWallet, function(){
                    callback();
                })
            }else{
                importNodeKeys(node, nodeWallet, function(){
                    nodeWalletHealthChecks(node, nodeWallet, callback);
                });
            }
        });
    })
}

function checkFNCount(nodeWallet, callback){
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWallet.nodeWallet + " appears to be stuck");
            restartWallet(nodeWallet, function(){
                checkFNCount(nodeWallet, callback); 
            });
        }
    },60000);

    nodeWallet.client.fundamentalnode('count', function(err, res){
        if(err){
            console.log(err);
        }else{
            if(res.result>=850){
                console.log("FN count synced")
                callback();
            }else{
                console.log("Wallet needs FN count syncing " + (850-res.result) + " remaining")
                setTimeout(function(){
                    checkFNCount(nodeWallet, callback);
                }, 10000);
            }
            requestCompleted = true;
        }
    })
}

function createClients(callback){
    console.log("Creating clients");
    async.eachSeries(Object.keys(nodeWallets), function(nodeWallet, callback){
        var client = {
            protocol: 'http', //Optional. Will be http by default
            host: '127.0.0.1', //Will be 127.0.0.1 by default
            user: 'AltFreq', //Optional, only if auth needed
            pass: 'Ms016356!01', //Optional. Can be named 'pass'. Mandatory if user is passed.
            port: nodeWallets[nodeWallet].rpcPort, //Will be 8443 for https or 8080 for http by default
        };
        var wallet = new RpcClient(client);
        nodeWallets[nodeWallet].client = wallet;
        callback();
    }, function(){
        console.log("Finished clients");
        callback();
    });
}

function createConfigs(callback){
    console.log('creating configs');
    async.eachSeries(Object.keys(nodes), function(node, callback){
        if (!fs.existsSync(configDir+node+".conf")) {
            var nodeWalletNumber = nodes[node].nodeWallet;
            var nodeWallet = nodeWallets[nodeWalletNumber];
            var string = 'server=0\nrpcallowip=127.0.0.1/24\nrpcpassword=Ms016356!01\nrpcuser=AltFreq\nrpcport='+nodeWallet.rpcPort+'\nprinttoconsole=1\nfundamentalnodeprivkey=69DPrEDsf2WuEq1qmygZ64iyVMrBBtAtr2Wdi4WoqPEqgddttYK\nfundamentalnode=1\npromode=1\nmaxconnections=5\nrpcthreads=1\nminrelaytxfee=0.00005\nlimitfreerelay=5\nmaxmempool=100\ndbcache=100\nbind=['+nodeWallet.ip+']\nexternalip=['+nodeWallet.ip+']\nfundamentalnodeaddr=['+nodeWallet.ip+']:5647\nfundamentalnodedonation='+donationAddress+'\nfundamentalnodepercentage=100\n';
            if(nodeWalletNumber==0){
                string+="blocknotify=node '"+blockNotify+"' %s\n";
            }
            fs.writeFile(configDir+node+".conf", string, 'utf8', function (err) {
                if (err) {
                    console.log("An error occured while writing JSON Object to File.");
                    return console.log(err);
                }
                callback();
            });
        }else{
            callback();
        }
    }, function(){
        console.log('creating configs completed');
        callback();
    });

}

function createWalletDats(callback){
    console.log("Creating wallet.dats");
    async.eachSeries(Object.keys(nodes), function(node, callback){
        if (!fs.existsSync(walletDatDir+node+".dat")) {
            console.log("Wallet.dat missing for " + node);
            generateWalletDat(nodes[node], nodeWallets[nodes[node].nodeWallet], function(){
                console.log("finished generating wallet")
                callback();
            })
        }else{
            callback();
        }
    }, function(){
        callback();
    })
}

function generateWalletDat(node, nodeWallet, callback){
    console.log("Generating wallet.dat for node " + nodeWallet.nodeWallet)
    startWallet(node, nodeWallet, function(){
        waitForWalletSync(nodeWallet, function(){
            importNodeKeys(node, nodeWallet, function(){
                checkFNOutputs(node.nodeOutput, nodeWallet, function(outputCorrect){
                    console.log("Wallet contains output " + outputCorrect)
                    if(outputCorrect){
                        stopWallet(nodeWallet, 0, function(){
                            console.log("Wallet stopped")
                            callback();
                        });
                    }else{
                        console.log("Error creating wallet.dat")
                        stopWallet(nodeWallet, 0, function(){
                            console.log("Wallet stopped")
                            deleteFile(walletDatDir+node+'.dat',function(){
                                console.log("trying again");
                                generateWalletDat(node, callback);
                            })
                        });
                    }
                });
            })
        })
    })
}

function deleteFile(path, callback){
    console.log("Deleting file " + path)
    if(fs.existsSync(path)){
        fs.unlinkSync(path);
        callback()
    }else{
        callback();
    }
}

function stopWallet(nodeWallet, attempt, callback){
    stopFundamentalNode(nodeWallet, function(){
        console.log("Stopping wallet node "  + nodeWallet.nodeWallet);
        nodeWallet.client.stop(function(){
            setTimeout(function(){
                checkWalletStopped(nodeWallet, function(walletStopped){
                    if(walletStopped){
                        console.log("Wallet is stopped")
                        callback();
                    }else{
                        if(attempt>5){
                            console.log("Forcing wallet to stop node "+ nodeWallet.nodeWallet)
                            forceStopWallet(nodeWallet, callback);
                        }else{
                            console.log("Trying to stop again node "+ nodeWallet.nodeWallet + " attempt "+  attempt)
                            stopWallet(nodeWallet, attempt+=1, callback)
                        }
                    }
                });
            },3000);
        });  
    })
}

function checkWalletStopped(nodeWallet, callback){
    getNodePID(nodeWallet, function(pid){
        console.log("PID "+ nodeWallet.nodeWallet + " = " + pid)
        console.log(!pid)
        if(!pid){
            callback(true);
        }else if(pid>0){
            callback(false);
        }else{
            callback(true);
        }
    })
}

function restartWallet(nodeWallet, callback){
    console.log("Restarting wallet " + nodeWallet.nodeWallet);
    stopWallet(nodeWallet, 0 , function(){
        setTimeout(function(){
            startWallet(nodeWallet.nodeWallet, nodeWallet, function(){
                console.log("Wallet reset");
                setTimeout(function(){
                    callback();
                },3000);
            })
        }, 3000)
    })
}

function forceStopWallet(nodeWallet, callback){
    //lsof -ti TCP@[2604:a880:400:d1::ab6:100f]:5647
    console.log("Force stopping wallet")
    getNodePID(nodeWallet,function(pid){
        killProcess(pid, function(){
            setTimeout(function(){
                checkWalletStopped(nodeWallet, function(walletStopped){
                    if(walletStopped){
                        callback()
                    }else{
                        forceStopWallet(nodeWallet, callback);
                    }
                });
            }, 3000);
        });
    });
    //kill -9
}

function killProcess(pid, callback){
    dir = exec("kill -9 "+pid, function(err, stdout, stderr) {
        if (err) {
            console.log(err);
        }
        console.log(stdout);
        callback();
    });
}

function getNodePID(nodeWallet, callback){
    console.log("Getting node PID - " + nodeWallet.nodeWallet);
    dir = exec("lsof -ti TCP@["+nodeWallet.ip+"]:5647", function(err, stdout, stderr) {
        //        console.log(stdout);
        callback(stdout.split("\n")[0]);
    });
}

function checkFNOutputs(nodeOutput, nodeWallet, callback){
    console.log("Checking fn outputs " + nodeOutput)
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWalet.nodeWallet + " appears to be stuck");
            restartWallet(nodeWallet, function(){
                checkFNOutputs(nodeOutput, nodeWallet, callback); 
            });
        }
    },60000)
    nodeWallet.client.fundamentalnode('outputs', function(err, res){
        console.log("Checking outputs")
        var found = false;
        async.eachSeries(Object.keys(res.result),function(val, callback){
            if(nodeOutput===val){
                found = true;
            }
            callback();
        }, function(){
            if(found){
                callback(true);
            }else{
                //stopwallet
                stopWallet(nodeWallet, 0, function(){
                    deleteFile(walletDatDir+nodeOutput+'.dat',function(){
                        var node = nodes[nodeOutput];
                        generateWalletDat(node, nodeWallet, function(){
                            startWallet(node, nodeWallet, function(){
                                nodeWalletHealthChecks(nodes[nodeOutput],nodeWallet, function(){
                                    checkFNOutputs(nodeOutput, nodeWallet, callback);
                                })
                            })
                        });
                    });
                })
                //delete wallet.dat
                //import
                //startwallet
            }  
        })
        requestCompleted = true;
    });
}

function importNodeKeys(node, nodeWallet, callback){
    console.log("need to import keys " + node)
    importKey(nodeWallet, node.nodeAddress.privateKey, function(){
        async.eachSeries(node.inputAddresses, function(data, callback){
            importKey(nodeWallet, data.pubkey, function(){
                callback();
            })
        }, function(){
            console.log("importing keys completed")
            callback();
        })
    });
}

function importKey(nodeWallet, privkey, callback){
    console.log("importing privkey " + privkey);
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWalet + " appears to be stuck");
            restartWallet(nodeWallet, function(){
                importKey(nodeWallet, privkey, callback); 
            });
        }
    },1800000)
    nodeWallet.client.importPrivKey(privkey, function(err, res){
        if(err){
            console.log(err);
        }else{
            console.log("Priv key imported")
            callback();
        }
        requestCompleted = true;
    })
}

function waitForWalletSync(nodeWallet, callback){
    console.log("Checking wallet synced node " + nodeWallet.nodeWallet)
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWallet.nodeWallet + " appears to be stuck");
            restartWallet(nodeWallet, function(){
                waitForWalletSync(nodeWallet, callback); 
            });
        }
    },60000);
    getBlockCount(function(blockcount){
        nodeWallet.client.getBlockCount(function(err, res){
            if(err){
                console.log(err);
            }else{
                if(res.result>=blockcount){
                    console.log("Wallet synced")
                    callback();
                }else{
                    console.log("Wallet needs syncing " + (blockcount-res.result) + " remaining")
                    setTimeout(function(){
                        waitForWalletSync(nodeWallet, callback);
                    }, 10000);
                }
            }
            requestCompleted = true;
        })
    })
}

function stopAllWallets(callback){
    async.eachSeries(nodeWallets, function(nodeWallet, callback){
        stopWallet(nodeWallet, 0,function(){
            callback();
        })
    },function(){
        callback();
    });
}

function startWallet(node, nodeWallet, callback){
    console.log("Starting wallet "+nodeWallet.nodeWallet)
    console.log("screen -dmS node"+nodeWallet.nodeWallet+" bash -c 'nice b3coind -datadir=/root/nodeWallets/node"+nodeWallet.nodeWallet+" -conf="+configDir+node.nodeOutput+".conf -wallet="+walletDatDir+node.nodeOutput+".dat; sleep 2'");
    dir = exec("screen -dmS node"+nodeWallet.nodeWallet+" bash -c 'nice b3coind -datadir=/root/nodeWallets/node"+nodeWallet.nodeWallet+" -conf="+configDir+node.nodeOutput+".conf -wallet="+walletDatDir+node.nodeOutput+".dat; sleep 2'", function(err, stdout, stderr) {
        if (err) {
            // should have err.code here?  
            console.log(err);
        }
        console.log(stdout);
    });

    dir.on('exit', function (code) {
        // exit code is code
        setTimeout(function(){
            walletRunning(nodeWallet, 0, function(running){
                if(running){
                    console.log("wallet running")
                    callback();
                }else{
                    startWallet(node, nodeWallet, callback);
                }
            });
        },3000);
    });
}

function walletRunning(nodeWallet, attempt, callback){
    console.log("checking if wallet running "+ nodeWallet.nodeWallet +  " attempt " + attempt);
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWalet.nodeWallet + " appears to be stuck");
            restartWallet(nodeWallet, function(){
                walletRunning(nodeWallet, 0, callback); 
            });
        }
    },60000);
    nodeWallet.client.getBlockCount(function(err, res){
        if(err){
            console.log(err);
            console.log("unable to connect to wallet "+ nodeWallet.nodeWallet);
            if(attempt < 5){
                setTimeout(function(){
                    walletRunning(nodeWallet, attempt+=1, callback);
                }, 5000);
            }else{
                console.log("attempt >=5 " + nodeWallet.nodeWallet)
                callback(false);
            }
        }else{
            console.log("Wallet running result " + nodeWallet.nodeWallet)
            callback(res.result>=0);
        }
        console.log("request completed " +nodeWallet.nodeWallet )
        requestCompleted = true;
    })
}

function getBlockCount(callback){
    request('http://chainz.cryptoid.info/b3/api.dws?q=getblockcount', function (error, response, body) {
        //        console.log('error:', error); // Print the error if one occurred
        //        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        //        console.log('body:', body); // Print the HTML for the Google homepage.
        callback(body);
    });
}

function getWalletClient(walletNumber){
    return nodeWallets[walletNumber].client;
}

//split nodes to users
function splitNodes(){
    var keys = Object.keys(nodes);
    var usingNodes = Math.floor(keys.length*nodeCountPercentage);
    var usingPosition = nodeCountUsagePosition*usingNodes;
    for(var i = usingPosition, j=0; i < usingNodes; i++){
        nodes[keys[i]].nodeWallet=j;
        nodes[keys[i]].nodeOutput=keys[i];
        if(Object.keys(nodeWallets).includes(j.toString())){
            nodeWallets[j].nodes.push(keys[i]);
        }else{
            nodeWallets[j] = {nodes:[keys[i]], ip:getAvailableIP(), rpcPort: (8000+j), nodeWallet: j};
        }
        if(j+1<walletCount){
            j++;
        }else{
            j=0;
        }
    }
    console.log(nodeWallets);
}

function getAvailableIP(){
    var ip = availableIps[0];
    availableIps.splice(0,1);
    return ip;
}

require('net').createServer(function(socket) {
    socket.on("error", function(err){
        console.log("Caught socket error: ")
        console.log(err.stack)
    });
    console.log("Block detected")
    socket.on('data', function(blockTx) {
        checkForNodeStake(blockTx);
    });
}).listen(8100);

function checkForNodeStake(rawTX){
    async.each(rawTX['vout'], function(output){
        var amount = output['value']
        if(amount>0){
            var address = output['scriptPubKey']['"addresses"'][0];
            checkAddressIsNode(address, function(isNode){
                if(isNode){
                    //make payment / queue
                    fs.appendFile('/root/payments.txt', address + " " + amount, function (err) {
                        if (err) throw err;
                        console.log('Saved!');
                    });
                }
            })
        }
    });
}

function checkAddressIsNode(address, callback){
    var keys = Object.keys(nodes)
    var found = false;
    for(var i =0; i < keys.length; i++){
        if(nodes[keys[i]].nodeAddress.address===address){
            found=true;
            break;
        }
    }
    callback(found);
}

function getTransaction(nodeWallet, txID, callback){
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWalet.nodeWallet + " appears to be stuck");
            //            restartWallet(nodeWallet, function(){
            getTransaction(nodeWallet, txid, callback); 
            //            });
        }
    },60000);
    nodeWallets[0].client.getTransaction(txID, function(err, res){
        var rawTx = res.result;
        requestCompleted = true;
        callback(rawTx);
    });
}

function getRawBlock(nodeWallet, callback){
    var requestCompleted = false;
    setTimeout(function(){
        if(!requestCompleted){
            console.log("Wallet " + nodeWallet.nodeWallet + " appears to be stuck");
            //            restartWallet(nodeWallet, function(){
            getRawBlock(nodeWallet, callback); 
            //            });
        }
    },60000);
    nodeWallet.client.getBlockHash(blockHash, function(err, res){
        var rawBlock = res.result;
        requestCompleted = true;
        callback(rawBlock);
    });
}

stdin.addListener("data", function(d) {
    // note:  d is an object, and when converted to a string it will
    // end with a linefeed.  so we (rather crudely) account for that
    // with toString() and then trim()    
    var args = d.toString().trim().split(' ');
    var command = args[0];
    args = args.splice(1);
    if(command==='q'){
        stopAllWallets(function(){
            process.exit(1);
        })
    }else{
        console.log('\n');
    }
});


function timedChecks(){
    setTimeout(function(){
        checkForPosNodes(function(){
            timedChecks();
        });
    }, 600000);
}

timedChecks();

function checkForPosNodes(callback){
    console.log("chekcing pos nodes")
    request('http://fn.b3nodes.net/fncheck.txt', function (error, response, body) {
        //        console.log('error:', error); // Print the error if one occurred
        //        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        //        console.log('body:', body); // Print the HTML for the Google homepage.
        if(body!=undefined){
            var lines = body.split("\n");
            var inPOS =0;
            var enabled = 0;
            async.each(lines, function(line, callback){
                if(line.length>50){
                    var vals = line.split('\t').filter(Boolean);
                    var address = vals[2];
                    var status = vals[1];
                    if(address){
                        checkAddressIsNode(address, function(isNode){
                            if(isNode){
                                if(status==="ENABLED"){
                                    enabled+=1;
                                }else{
                                    inPOS+=1;
                                }
                                callback();
                            }else{
                                callback();
                            }
                        })
                    }else{
                        callback();
                    }
                }else{
                    callback();
                }
            }, function(){
                console.log("=======Nodes in POS: " + inPOS + ",Enabled: "+enabled+" Total nodes: " + (enabled+inPOS)+"========");
                callback()
            })
        }else{
            console.log("Website down")
            callback()
        }
    });
}